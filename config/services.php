<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the cr   edentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */
    
    'stripe' => [
        'secret' => 'sk_test_51HOjzsC1zYIo0nPVtGcQectayrFTWlHpuLrEq2a9k556QtlIYMOJksjiPXZDr5lDFFbezTywBwWobnXakLRvcw5a00wkFxG1U5',
        
        "live_public"=>"pk_live_51IBBiDGMLSfhNJYIiP6UiIlViyx5VOPNCshdtiQg8g5ByDj2tofLt7ZQPn47EtVd7GlAQSrfZmF1IxWuT2TAdgpq00N5WN8ttp",
        "live_secret"=>"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",

        "test_secret"=>"sk_test_51HcZhRAcCMI2K7rsH60yWDdNq4Vltnhk56KE03b4p3Kh2R2A1WaP2ESZdTGa2sp0g939t5wdgHLUdSkKZHduDa9g00ksqJpt0r",
        "test_public"=>"pk_test_51HcZhRAcCMI2K7rs5nNCMuTKWt9sUvf03mnHJ1NzTPbwHWtzcOvnBqDdJZdgPJoxT5ZuGQRSu1yogTrX6mrarxRZ003qVVrYLv"
    ],
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'passport'=>[

        'key'=>'fmTKwSaSFycytDgB5F6ntiS0wcWxIRpuSrLX3I5A',
    ]

];
