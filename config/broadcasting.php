<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Broadcaster
    |--------------------------------------------------------------------------
    |
    | This option controls the default broadcaster that will be used by the
    | framework when an event needs to be broadcast. You may set this to
    | any of the connections defined in the "connections" array below.
    |
    | Supported: "pusher", "redis", "log", "null"
    |
    */
    // env('BROADCAST_DRIVER', 'null')
    'default' => 'pusher',

    /*
    |--------------------------------------------------------------------------
    | Broadcast Connections
    |--------------------------------------------------------------------------
    |
    | Here you may define all of the broadcast connections that will be used
    | to broadcast events to other systems or over websockets. Samples of
    | each available type of connection are provided inside this array.
    |
    */
    // 
    // 
    // 
    'connections' => [

        'pusher' => [
            'driver' => 'pusher',
            'key' => '0a6be60498548bd8679b',
            'secret' => 'a8db71c4a5fc4823530a',
            'app_id' => '1091770',
            'options' => [
                'cluster' => 'us3',
                'useTLS' => true
              ],
        ],
        // 'pusher' => [
        //     'driver' => 'pusher',
        //     'key' => '5a4f6e89abecfe95b06a',
        //     'secret' => 'd6c874740d0cc175ce15',
        //     'app_id' => '1040491',
        //     'options' => [
        //         'cluster' => 'ap2',
        //         'useTLS' => true
        //     ],
        // ],
        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
        ],

        'log' => [
            'driver' => 'log',
        ],

        'null' => [
            'driver' => 'null',
        ],

    ],

];
