<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <link rel="icon" href="<%= BASE_URL %>favicon.ico"> -->
    <link rel="icon" type="image/png" href="/images/logo/M1.png"/>

    <title>Meaal</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset(mix('css/main.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/iconfont.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/material-icons/material-icons.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/vuesax.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/prism-tomorrow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
    
    {{-- <link rel="stylesheet" href=""> --}}
    <!-- Favicon -->
    <style>
      /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
 .empty-extra{
      margin:15px 0px;
      background-color: var(--main-color);
  }
  .empty-extra h4{
          margin: 0;
          color: #fff;
          font-family: 'Montserrat', sans-serif;
          font-size: 18px;
          text-align: center;
      }
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
.stripe-card{
  width:auto!important;
  border-radius: .25rem!important
  border: 1px solid #ced4da!important;
}
  /* .card-border{
      background-color: #F5F5F5;
      border-radius: 8px;
      padding: 14px;

    } */
    .card-border{
      background-color: #F5F5F5;
      border-radius: 8px;
      padding: 14px;

    }
    
    .head{
        background-color: #F9F9F9;
        padding:8px;
        box-shadow: 0px 0px 8px 0px rgb(224, 223, 223);
        border-radius:4px;
    }
    [dir] .inventory .card .body-inventory .row-list[data-v-2422f490]:nth-child(even) .icon h3:hover {
      color:white!important;
  }
  .is-invalid{
        box-shadow: 0px 0px 8px rgb(250, 94, 94)!important;
    }
    
    </style>
    <script src="https://js.stripe.com/v3/"></script>
  </head>
  <body style="background-color:#ececec!important">
    <noscript>
      <strong>We're sorry but Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app" style="background-color:#ececec!important">
    </div>

    <!-- <script src="js/app.js"></script> -->
    <script src="{{ asset(mix('js/app.js')) }}"></script>


  </body>
</html>
