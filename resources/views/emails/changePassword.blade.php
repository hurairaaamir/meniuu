@component('mail::message')

<img src="{{ asset('images/logo/2.png') }}" alt="logo" />
<h1>Password Reset Email</h1>
<p>click the below button to redirect to the site and reset password</p>

<a href="{{$url}}">reset password</a>

@component('mail::button', ['url' => '#'])
Reset Password
@endcomponent



@endcomponent
