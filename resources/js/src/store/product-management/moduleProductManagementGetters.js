/*=========================================================================================
  File Name: moduleCalendarGetters.js
  Description: Calendar Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {

  GetProducts(state){
    return  state.products;
  },
  GetSelectedProduct(state){
    return state.product;
  },

  
  GetCategories(state){
    return  state.categories;
  },
  GetSelectedCategory(state){
    return state.category;
  },

}
