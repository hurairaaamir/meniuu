/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
  ADD_PRODUCT(state,product){
    state.products.unshift(product);
  },
  ADD_PRODUCTS(state,products){
    state.products=products;
  },
  ADD_SELECTED_PRODUCT(state,product){
    state.product=product;
  },


  ADD_CATEGORY(state,category){
    state.categories.unshift(category);
  },
  ADD_CATEGORIES(state,categories){
    state.categories=categories;
  },
  ADD_SELECTED_CATEGORY(state,category){
    state.category=category;
  },
  REMOVE_CATEGORY(state,id){
    const Index = state.categories.findIndex((u) => u.id == id);
    state.categories.splice(Index, 1);
  },
  REMOVE_PRODUCT(state,id){
    const Index = state.products.findIndex((u) => u.id == id);
    state.products.splice(Index, 1);
  },
  ADD_HISTORY(state,histories){
     state.histories=histories;
  },

  ADD_COUNT(state,count){
    state.count=count;
  },
  

}
