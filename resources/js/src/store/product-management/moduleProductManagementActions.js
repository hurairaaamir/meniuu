/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from "@/axios.js"


export default {

  storeProduct(context, product){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/product/store", product)
        .then((response) => {
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },

  getProducts(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/product/index/"+data.restaurant_id+"/"+data.product_category_id)
        .then((response) => {
          context.commit('ADD_PRODUCTS',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  getProduct(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/product/show/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  updateProduct(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.put("/api/product/update/"+data.id,data.product)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteProduct(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete("/api/product/delete/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          context.commit('REMOVE_PRODUCT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },










  storeCategory(context, category){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/category/store", category)
        .then((response) => {
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },

  getCategories(context,restaurant_id){

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/category/index/"+restaurant_id)
        .then((response) => {
          context.commit('ADD_CATEGORIES',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  getCategory(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/category/show/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_CATEGORY',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  updateCategory(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.put("/api/category/update/"+data.id,data.category)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteCategory(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete("/api/category/delete/"+id,)
        .then((response) => {
          context.commit('REMOVE_CATEGORY',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },










  getEnteries(context, restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/enteries/get/"+restaurant_id,)
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },
  storeEnteries(context,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/enteries/store",data)
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },







  getHistory(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/enteries/history/"+data.restaurant_id+"/"+data.type)
        .then((response) => {
          context.commit('ADD_HISTORY',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },











  getCount(context, restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/product/get-count/"+restaurant_id)
        .then((response) => {
          context.commit('ADD_COUNT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  storeNotificationEmail(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/notification-email/store",data)
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  getNotificationEmail(context, restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/notification-email/show/"+restaurant_id)
        .then((response) => {
          resolve(response.data.data)
        })
        .catch((error) => { reject(error) })
    })
  },

}
