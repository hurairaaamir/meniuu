/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
  ADD_EXTRAS(state,extras){
    state.extras=extras;
  },
  REMOVE_EXTRAS(state,id){
    const Index = state.extras.findIndex((u) => u.id == id);
    state.extras.splice(Index, 1);
  },

  ADD_DISHCATEGORY(state,dishCategory){
    
    state.dishCategories=dishCategory;
  },
  REMOVE_DISHCATEGORY(state,id){
    const Index = state.dishCategories.findIndex((u) => u.id == id);
    state.dishCategories.splice(Index, 1);
  },
  CHANGE_STATUS(state,dishCategory){
    const index=state.dishCategories.findIndex((u) => u.id == dishCategory.id);
    state.dishCategories[index].status=dishCategory.status;
  },
  CHANGE_DISH_STATUS(state,dish){
    const index=state.dishes.findIndex((u) => u.id == dish.id);
    state.dishes[index].status=dish.status;
  },
  ADD_DISH(state,dish){
    state.dishes=[];
    state.dishes=dish;
  },
  REMOVE_DISH(state,id){
    const Index = state.dishes.findIndex((u) => u.id == id);
    state.dishes.splice(Index, 1);
  },
  

  ADD_DISHES_INGREDIENT(state,dishIngredient){
    state.dishIngredients=dishIngredient;
  },
  REMOVE_DISHINGREDIENT(state,id){
    const Index = state.dishIngredients.findIndex((u) => u.id == id);
    state.dishIngredients.splice(Index, 1);
  },
  ADD_DISH_INGREDIENT(state,dishIngredient){
    state.dishIngredients.unshift(dishIngredient);
  },

  ADD_TABLES(state,tables){
    state.tables=tables;
  },
  REMOVE_TABLE(state,id){
    const Index = state.tables.findIndex((u) => u.id == id);
    state.tables.splice(Index, 1);
  },
  ADD_TABLE(state,table){
    state.tables.unshift(table);
  },
    
  ADD_SOCIALMEDIA(state,socialMedia){
    state.socialMedia=socialMedia;
  },
  REMOVE_TABLE(state,id){
    const Index = state.socialMedia.findIndex((u) => u.id == id);
    state.socialMedia.splice(Index, 1);
  },

  ADD_COUNT(state,count){
    state.count=count;
  },

  ADD_SEARCH_DISH(state,dish){
    state.dishes=[];
    state.dishes.push(dish);
  },
  ADD_ACCOUNT(state,account){
    state.account=account;
  }  
}
