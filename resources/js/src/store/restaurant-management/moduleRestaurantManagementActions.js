/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from "@/axios.js"


export default {

  storeExtra(context, extras){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/extras/store", extras)
        .then((response) => {
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },

  getExtras(context, restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/extras/index/"+restaurant_id)
        .then((response) => {
          context.commit('ADD_EXTRAS',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  getExtra(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/extras/show/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  updateExtra(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.put("/api/extras/update/"+data.id,data.extra)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteExtra(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete("/api/extras/delete/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          context.commit('REMOVE_EXTRAS',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  // Dish Categories

  storeDishCategory(context, dishCategory){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/dish-category/store", dishCategory)
        .then((response) => {
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },

  getDishCategories(context, restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dish-category/index/"+restaurant_id)
        .then((response) => {
          context.commit('ADD_DISHCATEGORY',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  getActiveDishCategories(context, restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dish-category/active/"+restaurant_id)
        .then((response) => {
          context.commit('ADD_DISHCATEGORY',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  geDishCategory(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dish-category/show/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  updateDishCategory(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    
    return new Promise((resolve, reject) => {
      // /"+data.id
      axios.post("/api/dish-category/update",data)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteDishCategory(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete("/api/dish-category/delete/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          context.commit('REMOVE_DISHCATEGORY',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  updateDishCategoryStatus(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/dish-category/status",data)
        .then((response) => {
          // context.commit('REMOVE_DISHCATEGORY',response.data.data);
          context.commit("CHANGE_STATUS",response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  







  // Dish 

  storeDish(context, dish){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/dishes/store", dish)
        .then((response) => {
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },

  getActiveDishes(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dishes/active/"+data.restaurant_id+"/"+data.id)
        .then((response) => {
          context.commit('ADD_DISH',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  getDishes(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dishes/index/"+data.restaurant_id+"/"+data.id)
        .then((response) => {
          context.commit('ADD_DISH',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  getDish(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dishes/show/"+id)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  searchDish(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/dishes/get-search-dish/"+data.restaurant_id+"/"+data.name)
        .then((response) => {
          context.commit('ADD_SEARCH_DISH',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
    })
  },

  updateDish(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/dishes/update",data)
        .then((response) => {
          // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteDish(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete("/api/dishes/delete/"+id)
        .then((response) => {
          context.commit('REMOVE_DISH',id);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  updateDishStatus(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/dish/status",data)
        .then((response) => {
          // context.commit('REMOVE_DISHCATEGORY',response.data.data);
          context.commit("CHANGE_DISH_STATUS",response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
























    // Dish Categories

    storeDishIngredient(context, dishCategory){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.post("/api/dish-ingredient/store", dishCategory)
          .then((response) => {
            context.commit('ADD_DISH_INGREDIENT',response.data.data);
            resolve(response);
            // this.fetchUsers();
          })
          .catch((error) => { reject(error) })
      })
    },
  
    getDishIngredients(context, data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/dish-ingredient/index/"+data.restaurant_id+"/"+data.dish_id)
          .then((response) => {
            context.commit('ADD_DISHES_INGREDIENT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
  
    geDishIngredient(context, id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/dish-ingredient/show/"+id)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            resolve(response.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },
  
    updateDishIngredient(context, data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        // /"+data.id
        axios.post("/api/dish-ingredient/update",data)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
    deleteDishIngredient(context, id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.delete("/api/dish-ingredient/delete/"+id)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            context.commit('REMOVE_DISHINGREDIENT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
    




























    // Table

    storeTable(context, table){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.post("/api/table/store", table)
          .then((response) => {
            context.commit('ADD_TABLE',response.data.data);
            resolve(response);
            // this.fetchUsers();
          })
          .catch((error) => { reject(error) })
      })
    },
  
    getTables(context, restaurant_id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/table/index/"+restaurant_id)
          .then((response) => {
            context.commit('ADD_TABLES',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
  
    getTable(context, id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/table/show/"+id)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            resolve(response.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },
  
    updateTable(context, data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      
      return new Promise((resolve, reject) => {
        // /"+data.id
        axios.put("/api/table/update/"+data.id,data.table)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
    deleteTable(context, id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.delete("/api/table/delete/"+id)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            context.commit('REMOVE_TABLE',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
  





















        // SocialMedia

    storeSocialMedia(context, table){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.post("/api/social-media/store", table)
          .then((response) => {
            // context.commit('ADD_SOCIALMEDIA',response.data.data);
            resolve(response);
            // this.fetchUsers();
          })
          .catch((error) => { reject(error) })
      })
    },
  
    getSocialMedia(context, restaurant_id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/social-media/index/"+restaurant_id)
          .then((response) => {
            context.commit('ADD_SOCIALMEDIA',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },

    updateSocialMedia(context, data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      
      return new Promise((resolve, reject) => {
        // /"+data.id
        axios.put("/api/social-media/update/"+data.id,data.socialMedia)
          .then((response) => {
            // context.commit('ADD_SELECTED_PRODUCT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },




    getCount(context, restaurant_id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/restaurant/get-count/"+restaurant_id)
          .then((response) => {
            context.commit('ADD_COUNT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },

    getAccount(context, restaurant_id){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.get("/api/restaurant/get-account/"+restaurant_id)
          .then((response) => {
            context.commit('ADD_ACCOUNT',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
    























}
