/*=========================================================================================
  File Name: actions.js
  Description: Vuex Store - actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
import axios from 'axios'

// let hostname=window.location.hostname;


const actions = {

    // /////////////////////////////////////////////
    // COMPONENTS
    // /////////////////////////////////////////////

    // Vertical NavMenu
    updateVerticalNavMenuWidth({ commit }, width) {
      commit('UPDATE_VERTICAL_NAV_MENU_WIDTH', width)
    },
    clearCard({ commit }){
      commit('CLEAR_CARD');
    },
    // VxAutoSuggest
    updateStarredPage({ commit }, payload) {
      commit('UPDATE_STARRED_PAGE', payload)
    },
    closeRstaurant({ commit }){
      commit('CHANGE_STATUS','close'); 
    },

    // The Navbar
    arrangeStarredPagesLimited({ commit }, list) {
      commit('ARRANGE_STARRED_PAGES_LIMITED', list)
    },
    arrangeStarredPagesMore({ commit }, list) {
      commit('ARRANGE_STARRED_PAGES_MORE', list)
    },

    // /////////////////////////////////////////////
    // UI
    // /////////////////////////////////////////////

    toggleContentOverlay({ commit }) {
      commit('TOGGLE_CONTENT_OVERLAY')
    },
    updateTheme({ commit }, val) {
      commit('UPDATE_THEME', val)
    },
    resetUser({commit}){
      commit('RESET_USER');
    },

    // /////////////////////////////////////////////
    // User/Account
    // /////////////////////////////////////////////

    updateUserInfo({ commit }, payload) {
      commit('UPDATE_USER_INFO', payload)
    },



    // auth actions

    retrieveToken(context, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('/api/login', {
          email: credentials.email,
          password: credentials.password,
        })
          .then(response =>{
            const token = response.data.access_token;
            localStorage.setItem('access_token', token);
            context.commit('retrieveToken', token);
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
        })
    },
    getUser(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
      return new Promise((resolve, reject) => {
        axios.post('/api/auth/user').then((response)=>{
          context.commit('setUser',response.data.data);
          context.commit('setRestaurant',response.data.data.restaurant);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    getCustomer(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
      return new Promise((resolve, reject) => {
        axios.post('/api/auth/user').then((response)=>{
          context.commit('setUser',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    register(context,form){
      return new Promise((resolve,reject)=>{
        axios.post('/api/register',{
          email:form.email,
          name:form.name,
          password:form.password
        }).then((response)=>{
          context.commit('setUser',response.data.data);
          resolve(response);
        })
        .catch((error)=>{
          reject(error);
        })
      })
    },
    logout(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

      return new Promise((resolve,reject)=>{
        context.commit('RESET_USER');
        localStorage.removeItem('access_token');
        axios.post('/api/logout')
          .then((response)=>{
            resolve(response);
            context.commit('destroyToken');
          }).catch((error)=>{
            reject(error);
          })
      })
    },
    createRestaurant(context,data){
      axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
      return new Promise((resolve, reject) =>{
        axios.post('/api/create-restaurant',data.Restaurant)
          .then((response)=>{
            context.commit('setRestaurant',response.data.data);
            resolve(response.data.data);
          })
          .catch((error)=>{
            reject(error);
          });
      })
    },
    getRestaurant(context,restaurant_id){
      axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
      return new Promise((resolve, reject) =>{
        axios.get('/api/show-restaurant/'+restaurant_id,)
          .then((response)=>{
            context.commit('setRestaurant',response.data.data);
            resolve(response.data.data);
          })
          .catch((error)=>{
            reject(error);
          });
      })
    },
    payIntent(context,data){
      axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
      return new Promise((resolve, reject) =>{
        axios.post('/api/pay/intent',data)
          .then((response)=>{
            resolve(response.data);
          })
          .catch((error)=>{
            reject(error);
          });
      })
    },
    verifyStripeEmail(context,data){
      axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
      return new Promise((resolve, reject) =>{
        axios.post('/api/verify/stripe/email',data)
          .then((response)=>{
            resolve(response.data);
          })
          .catch((error)=>{
            reject(error);
          });
      })
    },

    createCustomer(context,data){
      axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
      return new Promise((resolve, reject) =>{
        axios.post('/api/create-customer',data)
          .then((response)=>{
            resolve(response.data);
          })
          .catch((error)=>{
            reject(error);
          });
      })
    },

    changePermission(context,data){
      axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
      return new Promise((resolve, reject) =>{
        axios.post('/api/change-permission',data)
          .then((response)=>{
            // context.commit('setRestaurant',response.data.data);
            resolve(response.data.data);
          })
          .catch((error)=>{
            reject(error);
          });
      })
    },

    retrieveTokenCustomer(context, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('/api/login', {
          email: credentials.email,
          password: credentials.password,
        })
          .then(response =>{
            const token = response.data.access_token;
            localStorage.setItem('access_token', token);
            context.commit('retrieveToken', token);
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
        })
    },
    changeStatus(context, table){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.post("/api/restaurant/change-status", table)
          .then((response) => {
            context.commit('CHANGE_STATUS',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
    adminChangeStatus(context, table){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.post("/api/restaurant/change-status", table)
          .then((response) => {
            // context.commit('CHANGE_STATUS',response.data.data);
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    },
    getRecord(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
  
      return new Promise((resolve, reject) => {
        axios.post("/api/get-record")
          .then((response) => {
            resolve(response);
          })
          .catch((error) => { reject(error) })
      })
    }

}

export default actions


