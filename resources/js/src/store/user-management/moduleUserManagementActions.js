/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from "@/axios.js"


export default {

  creatUser(context, user) {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/user-management/store-users", user)
        .then((response) => {
          context.commit('ADD_USER', response.data.user);
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },
  editUser(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.post('/api/user-management/edit-users', data.user)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  editOwner(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.post('/api/user-management/edit-owner', data.user)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  editRestaurant(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.post('/api/user-management/edit-restaurant', data)
        .then((response) => {
          context.commit('STORE_RESTAURANT',response.data.data);
          
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchUsers(context) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.get("/api/user-management/users")
        .then((response) => {
          context.commit('SET_USERS', response.data.data);
          resolve(response);
          
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchUser(context, userId) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get(`/api/user-management/users/${userId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeRecord(context, userId) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete(`/api/user-management/users/${userId}`)
        .then((response) => {
          context.commit('REMOVE_RECORD', userId);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  sendMessage(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post('/api/user-management/send-message', data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => { reject(error) })
    });
  },
  createRestaurant(context,data){
    axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
    return new Promise((resolve, reject) =>{
      axios.post('/api/create-restaurant',data.Restaurant)
        .then((response)=>{
          context.commit('STORE_RESTAURANT',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
    })
  },
  getRestaurant(context){
    axios.defaults.headers.common['Authorization']= 'Bearer '+ localStorage.getItem('access_token');
    return new Promise((resolve, reject) =>{
      axios.get('/api/get-restaurant')
        .then((response)=>{
          context.commit('STORE_RESTAURANT',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
    })
  }
  
}
