/*=========================================================================================
  File Name: store.js
  Description: Vuex store
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Vuex from 'vuex'

import state from "./state"
import getters from "./getters"
import mutations from "./mutations"
import actions from "./actions"


Vue.use(Vuex)

import moduleUserManagement from './user-management/moduleUserManagement.js'
import modulTeamManagement from './team-management/moduleTeamManagement.js'
import modulProductManagement from './product-management/moduleProductManagement.js'
import moduleRestaurantManagement from './restaurant-management/moduleRestaurantManagement.js'
import moduleOrderManagement from './order-management/moduleOrderManagement.js'
import moduleAnalyticsManagement from './analytics-management/moduleAnalyticsManagement.js'

export default new Vuex.Store({
    getters,
    mutations,
    state,
    actions,
    modules: {
      moduleUserManagement,
      modulTeamManagement,
      modulProductManagement,
      moduleRestaurantManagement,
      moduleOrderManagement,
      moduleAnalyticsManagement
      // todo: moduleTodo,
      // calendar: moduleCalendar,
      // chat: moduleChat,
      // email: moduleEmail,
      // auth: moduleAuth,
      // eCommerce: moduleECommerce,
  },
    strict: process.env.NODE_ENV !== 'production'
})
