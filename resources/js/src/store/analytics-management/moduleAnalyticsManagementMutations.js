/*=========================================================================================
  File Name: moduleCale ndarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
  
  ADD_ORDERS(state,orders){
    state.orders=orders;
    state.total_num=orders.length;
  },
  ADD_COUNT_DISH(state,dishCount){
    state.dishCount=dishCount;
  },
  ADD_COUNT_DISH_CATEGORY(state,dishCategoryCount){
    state.dishCategoryCount=dishCategoryCount;
  },
  ADD_REVENUE(state,revenue){
    state.revenue=revenue;
  },
  ADD_ORDER_DETAILS(state,orderDetails){
    state.OrderDetails=orderDetails;
  },
  ADD_MOST_SOLD(state,mostSold){
    state.mostSoldDish=mostSold.dish;
    state.mostSoldCategory=mostSold.category;
  },
  ADD_MONEY(state,money){
    state.money=money[0].money;
  },
  ADD_PLATES(state,plates){
    state.plates=plates;
  },
  ADD_INVENTORY(state,inventory){
    state.inventory=inventory;
  },
  

  
}
