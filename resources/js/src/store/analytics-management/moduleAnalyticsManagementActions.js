/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from "@/axios.js"


export default {


  getCountDish(context ,restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.get("/api/analytics/count-dish/"+restaurant_id)
        .then((response)=>{
          context.commit('ADD_COUNT_DISH',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },

  getcountDishCategory(context ,restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.get("/api/analytics/count-dish-category/"+restaurant_id)
        .then((response)=>{
          context.commit('ADD_COUNT_DISH_CATEGORY',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  postcountDishCategory(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/analytics/count-dish-category",data)
        .then((response)=>{
          context.commit('ADD_COUNT_DISH_CATEGORY',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  countRevenue(context ,restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.get("/api/analytics/count-revenue/"+restaurant_id)
        .then((response)=>{
          context.commit('ADD_REVENUE',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  orderDetails(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/analytics/order-details",data)
        .then((response)=>{
          context.commit('ADD_ORDER_DETAILS',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  getMostSold(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/analytics/get-most-sold",data)
        .then((response)=>{
          context.commit('ADD_MOST_SOLD',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  getMoney(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/analytics/get-money",data)
        .then((response)=>{
          context.commit('ADD_MONEY',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  
  getPlate(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/analytics/get-plate",data)
        .then((response)=>{
          context.commit('ADD_PLATES',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  getInventory(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/analytics/get-inventory",data)
        .then((response)=>{
          context.commit('ADD_INVENTORY',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  
}
