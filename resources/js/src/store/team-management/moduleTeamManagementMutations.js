/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
  SET_USERS(state, users) {
    state.users = users
  },
  REMOVE_RECORD(state, itemId) {
      const userIndex = state.users.findIndex((u) => u.id == itemId)
      state.users.splice(userIndex, 1)
  },
  ADD_TEAM(state,employees){
    state.employees=employees;
  },
  STORE_RESTAURANT(state,restaurant){
    state.restaurant=restaurant;
  },
  REMOVE_USER(state,id){
    const userIndex = state.employees.findIndex((u) => u.id == id)
    state.employees.splice(userIndex, 1);
  }
}
