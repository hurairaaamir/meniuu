/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from "@/axios.js"


export default {
  storeTeam(context,user) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/team-store", user)
        .then((response) => {
          context.commit('ADD_TEAM', response.data.user);
          
          resolve(response);
          // this.fetchUsers();
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchTeam(context,data) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.get("/api/team-get/"+data.type+'/'+data.restaurant_id)
        .then((response) => {
          context.commit('ADD_TEAM', response.data.data);
          resolve(response.data.data);
          
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteTeam(context,id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.delete("/api/team/delete/"+id)
        .then((response) => {
          // context.commit('ADD_TEAM', response.data.data);
          context.commit('REMOVE_USER',response.data.data);
          resolve(response.data.data);
          
        })
        .catch((error) => { reject(error) })
    })
  }
}
