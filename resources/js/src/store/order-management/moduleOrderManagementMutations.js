/*=========================================================================================
  File Name: moduleCale ndarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import { isSet } from "lodash";


export default {
  
  ADD_ORDERS(state,orders){
    state.orders=orders;
    state.total_num=orders.length;
  },
  ADD_TABS(state,tabs){
    state.tableTabs=tabs.table;
    state.linkTabs=tabs.link;
  },
  REMOVE_ORDER(state,id){
    const Index = state.orders.findIndex((u) => u.id == id);
    if(Index!=-1&&Index!=null){
      state.orders.splice(Index, 1);
      state.total_num--;
    }
  },
  REMOVE_ITEM(state,i){
    state.order.items.splice(i,1);
  },
  ADD_ITEM(state,item){
    state.order.total -=parseFloat(state.order.oldTip);
    
    state.order.subtotal+=(parseFloat(item.price));
    state.order.total+=(parseFloat(item.price));

    item.extras.forEach(element=>{
      state.order.total+=(parseFloat(element.price));
    });
    state.order.items.push(item);

    state.order.oldTip = (parseFloat(state.order.total) * parseFloat(state.order.tip)) / 100;
    state.order.total +=
        ( parseFloat(state.order.total) * parseFloat(state.order.tip) ) / 100;

  },
  SET_ORDER(state,order){
    state.order=order;
  },
  UPDATE_ITEM_STATUS(state,data){
    let index=state.orders.findIndex(o => o.id == data.order.id);
    let i=state.orders[index].items.findIndex(i => i.id == data.item.id);
    state.orders[index].items[i].status=data.item.status;
  },
  UPDATE_ORDER_STATUS(state,order){
    let index=state.orders.findIndex(o => o.id == order.id);
    state.orders[index].status=order.status;
  },

  SET_REAL_ORDER(state,order){
    if(state.order.length==0){
      state.orders.push(order);
    }else{
      let index=state.orders.findIndex(o=> o.id==order.id);
      if(index!=-1){
        state.orders[index].status=order.status;
      }else{
        state.orders.push(order);
        state.total_num++;
      }
    }
  },
  SET_ITEM(state,item){
    let index=state.orders.findIndex(o=> o.id==item.order_id);

    let i=state.orders[index].items.findIndex(i=> i.id==item.id);

    state.orders[index].items[i].status=item.status;
  },
  RESET_ITEM(state,i){
    state.order.items.splice(i, 1);
  },
  RESET_ORDER(state){
    state.order={
      items:[],
      email:'',
      phone:'',
      optional_note:'',
      pay:'',
      change:'',
      table:'',
      subtotal:0,
      total:0,
      tip:0,
      oldTip:0,
      timestamp:'',
      paymentMethod:'',
      cardNo:'',
      mmyycc:'',
      orderType:'table',
      restaurant_id:''
    }
  },
  ADD_ORDER_NO(state,order_no){
    state.order_no=order_no;
  },
  ADD_SEARCH_DISHES(state,dishes_name){
    state.dishes_name=dishes_name;
  },
  ADD_TAB(state,tab){
    state.selectedTabs.push(tab);
  },
  REMOVE_TAB(state,index){
    state.selectedTabs.splice(index,1);
  },
  SET_TAB(state){
    state.selectedTabs=['takeOrder','link','table'];
  },
  ADD_ORDER(state,order){
    state.single_order=order;
  }
  
}
