/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
import axios from "@/axios.js"


export default {

  mutateItem({commit},item){
    commit('ADD_ITEM', item);    
  },
  removeItem({commit},i){
    commit('REMOVE_ITEM',i);
  },
  setOrder({commit},order){
    commit('SET_ORDER',order);
  },
  resetOrder({commit}){
    commit('RESET_ORDER');
  },
  resetItem({commit},i){
    commit('RESET_ITEM',i);
  },
  updateStatusItem({commit},data){
    commit('UPDATE_ITEM_STATUS',data);
  },
  upateStatusOrder({commit},order){
    commit('UPDATE_ORDER_STATUS',order)
  },
  
  setItem({commit},item){
    commit('SET_ITEM',item);
  },
  setRealOrder({commit},order){
    commit('SET_REAL_ORDER',order);
  },
  removeOrder({commit},id){
      commit('REMOVE_ORDER',id);
  },
  addTab({commit},tab){
    commit('ADD_TAB',tab);
  },
  removeTab({commit},index){
    commit('REMOVE_TAB',index);
  },
  setTab({commit}){
    commit('SET_TAB');
  },


  storeOrder(context,data) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/order/store", data)
        .then((response) => {
          
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  
  getOrders(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/order/index/"+data.restaurant_id+"/"+data.type)
        .then((response) => {
          context.commit('ADD_ORDERS',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  getOrder(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/order/get/"+data.restaurant_id+"/"+data.id)
        .then((response) => {
          context.commit('ADD_ORDER',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  
  getTabs(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/order/tabs/"+data.restaurant_id)
        .then((response) => {
          context.commit('ADD_TABS',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },

  getOrdersBySource(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.post("/api/order/get-order-by-source",data)
        .then((response) => {
          context.commit('ADD_ORDERS',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteOrder(context, id){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise((resolve, reject) => {
      axios.delete("/api/order/delete/"+id)
        .then((response) => {
          context.commit('REMOVE_ORDER',response.data.data);
          resolve(response);
        })
        .catch((error) => { reject(error) })
    })
  },
  confirmPassword(context ,password){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');
    return new Promise((resolve,reject)=>{
      axios.post("/api/order/confirmPassword",{password:password})
        .then((response)=>{
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },




  itemStatus(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');
    return new Promise((resolve,reject)=>{
      axios.post("/api/order/item-status",data.item)
        .then((response)=>{
          context.commit('UPDATE_ITEM_STATUS',data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  orderStatus(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');
    return new Promise((resolve,reject)=>{
      axios.post("/api/order/order-status",data)
        .then((response)=>{          

          context.commit('UPDATE_ORDER_STATUS',data);
          console.log(response.data.data);
          if(response.data.data.status=='served'||response.data.data.status=='Delivered')
          {
            console.log(response.data.data.status);
            context.commit('REMOVE_ORDER',response.data.data.id);
          }
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  UpdateOrderType(context ,data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/order/update-order-type",data)
        .then((response)=>{
          context.commit('REMOVE_ORDER',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },

  getOrderNo(context ,restaurant_id){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.get("/api/order/get-order-no/"+restaurant_id)
        .then((response)=>{
          context.commit('ADD_ORDER_NO',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },


  SearchDish(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.get("/api/order/search-dish/"+data.restaurant_id+"/"+data.value)
        .then((response)=>{
          context.commit('ADD_SEARCH_DISHES',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },

  Stripe(context, data){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');

    return new Promise((resolve,reject)=>{
      axios.post("/api/order/stripe",data)
        .then((response)=>{
          resolve(response.data.data);
        })
        .catch((error)=> { reject(error)});
    })
  },
  


}
