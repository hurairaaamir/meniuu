/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default {
  orders:[],
  order:{
    items:[],
    email:'',
    phone:'',
    optional_note:'',
    pay:'',
    change:'',
    table:'',
    subtotal:0,
    total:0,
    tip:0,
    oldTip:0,
    timestamp:'',
    paymentMethod:'',
    cardNo:'',
    mmyycc:'',
    orderType:'table',
    restaurant_id:''
  },
  order_no:'',
  dishes_name:[],
  total_num:'',
  tableTabs:[],
  linkTabs:[],
  selectedTabs:['takeOrder','link','table'],
  single_order:[]
}

