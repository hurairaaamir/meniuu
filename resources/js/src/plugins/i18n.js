import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from '../lang/en.json'
import mx from '../lang/mx.json'

const languages={
    en:en,
    mx:mx
}
const messages=Object.assign(languages);

Vue.use(VueI18n)
export const i18n =new VueI18n({
    locale:'mx',
    fallbackLocale:'en',
    messages:messages
});