import Vue from "vue";
import Router from "vue-router";
import frontendRouter from "./frontendRouter.js";
import backendRouter from "./backendRouter.js";
import store from "./store/store.js";
import { toPairs } from "lodash";

var allRoutes = [];
allRoutes = allRoutes.concat(frontendRouter, backendRouter);

const routes = allRoutes;

Vue.use(Router);

const router = new Router({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});
router.beforeEach((to, from, next) => {
    // if(to.path != from.path){
        // this.Vue.
    // }
    if(to.matched.some(record=> record.meta.requireAuth)){
        if(!store.getters.loggedIn){
            next({name:'page-login'});
            
        }else{
            let role=store.state.user.role;
            
            switch(role){
                case 'cook':
                    (to.matched.some(record=> record.meta.forCook)) ? next() : next({name:'home'});
                    break;
                case "waiter":
                    to.matched.some(record => record.meta.forWaiter)
                        ? next()
                        : next({ name: "home" });
                    break;
                case "cashier":
                    to.matched.some(record => record.meta.forCashier)
                        ? next()
                        : next({ name: "home" });
                    break;
                case "manager":
                    to.matched.some(record => record.meta.forManager)
                        ? next()
                        : next({ name: "home" });
                    break;
                case "delivery_man":
                    to.matched.some(record => record.meta.forDeliveryMan)
                        ? next()
                        : next({ name: "home" });
                    break;
                case "owner":
                    next();
                    break;
            }
            next();
        }
    } else {
        next();
    }
});
router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById("loading-bg");
    if (appLoading) {
        appLoading.style.display = "none";
    }
    // $Progress.finish()
});

export default router;
