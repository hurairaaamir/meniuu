import Orders from "@/views/frontend/Orders";
import Restaurant from "@/views/frontend/Restaurant";
import RestaurantHome from "@/views/frontend/components/restaurant/Home";
import Extras from "@/views/frontend/components/restaurant/Extras";
import AddExtra from "@/views/frontend/components/restaurant/AddExtra";
import SocialLinks from "@/views/frontend/components/restaurant/SocialLinks";
import AddSocialLink from "@/views/frontend/components/restaurant/AddSocialLink";
import Tables from "@/views/frontend/components/restaurant/Tables";
import AddTable from "@/views/frontend/components/restaurant/AddTable";
//  Order
import CurrentOrder from "@/views/frontend/components/orders/CurrentOrders";
import HomeOrder from '@/views/frontend/components/orders/Home';
//Take order 
import Takeorder from "@/views/frontend/Takeorder";
import CategoriesTake from "@/views/frontend/components/take-order/Categories";
import ItemsMenu from "@/views/frontend/components/take-order/Items";
// Bank

import Bank from "@/views/frontend/bank";
import BankHome from "@/views/frontend/components/bank/home";
import BankSuccess from "@/views/frontend/components/bank/success";

// Analytics 
import Analytics from '@/views/frontend/Analytics'
import General from '@/views/frontend/components/analytics/General'
import Money from '@/views/frontend/components/analytics/Money'
import Plates from '@/views/frontend/components/analytics/Plates'
import InventoryAnalytics from '@/views/frontend/components/analytics/Inventory'
//edit menu
import CategoriesMenu from "@/views/frontend/components/restaurant/editMenu/Categories";
import CreateCategoryMenu from "@/views/frontend/components/restaurant/editMenu/CreateCategory";
import ProductsMenu from "@/views/frontend/components/restaurant/editMenu/Products";
import CreateProductMenu from "@/views/frontend/components/restaurant/editMenu/CreateProduct";
import UsageProductsMenu from "@/views/frontend/components/restaurant/editMenu/UsageProducts";

//My Inventory
import Inventory from "@/views/frontend/Inventory";
import InventoryHome from "@/views/frontend/components/inventory/Home";
//My Inventory => product list
import Categories from "@/views/frontend/components/inventory/productList/Categories";
import Products from "@/views/frontend/components/inventory/productList/Products";
import CreateCategory from "@/views/frontend/components/inventory/productList/CreateCategory";
import CreateProduct from "@/views/frontend/components/inventory/productList/CreateProduct";
//My Inventory => product entries
import Entries from "@/views/frontend/components/inventory/Entries/Entries";
import CreateEntries from "@/views/frontend/components/inventory/Entries/CreateEntries";
import EntriesHistory from "@/views/frontend/components/inventory/Entries/History";
import ViewEntry from "@/views/frontend/components/inventory/Entries/ViewEntry";
//My Inventory => product Output
import Output from "@/views/frontend/components/inventory/Output/Output";
import CreateOutput from "@/views/frontend/components/inventory/Output/CreateOutput";
import HistoryOutput from "@/views/frontend/components/inventory/Output/HistoryOutput";
import ViewOutput from "@/views/frontend/components/inventory/Output/ViewOutput";
//My Inventory => Stocks
import ViewStock from "@/views/frontend/components/inventory/stocks/ViewStock";
import Stocks from "@/views/frontend/components/inventory/stocks/Stocks";
//My Inventory => Stock Points
import ViewStockPoints from "@/views/frontend/components/inventory/stocksPoints/ViewStockPoints";
import StockPoints from "@/views/frontend/components/inventory/stocksPoints/StockPoints";

//My Inventory => Notification
import Notifications from "@/views/frontend/components/inventory/notifications/Notifications";
import Team from "@/views/frontend/Team";
//=>Cooks
import TeamList from "@/views/frontend/components/Team/List";
import AddMember from "@/views/frontend/components/Team/AddMember";

//=> Order by Tables
import TakeOrderTables from "@/views/frontend/TakeOrderTables";
//->
import CategoriesOrder from "@/views/frontend/components/take-order-table/Categories";
import CategoryOrder from "@/views/frontend/components/take-order-table/Category";
import CheckoutOrder from "@/views/frontend/components/take-order-table/Checkout";
const routes = [
    {
        path: "",
        component: () => import("./layouts/full-page/fullpage.vue"),
        children: [
            {
                path: "/",
                name: "page-login",
                component: () => import("@/views/pages/Login.vue")
            },
            {
                path: "/login",
                name: "page-login",
                component: () => import("@/views/pages/Login.vue")
            },
            {
                path: "/add-restaurant",
                name: "Add-Restaurant",
                component: require("@/views/frontend/AddRestaurant.vue").default,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                }
            },
            {
                path: "/forgot-password",
                name: "forgot-password",
                component: () => import("@/views/pages/ForgetPassword.vue")
            },
            {
                path: "/reset-password/:email/:token",
                name: "reset-password",
                component: () => import("@/views/pages/ResetPassword.vue")
            },
            
        ]
    },
    {
        path: "",
        component: () => import("./layouts/nav-page/navPage.vue"),
        children: [
            {
                path: "/landing",
                name: "landing",
                component: require("./views/frontend/NewLanding.vue").default,
                meta: { 
                    requireAuth:true ,
                    forManager:true,
                    forOwner:true
                }
                
            },
            {
                path: "/error-404",
                name: "page-error-404",
                component: () => import("@/views/pages/Error404.vue")
            }
        ]
    },

    {
        path: "",
        component: () => import("./layouts/sidebar-page/SidebarPage.vue"),
        children: [
          
            {
                path: "/edit-restaurant/:restaurant_id/restaurant",
                name: "Edit-Restaurant",
                component: require("@/views/frontend/AddRestaurant.vue").default,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                }
            },
            {
                path: "/dashboard",
                name: "Orders",
                component: Orders,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                    forCook:true ,
                    forWaiter:true,
                    forCashier:true,
                    forDeliveryMan:true
                }
            },

            {
                path: "/orders/:restaurant_id/restaurant",
                name: "Orders",
                component: Orders,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                    forCook:true ,
                    forWaiter:true,
                    forCashier:true,
                    forDeliveryMan:true
                },
                children: [
                    { path: "", component: HomeOrder },
                    { path: "current-order/:type", name: "taken-orders", component: CurrentOrder },
                    // { path: ":category", component: ItemsMenu }
                ]
            },
            {
                path: "/take-order/:restaurant_id/restaurant",
                name: "TakeOrder",
                component: Takeorder,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                    forWaiter:true,
                    forCashier:true,
                },
                children: [
                    { path: "", component: CategoriesTake },
                    { path: ":category", component: ItemsMenu }
                    //   {path: ':teamCategory/new', component: AddMember},
                    //   {path: ':teamCategory/edit/:id', component: AddMember},
                ]
            },
            {
                path: "/restaurant/:restaurant_id/restaurant",
                name: "Restaurant",
                component: Restaurant,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                },
                children: [
                    { path: "", component: RestaurantHome },
                    { path: "extras", component: Extras },
                    { path: "extras/new", component: AddExtra },
                    { path: "extras/edit/:id", component: AddExtra },
                    { path: "social-links", component: SocialLinks },
                    { path: "social-links/new", component: AddSocialLink },
                    { path: "social-links/edit/:id", component: AddSocialLink },
                    { path: "tables", component: Tables },
                    { path: "tables/new", component: AddTable },
                    { path: "tables/edit/:id", component: AddTable },
                    //edit menu
                    { path: "menu", component: CategoriesMenu },
                    { path: "menu/new", component: CreateCategoryMenu },
                    {
                        path: "menu/:category/edit",
                        component: CreateCategoryMenu
                    },
                    { path: "menu/:category/", component: ProductsMenu },
                    {
                        path: "menu/:category/new",
                        component: CreateProductMenu
                    },
                    {
                        path: "menu/:category/:product/edit",
                        component: CreateProductMenu
                    },
                    {
                        path: "menu/:category/:product/products",
                        component: UsageProductsMenu
                    }
                ]
            },
            {
                path: "/inventory/:restaurant_id/restaurant",
                name: "Inventory",
                component: Inventory,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                },
                children: [
                    { path: "", component: InventoryHome },
                    { path: "categories", component: Categories },
                    { path: "categories/new", component: CreateCategory },
                    {
                        path: "categories/:category/edit",
                        component: CreateCategory
                    },
                    { path: "categories/:category", component: Products },
                    {
                        path: "categories/:category/:product",
                        component: CreateProduct
                    },
                    {
                        path: "categories/:category/new",
                        component: CreateProduct
                    },
                    {
                        path: "categories/:category/:product/edit",
                        component: CreateProduct
                    },
                    { path: "entries", component: Entries },
                    { path: "entries/new", component: CreateEntries },
                    { path: "entries/history", component: EntriesHistory },
                    { path: "entries/history/:entry", component: ViewEntry },
                    // Ouput Entry
                    { path: "output", component: Output },
                    { path: "output/new", component: CreateOutput },
                    { path: "output/history", component: HistoryOutput },
                    { path: "output/history/:entry", component: ViewOutput },
                    //stocks
                    { path: "stocks", component: Stocks },
                    { path: "stocks/:id", component: ViewStock },
                    //stock points
                    { path: "stock-points", component: StockPoints },
                    { path: "stock-points/:id", component: ViewStockPoints },
                    //notifications
                    { path: "notifications", component: Notifications }
                ]
            },
            {
                path: "/team/:restaurant_id/restaurant",
                name: "Team",
                component: Team,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                },
                children: [
                    { path: "", redirect: "/team/:cooks" },
                    { path: ":teamCategory", component: TeamList },
                    { path: ":teamCategory/new", component: AddMember },
                    { path: ":teamCategory/edit/:id", component: AddMember }
                ]
            },
            {
                path: "/analytics/:restaurant_id/restaurant",
                name: "Analytics",
                component: Analytics,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                },
                children: [
                    // { path: "", component: General },
                    { path: "general", component: General },
                    { path: "money", component: Money },
                    { path: "plates", component: Plates },
                    { path: "inventory", component: InventoryAnalytics },
                ]
            },
            {
                path: "/bank/:restaurant_id/restaurant",
                name: "Bank",
                component: Bank,
                meta: { 
                    requireAuth:true ,
                    forOwner:true,
                    forManager:true,
                },
                children: [
                    { path: "", component: BankHome },
                    { path: "success", component: BankSuccess },
                ]
            },
            //   {
            //     path: '/login',
            //     name: 'Login',
            //     component: Login
            // },
            // {
            //     path: '/pages/login',
            //     name: 'page-login',
            //     component: () => import('@/views/pages/Login.vue')
            // },
            {
                path: "/pages/error-404",
                name: "page-error-404",
                component: () => import("@/views/pages/Error404.vue")
            }
        ]
    },
    {
        path: "/logout",
        name: "logout",
        component: () => import("@/views/pages/Logout.vue")
    },
    {
        path: "",
        component: () => import("./layouts/take-order-table/take-order-table.vue"),
        children: [
            {
                path: "/restaurant/:restaurant_id/table/:table_name/:table_id",
                name: "Table-order",
                component: TakeOrderTables,
                children: [
                  { path: "", component: CategoriesOrder },
                  { path: ":category/:category_name/checkout", component: CheckoutOrder },
                  { path: ":category/:category_name", component: CategoryOrder },
                ]
            },
            {
                path: "/:restaurant_name/restaurant/:restaurant_id/:link_name/link/:link_id",
                name: "Table-order",
                component: TakeOrderTables,
                children: [
                  { path: "", component: CategoriesOrder },
                  { path: ":category/:category_name/checkout", component: CheckoutOrder },
                  { path: ":category/:category_name", component: CategoryOrder },
                ]
            }
        ]
    }
];

export default routes;
