/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import App from './App.vue'

// Vuesax Component Framework
import Vuesax from 'vuesax'

// const GlobalEvent = require('./event');
// const GlobalEvent = class GlobalEvent {
//   constructor() {
//     this.vue = new Vue();
//   }

//   fire(event, data = null) {
//     this.vue.$emit(event, data);
//   }

//   listen(event, callback) {
//     this.vue.$on(event, callback);
//   }
// }
// window.Event = new GlobalEvent();

Vue.use(Vuesax)
import VueHtml2pdf from 'vue-html2pdf'
Vue.use(VueHtml2pdf)
Vue.prototype.$eventHub = new Vue(); // Global event bus

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: '#ff2865',
  failedColor: 'red',
  thickness: '3px',
  height: '4px',
  
});

// Theme Configurations
import '../themeConfig.js'


// Globally Registered Components
import './globalComponents.js'


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'





// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)



import Errors from '../../assets/utils/Errors';
window.Errors =Errors;



// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'





// laravel Ecoho

import Echo from 'laravel-echo';


window.Pusher = require('pusher-js');

window.Echo = new Echo({
  broadcaster: 'pusher',
  key: '0a6be60498548bd8679b',
  cluster: 'us3',
  forceTLS: true,
  authEndpoint: 'broadcasting/auth',
  auth:{
    headers:{
          Authorization: `Bearer ${store.state.token}`
      }
  }
});


import { i18n } from './plugins/i18n'






// sudo iptables -A INPUT -p tcp -i eth0 --dport 6001 -j ACCEPT


import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

VueClipboard.config.autoSetContainer = true ;

import {mapGetters} from "vuex";

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    i18n,
    // GlobalEvent,
    mounted() {
      var style = document.createElement('style');
      style.id = "theme"
      style.innerHTML = `
        :root{
          --main-color: ${this.$store.state.theme.mainColor};
          --hover-color: ${this.$store.state.theme.hoverColor};
          --sidebar-color: ${this.$store.state.theme.sidebarColor};
          --body-color: ${this.$store.state.theme.bodyColor};
          --font-color: ${this.$store.state.theme.fontColor};
          --btn-color: ${this.$store.state.theme.btnColor}
        }
    `;
      document.head.appendChild(style);
    },
    computed: {
      ...mapGetters([
        'getTheme'
      ]),
      // root(){
      //   console.log(getTheme);
      //   return   '--main-color:'+ this.getTheme.mainColor
      // }
    },
    watch: {
      '$store.state.theme.mainColor': function(){
        // console.log(this.$store.state.theme.mainColor);
         let theme = this.$store.state.theme
        document.getElementById('theme').innerHTML  = `
        :root{
          --main-color: ${theme.mainColor};
          --hover-color: ${theme.hoverColor};
  
        }
    `;
      },
      '$store.state.theme.hoverColor': function(){
        // console.log(this.$store.state.theme.mainColor);
         let theme = this.$store.state.theme
        document.getElementById('theme').innerHTML  = `
        :root{
          --main-color: ${theme.mainColor};
          --hover-color: ${theme.hoverColor};
  
        }
    `;
      }
    },
    render: h => h(App)
}).$mount('#app')


// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key:'ABCDEFG',
//     cluster:'mt1',
//     wsHost:'meniuu.dev.rigrex.com:6001',
//     // wsPort:6001,
//     wssPort: 6001,
//     disableStats: true,
//     enabledTransports: ['ws','wss'],
//     // encrypted: true,
//     // forceTLS: false,

//     auth:{
//       headers:{
//           Authorization: `Bearer ${token}`
//       }
//     }
    
// });
