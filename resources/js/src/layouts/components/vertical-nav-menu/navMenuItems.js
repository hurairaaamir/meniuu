/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  {
    url: "/admin/dashboard",
    name: "Dashboard",
    slug: "dashboard",
    icon: "HomeIcon",
  },
  {
    url: '/apps/user/user-list',
    name: "Restaurants List",
    slug: "app-user-list",
    icon: "UserIcon",
  },
  // {
  //   url: "/page2",
  //   name: "Page 2",
  //   slug: "page2",
  //   icon: "FileIcon",
  // },
  // {
  //   url: null,
  //   name: "User",
  //   icon: "UserIcon",
  //   i18n: "User",
  //   submenu: [
  //     {
  //       url: '/apps/user/user-list',
  //       name: "Restaurants List",
  //       slug: "app-user-list",
  //       i18n: "List",
  //     },
      // {
      //   url: '/apps/user/user-view/268',
      //   name: "View",
      //   slug: "app-user-view",
      //   i18n: "View",
      // },
      // {
      //   url: '/apps/user/user-edit/268',
      //   name: "Edit",
      //   slug: "app-user-edit",
      //   i18n: "Edit",
      // },
  //   ]
  // },
  // {
  //   url: "/client",
  //   name: "Clients",
  //   slug: "clients",
  //   icon: "UsersIcon",
  // },
  // {
  //   url: null,
  //   name: "Projects",
  //   icon: "BriefcaseIcon",
  //   i18n: "Todo",
  //   submenu: [
  //     {
  //       url: '/project/current-projects',
  //       name: "Current Projects",
  //       slug: "current-projects",
  //       i18n: "List",
  //     },
  //     {
  //       url: '/project/all-projects',
  //       name: "all projects",
  //       slug: "all-projects",
  //       i18n: "View",
  //     },
  //     {
  //       url: '/apps/user/user-edit/268',
  //       name: "Edit",
  //       slug: "app-user-edit",
  //       i18n: "Edit",
  //     },
  //   ]
  // },
]
