const routes = [
    {
        path: '/admin',
        meta: { requireAuth: true },
        component: () => import('./layouts/main/Main.vue'),
        children: [
            {
                path: '/admin/dashboard',
                name: 'dashboard',
                component: () => import('./views/dashboard.vue')
            },
            {
                path: '/page2',
                name: 'page-2',
                component: () => import('./views/Page2.vue')
            },  
        ],
    },

    // =============================================================================
    // UI ELEMENTS
    // =============================================================================
    {
        path: '/admin',
        component: () => import('./layouts/main/Main.vue'),
        meta: { requireAuth: true },
        children: [
            {
                path: '/apps/user/user-list',
                name: 'app-user-list',
                component: () => import('@/views/backend/user/user-list/UserList.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'User' },
                        { title: 'List', active: true },
                    ],
                    pageTitle: 'User List',
                    rule: 'editor'
                },
            },
            {
                path: '/apps/user/user-view/:userId',
                name: 'app-user-view',
                component: () => import('@/views/backend/user/UserView.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'User' },
                        { title: 'View', active: true },
                    ],
                    pageTitle: 'User View',
                    rule: 'editor'
                },
            },

            {
                path: '/apps/user/user-edit/:userId',
                name: 'app-user-edit',
                component: () => import('@/views/backend/user/user-edit/UserEdit.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'User' },
                        { title: 'Edit', active: true },
                    ],
                    pageTitle: 'User Edit',
                    rule: 'editor'
                },
            },
        ],

    },

    // {
    //   path: '/admin',
    //   component: () => import('./layouts/main/Main.vue'),
    //   children: [
    //         {
    //           path: '/project/current-projects',
    //           name: 'current-project',
    //           component: () => require('./views/backend/project/current-projects/currentProjects.vue').default,
    //           meta: {
    //               breadcrumb: [
    //                   { title: 'Home', url: '/' },
    //                   { title: 'Projects'},
    //                   { title: 'current projects' },
    //               ],
    //               pageTitle: 'Current Projects',
    //               rule: 'editor'
    //           },
    //       },

    //   ],

    // },









    {
        path:'/logout',
        name:'logout',
        component:require('./views/pages/Logout.vue').default,
    },
    {
        path:'/login',
        name:'login',
        component:require('./views/pages/Login.vue').default,
    },
    {
        path:'/register',
        name:'register',
        component:require('./views/pages/Register.vue').default,
    },
    {
        path: '*',
        // redirect: '/pages/error-404'
        component: () => import('@/views/pages/Error404.vue'),
    }   
]

  
export default routes