<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable=[
        'name',
        'address',
        'delivery',
        'home_delivery',
        'logo',
        'phone',
        'currency',
        'description',
        'user_id',
        "inventory_permission",
        "analytics_permission",
        "status"
    ];

    public function employee(){
        return $this->hasMany(User::class);
    }

    public function account(){
        return $this->hasOne(Account::class);
    }
}
