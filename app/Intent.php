<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intent extends Model
{
    protected $fillable=['payload'];
}
