<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSecret extends Model
{
    protected $fillable=['secret_id','restaurant_id'];
}
