<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable=['account_id','email','restaurant_id','status'];

    public function restaurant(){
        return $this->belongsTo(Restaurant::class);
    }     
}
