<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable=["ch_description","extras","price","name","quantity","note","total","order_id","status","category_name","restaurant_id"];

    public function order(){
        return  $this->belongsTo(Order::class);
    }

    public function dish(){
        return $this->hasOne(Dish::class);
    }
}

