<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable=['name','come_with','description','price','image1','image2','image3','image4',
    'choice1','choice2','choice3','choice4','ch1_description','ch2_description','ch3_description','ch4_description','extras',
    'send_to','dish_category_id','status','restaurant_id'];

    public function DishCategories(){
        return $this->belongsTo(DishCategory::class);
    }

    public function DishIngredients(){
        return $this->hasMany(DishIngredient::class);
    }
}
