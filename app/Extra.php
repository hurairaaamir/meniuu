<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
    protected $fillable=['name','price','restaurant_id'];

    public function dishes(){
        $this->belongsToMany(Dish::class);
    }
}
