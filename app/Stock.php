<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable=['restaurant_id','message','type'];

    public function stock_entries(){
        return $this->hasMany(StockEnteries::class);
    }
}
