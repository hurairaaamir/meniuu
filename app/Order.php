<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=["email","phone","optional_note","pay","change","table",
                        "subtotal","restaurant_id","status","timestamp",
                    "paymentMethod","cardNo","mmyycc","orderType","total",
                    "tipAmount","tip","serviceMode","address","source","order_code",
                    "customer_name","stripe_id","active","created_at"];

    public function items(){
        return $this->hasMany(OrderItem::class);
    }
}
