<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\StockEnteryEvent' => [
            'App\Listeners\StockEnteryListener',
        ],
        'App\Events\MailEvent' => [
            'App\Listeners\MailListner',
        ],
        'App\Events\FoodReadyEvent' => [
            'App\Listeners\FoodReadyListner',
        ],
        'App\Events\StockEmailEvent' => [
            'App\Listeners\StockEmailListner',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
