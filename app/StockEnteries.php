<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockEnteries extends Model
{
    protected $fillable=['product','category','measuring_unit','quantity','stock_id',"order_id","restaurant_id"];

    public function stock(){
        return $this->belongsTo(Stock::class);
    }
}
