<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishCategory extends Model
{
    protected $fillable =['dish_name','image','status','restaurant_id'];

    public function dishes(){
        return $this->hasMany(Dish::class);
    }
}
