<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable=['category_name','restaurant_id'];

    public function products(){
        return $this->hasMany(Product::class);
    }
}
