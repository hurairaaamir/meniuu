<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationEmails extends Model
{
    protected $fillable=['min_email','max_email','opt_email','restaurant_id'];
}
