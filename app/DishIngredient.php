<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishIngredient extends Model
{
    protected $fillable=['product','category','measuring_unit','quantity','dish_id','restaurant_id'];

    public function dish(){
        return $this->belongsTo(Dish::class);
    }
}

