<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Events\MailEvent;
use Illuminate\Support\Facades\Mail;
use App\Mail\ChangePassword;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;



class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            "email"=>"required",
            "password"=>"required"
        ]);

        $http = new \GuzzleHttp\Client;


        try {
            $response = $http->post($this->getUrl().'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => config('services.passport.key'),
                    'username' => $request->email,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody();

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }

            return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role'=> 'admin'
        ]);

        return response()->json([
            "data"=>$user
        ]);
    }

    public function logout()
    {
        auth()->guard('api')->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }

    public function createCustomer(Request $request){
        
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role'=> 'customer'
        ]);
    }

    public function forgetPassword(Request $request){
        
        $request->validate([
            "email"=>"required|email"
        ]); 

        $user=User::where('email',$request->email)->first();
        

        if($user){
            $token=($user->token)?$user->token:$this->random(20);
            
            $url=$this->getUrl()."/#/reset-password/".$user->email."/".$token;

            $user->update([
                "token"=>$token
            ]);
            event(new MailEvent($user->email,$url));

            return response()->json([
                "data"=>"email is sent"
            ]);

        }else{
            return response()->json([
                "errors"=>["onEmail"=>"Email not found"]
            ],405);
        }
                
    }

    public function resetPassword(Request $request){
        $request->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);
        $user=User::where('email',$request->email)->where('token',$request->token)->first();
        $user->update([
            "password"=>Hash::make($request->password),
            "token"=>NULL
        ]);
        return response()->json([
            "data"=>"password changed"
        ]);
        
    }

    private function getUrl(){
        return url("/")=="http://127.0.0.1:8000"? "http://127.0.0.1:8001" : "https://restaurante.meaal.mx";
    }

    private function random($size)
    {
        $permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, $size);
    }

    public function mail(){
        // event(new MailEvent("mdawoodch16@gmail.com","https://rigrex"));
        foreach (['muhammadhurairaaamir@gmail.com'] as $recipient) {
            Mail::to($recipient)->send(new ChangePassword("https://rigrex"));
        }       
    }

}