<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\DishCategory;
use App\Table;
use App\SocialMedia;
use App\Extra;
use App\Account;

class RestaurantController extends Controller
{
    public function editRestaurant(Request $request){
        $request->validate([
            "name"=>"required",
            "description"=>"required",
            "address"=>"required",
            "phone"=>"required",
            "currency"=>"required",
            "delivery"=>"required",
            "id"=>"required"
        ]);
        $restaurant=Restaurant::findOrFail($request->id);
        if($request->logo){
            $image=$this->storeImage($request->logo);
            $restaurant->update([
                "name"=>$request->name,
                "description"=>$request->description,
                "phone"=>$request->phone,
                "currency"=>$request->currency,
                "delivery"=>$request->delivery,
                "address"=>$request->address,
                "logo"=>$image,
            ]);
        }else{
            $restaurant->update([
                "name"=>$request->name,
                "description"=>$request->description,
                "phone"=>$request->phone,
                "currency"=>$request->currency,
                "address"=>$request->address,
                "delivery"=>$request->delivery,
            ]);
        }


        return response()->json([
            "data"=>$restaurant
        ]);

    }
    public function store(Request $request){

        $request->validate([
            "name"=>"required",
            "description"=>"required",
            "address"=>"required",
            "phone"=>"required",
            "currency"=>"required",
            "delivery"=>"required",
            "logo"=>"required|image"
        ]);

        $image=$this->storeImage($request->logo);

        $restaurant=Restaurant::create([
            "name"=>$request->name,
            "description"=>$request->description,
            "phone"=>$request->phone,
            "currency"=>$request->currency,
            "delivery"=>$request->delivery,
            "address"=>$request->address,
            "logo"=>$image,
            "user_id"=>auth()->id()
        ]);

        return response()->json([
            "data"=>$restaurant
        ]);

        // dd($request->all());
    }
    protected function storeImage($image){
        $image_name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/restaurant/';
        $image->move($destinationPath, $image_name);
        
        $path='/images/restaurant/'.$image_name;

        return $path;
    }

    public function sendMessage(Request $request){
        $message=$request->message;
        
        event(new \App\Events\RestaurantOpenEvent($message,$request->user_id));
        // ->toOthers();

        return response()->json([
            "user"=>$message
        ]);
        
    }

    public function show(Request $request){
        $restaurant=Restaurant::where('user_id',auth()->id())->first();
        
        return response()->json([
            "data"=>$restaurant
        ]);
    }

    public function getCount($restaurant_id){
        $dishCategoryNo=DishCategory::where('restaurant_id',$restaurant_id)->count();
        $tableNo=Table::where('restaurant_id',$restaurant_id)->count();
        $socialMediaNo=SocialMedia::where('restaurant_id',$restaurant_id)->count();
        $extraNo=Extra::where('restaurant_id',$restaurant_id)->count();
        
        return response()->json([
            "data"=>[
                "dish_category_no"=>$dishCategoryNo,
                "table_no"=>$tableNo,
                "social_media_no"=>$socialMediaNo,
                "extra_no"=>$extraNo
            ]
        ]);
    }
    public function get($id){

        $restaurant=Restaurant::findOrFail($id);

        return response()->json([
            "data"=>$restaurant
        ]);
        
    }

    public function changePermission(Request $request){
        $restaurant=Restaurant::findOrFail($request->restaurant_id);

        if($request->permission=='inventory'){
            if($restaurant->inventory_permission == 'no'){
                $restaurant->update([
                    "inventory_permission"=>"yes"
                ]);
            }else{
                $restaurant->update([
                    "inventory_permission"=>"no"
                ]);
            }
        }else if($request->permission == 'analytics'){
            if($restaurant->analytics_permission == 'no'){
                $restaurant->update([
                    "analytics_permission"=>"yes"
                ]);
            }else{
                $restaurant->update([
                    "analytics_permission"=>"no"
                ]);
            }
        }

        return response()->json([
            "data"=>$restaurant
        ]);
    }
    public function changeStatus(Request $request){
        $restaurant=Restaurant::findOrFail($request->restaurant_id);
        $restaurant->update([
            "status"=>$request->status
        ]);
        return response()->json([
            "data"=>$restaurant->status
        ]);
    }

    public function getAccount($restaurant_id){
        $account=Account::where('restaurant_id',$restaurant_id)->where('status','active')->first();

        return response()->json([
            "data"=>$account
        ]);
    }
}

