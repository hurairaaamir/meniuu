<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Cartalyst\Stripe\Stripe;
use App\Intent;
use Stripe\Stripe;
use App\Account;
use App\Restaurant;
use App\OrderSecret;
use App\Dish;
use App\Extra;
use Exception;
// https://receive-smss.com/sms/12127319863/

class StripeController extends Controller
{
    public function checkout(Request $request){
        Stripe::setApiKey(config('services.stripe.live_secret'));
        
        $stripe_account = \Stripe\Account::create([
            'type' => 'express',
        ]);

        $account=Account::where('restaurant_id',$request->restaurant_id)->first();
        
        if(!$account){
            $account=Account::create([
                "account_id"=>$stripe_account->id,
                "restaurant_id"=>$request->restaurant_id       
            ]);
        }else{
            $account->update([
                "account_id"=>$stripe_account->id,
                "restaurant_id"=>$request->restaurant_id
            ]);
        }

        $account_links = \Stripe\AccountLink::create([
            'account' => $stripe_account->id,
            'refresh_url' => $this->getUrl().'/#/bank/'.$request->restaurant_id.'/restaurant',
            'return_url' => $this->getUrl().'/api/bank/success/'.$request->restaurant_id,
            'type' => 'account_onboarding',
        ]);

        return redirect($account_links->url);

    }


    public function ConnectSuccess($restaurant_id){

        $account=Account::where('restaurant_id',$restaurant_id)->first();

        Stripe::setApiKey(config('services.stripe.live_secret'));
        $stripe_account=\Stripe\Account::retrieve($account->account_id);

        $status=($stripe_account->charges_enabled)?'active':'inactive';

        $account->update([
            'account_id'=>$stripe_account->id,
            'email'=>$stripe_account->email,
            'status'=>$status
        ]);

        return redirect($this->getUrl().'/#/bank/'.$restaurant_id.'/restaurant');        
    }


    
    public function payIntent(Request $request){
        
        if($request->paymentMethod=='card'&&$request->sendBy!='employee'){
            if(!$this->priceConfirm($request)){
                return response()->json(['errors' => ["invalid request"] ], 422);
            }
        }

        $rest=Restaurant::findOrFail($request->restaurant_id);

        
        if($rest->status!='open'){
            return response()->json([
                'errors' => ['restaurantClose'=>"Sorry Restaurant is close"],
            ], 422);
        }
        
        if($request->source=='link'){
            $request->validate([
                "serviceMode"=>"required",
                "customer_name"=>"required",
                "phone"=>"required"
            ]);
            if(!$request->receipt){
                $request->validate([
                    "email"=>"required|email"
                ]);
            }

            if($request->serviceMode=='Delivery'){
                $request->validate([ 
                    "address"=>"required",
                    "customer_name"=>"required",      
                    "phone"=>"required",
                ]);
            }
            if($request->serviceMode=='PickUp'){
                $request->validate([ 
                    "phone"=>"required",
                    "email"=>"required|email"
                ]);
            }
        }
        
        if($request->paymentMethod=='card'&&(!$request->receipt)){
            $request->validate([
                "email"=>"required|email",
            ]);
        }else if($request->paymentMethod=='cash'){
            $request->validate([
                "pay"=>"required",
            ]);
        }

        Stripe::setApiKey(config('services.stripe.live_secret'));
        $account=Account::where('restaurant_id',$request->restaurant_id)->first();

        
        if(in_array($request->currency,['USD','MXN'])){
            $currency= strtolower($request->currency);
        }else{
            return response()->json([
                'errors' => [],
            ], 422); 
        }
        if(!$account){
            return response()->json([
                'errors' => ['nostripe'=>["stripe is not setup for the restaurant"]],
            ], 422);
        }
        if($account->status=!'active'){
            return response()->json([
                'errors' => ['nostripe'=>["stripe is not setup for the restaurant"]],
            ], 422);
        }

        $amount=round(((float)$request->amount)*100); 
        $fee=round(((float)$amount)*0.05);

        $paymentIntent = \Stripe\PaymentIntent::create([
            'amount' => $amount,
            'currency' => $currency,
            'transfer_data' => ['destination' => $account->account_id],
            'application_fee_amount' => $fee,
            "receipt_email"=>$request->email,
            "metadata"=>$request->data
          ]);

          OrderSecret::create([
              "restaurant_id"=>$request->restaurant_id,
              "secret_id"=>$paymentIntent->id
          ]);

          return response()->json([
            'publishableKey' => config('services.stripe.live_secret'),
            "i"=>$paymentIntent->id,
            'clientSecret' => $paymentIntent->client_secret,
          ]);
    }

    private function priceConfirm(Request $request){
        $amount=$request->amount;
        $calAmmount=0;
        foreach($request->items as $item){
            $dish=Dish::select('price')->where('id',$item["id"])->where('restaurant_id',$request->restaurant_id)->first();
            $calAmmount+=( (float)$dish->price * $item["quantity"]);
            foreach($item["extras"] as $extra){
                $extra=Extra::select('price')->where('restaurant_id',$request->restaurant_id)->where('name',$extra["name"])->first();
                $calAmmount+=( (float)$extra->price * $item["quantity"]);
            }
        }
        return ($amount>=$calAmmount) ? true:false;
    }


    public function verifyStripeEmail(Request $request)
    {
        Stripe::setApiKey(config('services.stripe.live_secret'));

        $acc=null;
        $accounts = \Stripe\Account::all();
        return response()->json([
            "strpe"=>$accounts
        ]);
        foreach($accounts->data as $account){
            if($account->email==$request->email){
                try{
                    if($account->capabilities->transfers=='active'){
                        $acc=$account;
    
                    }else{
                        return response()->json([
                            'errors' => ['account'=>"account not connected "],
                        ], 422);
                    }
                }catch(Exception $e){
                    return response()->json([
                        'errors' => ['account'=>"account not connected "],
                    ], 422);
                }
            }
        }
        if($acc==null){
            return response()->json([
                'errors' => ['notfound'=>["email not found , type correct new email or try to reconnect your stripe account"]],
            ], 422);
        }else{
            $account=Account::where('restaurant_id',$request->restaurant_id)->first();
            if($account){
                $account->update([
                    'account_id'=>$acc->id,
                    'email'=>$acc->email,
                    'restaurant_id'=>$request->restaurant_id
                ]);
            }else{
                $account=Account::create([
                    'account_id'=>$acc->id,
                    'email'=>$acc->email,
                    'restaurant_id'=>$request->restaurant_id
                ]);
            }
        }
        return response()->json([
            "data"=>$account
        ]);
    }



    public function webhook(Request $request){
        $payload = $request->getContent();
        $sig_header = $request->header('stripe-signature');
        
        
        $event = null;

        // tail -f storage/logs/laravel.log

        try {

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, 'whsec_FjJsFWK8wzqMdzu6e921VSyUpHpGbhJd'
            );
            
        } catch(\UnexpectedValueException $e) {
            // Invalid payload.
            return response()->json([
                "error"=>"Invalid payload"
            ],400);
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            return response()->json([],400);
        }
    
        if ($event->type == 'payment_intent.succeeded') {
            
            Intent::create([
                "payload"=>$event
            ]);

        }
        return response()->json([
            "succeess"
        ],200);
    }

    public function getIntent(){
        $intents=Intent::all();
        return $intents;
    }


    private function getUrl(){
        return url("/")=="http://127.0.0.1:8000"? "http://127.0.0.1:8000" : "https://restaurante.meaal.mx";
    }
}














        // dd('here');
        // try{
        //     $stripe= Stripe::make('sk_test_51HOjzsC1zYIo0nPVtGcQectayrFTWlHpuLrEq2a9k556QtlIYMOJksjiPXZDr5lDFFbezTywBwWobnXakLRvcw5a00wkFxG1U5');
        //     $charge = $stripe->charges()->create([
        //         'amount'               => 30.00,
        //         'currency'             => 'USD',
        //         'source'=>$request->stripeToken,
        //         "description"=>"this is test description",
        //         'receipt_email'=>$request->email,
        //         'metadata'=>[
        //             'data1'=>'metadata 1',
        //             'data2'=>'metadata 2',
        //             'data3'=>'metadata 3',
        //             'data4'=>'metadata 4',
        //         ]
                
        //     ]);
        // }catch(Exception $e){

        // }
        // dd($charge);


        // return response()->json([
        //     "data"=>$charge
        // ]);
        // dd('here');


        





        // live PUblic key =pk_live_51IBBiDGMLSfhNJYIiP6UiIlViyx5VOPNCshdtiQg8g5ByDj2tofLt7ZQPn47EtVd7GlAQSrfZmF1IxWuT2TAdgpq00N5WN8ttp

        // live secrete=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














        // test public = pk_test_51HcZhRAcCMI2K7rs5nNCMuTKWt9sUvf03mnHJ1NzTPbwHWtzcOvnBqDdJZdgPJoxT5ZuGQRSu1yogTrX6mrarxRZ003qVVrYLv

        // secrete= sk_test_51HcZhRAcCMI2K7rsH60yWDdNq4Vltnhk56KE03b4p3Kh2R2A1WaP2ESZdTGa2sp0g939t5wdgHLUdSkKZHduDa9g00ksqJpt0r
