<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;
use App\Profile;
use Intervention\Image\Facades\Image;
use App\Restaurant;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::where('role','owner')->orderBy('users.id','DESC')->get();
        return response()->json([
            "data"=>$users
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::findOrFail($id);
        $data = new UserResource($user);

        return $data;

        $user=User::join('restaurants','users.id','restaurants.user_id')
            ->where('restaurants.id','=',$id)->first();

        return response()->json([
            "data"=>$user
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string','min:8','confirmed'],
            "image"=>'required|image|mimes:jpg,png,jpeg|max:4000',
        ]);

        $image='';
        if($request->image){
            $image=$this->storeImage($request->image);

            $user=User::create([
                "name"=>$request->name,
                "email"=>$request->email,
                "password"=>Hash::make($request->password),
                "image"=>$image,
                'role'=>'owner'
            ]);

        }else{
            $user=User::create([
                "name"=>$request->name,
                "email"=>$request->email,
                "password"=>Hash::make($request->password),
                'role'=>'owner'
            ]);
        }

        return response()->json([
            "user"=>$user
        ]);

    }

    protected function storeImage($image){

        $image_name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/owner/';
        // $image->move($destinationPath, $image_name);
        $img = Image::make($image->getRealPath());
        $img->resize(500, 500, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$image_name);
        
        $path='/images/owner/'.$image_name;

        return $path;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8|confirmed',
        ]);
        
        $user=User::findOrFail($request->id);
        if($user->email!=$request->email){
            $request->validate([
                "email"=>"unique:users"
            ]);
        }

        if($request->image){
            // @unlink($request->image);
            $image=$this->storeImage($request->image);
            $user->update([
                "name"=>$request->name,
                "role"=>$request->role,
                "email"=>$request->email,
                "password"=>Hash::make($request->password),
                "image"=>$image
            ]);
        }else{
            $user->update([
                "name"=>$request->name,
                "role"=>$request->role,
                "email"=>$request->email,
                "password"=>Hash::make($request->password),
            ]);
        }

        return response()->json([
            "user"=>$user
        ]);        
    }

    public function editOwner(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => 'required|string|min:8|confirmed',
        ]);
        $user=User::findOrFail($request->id);
        
        if($request->image){
            // @unlink($request->image);
            $image=$this->storeImage($request->image);
            $user->update([
                "name"=>$request->name,
                "email"=>$request->email,
                "password"=>Hash::make($request->password),
                "image"=>$image
            ]);
        }else{
            $user->update([
                "name"=>$request->name,
                "email"=>$request->email,
                "password"=>Hash::make($request->password),
            ]);
        }

        return response()->json([
            "user"=>$user
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function auth_user(){
        $user=auth()->user();

        $role=['cook','waiter','cashier','manager','delivery_man'];
        
        if($user->role=='owner'){
            $user->restaurant;        
        }else if(in_array($user->role,$role)){
            $restaurant=Restaurant::where('id',$user->restaurant_id)->first();
            $user->restaurant=$restaurant;
        }
        
        return response()->json([
            "data"=>$user
        ]);
    }
    public function team_store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        if($request->role=='admin'){
            return response()->json([
                "error"=>"invalid request"
            ],405);
        }
        $img='';
        switch($request->role){
            case 'delivery_man':
                $img='/images/users/delivery_man.jpeg';
            break;
            case 'cashier':
                $img='/images/users/cashier.jpeg';
            break;
            case 'manager':
                $img='/images/users/manager.png';
            break;
            case 'waiter':
                $img='/images/users/waiter.jpeg';
            break;
            case 'cook':
                $img='/images/users/cook.jpeg';
            break;
        }
        $user=User::create([
            "name"=>$request->name,
            "email"=>$request->email,
            "password"=>Hash::make($request->password),
            'role'=>$request->role,
            'restaurant_id'=>$request->restaurant_id,
            'image'=>$img
        ]);
        
        return response()->json([
            "data"=>$user
        ]);
    }
    
    public function fetchTeam($type,$restaurant_id){
        $teams=User::where('restaurant_id',$restaurant_id)->where('role',$type)->get();
        return response()->json([
            "data"=>$teams
        ]);
    }

    public function delete($id){
        $user=User::findOrFail($id);
        $user->delete();
        return response()->json([
            "data"=>$id
        ]);
    }
    


}
