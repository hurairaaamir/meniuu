<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DishResource;
use Intervention\Image\Facades\Image;
use App\Dish;

class DishController extends Controller
{
    public function index($restaurant_id,$category_id){


        $dishes=DishResource::collection(Dish::where('dish_category_id',$category_id)->get());

        return response()->json([
            "data"=>$dishes
        ]);
    }
    public function indexActive($restaurant_id,$category_id){
        // ->where('status','active')

        $dishes=DishResource::collection(Dish::where('dish_category_id',$category_id)->where("status","active")->get());

        return response()->json([
            "data"=>$dishes
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'name'=>"required",
            'description'=>"required",
            'price'=>"required",
            "image1"=>'required|image|mimes:jpg,png,jpeg|max:4000',
            'send_to'=>"required",
        ]);
        if($request->image2){
            $request->validate([
                "image2"=>'image|mimes:jpg,png,jpeg|max:4000',
            ]);
        }
        if($request->image3){
            $request->validate([
                "image3"=>'image|mimes:jpg,png,jpeg|max:4000',
            ]);
        }
        if($request->image4){
            $request->validate([
                "image4"=>'image|mimes:jpg,png,jpeg|max:4000',
            ]);
        }

        if($this->securityChecks($request->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $choices=['','','',''];
        $ch=explode(',',$request->choices);

        foreach($ch as $index=>$choice){
            $choices[$index]=$choice;
        }

        $ch1_description;
        $ch1_description=explode(',',$request->ch0_description);
        $ch1_description=serialize($ch1_description);

        $ch2_description;
        $ch2_description=explode(',',$request->ch1_description);
        $ch2_description=serialize($ch2_description);
        
        $ch3_description;
        $ch3_description=explode(',',$request->ch2_description);
        $ch3_description=serialize($ch3_description);

        
        $ch4_description;
        $ch4_description=explode(',',$request->ch3_description);
        $ch4_description=serialize($ch4_description);
        
        $extras;
        $extras=explode(',',$request->extras);
        $extras=serialize($extras);
        
        
        $come_withs;
        $come_withs=explode(',',$request->come_with);
        $come_withs=serialize($come_withs);

       
        $image1="";$image2="";$image3="";$image4="";
        
        if($request->image1){
            $image1=$this->storeImage($request->image1);
        }
        if($request->image2){
            $image2=$this->storeImage($request->image2);
        }
        if($request->image3){
            $image3=$this->storeImage($request->image3);
        }
        if($request->image4){
            $image4=$this->storeImage($request->image4);
        }
        
        $dish=Dish::create([
            'name'=>$request->name,
            'come_with'=>$come_withs,
            'description'=>$request->description,
            'price'=>$request->price,
            'status'=>'active',
            'image1'=>$image1,
            'image2'=>$image2,
            'image3'=>$image3,
            'image4'=>$image4,
            'choice1'=>$choices[0],
            'choice2'=>$choices[1],
            'choice3'=>$choices[2],
            'choice4'=>$choices[3],
            'extras'=>$extras,
            'ch1_description'=>$ch1_description,
            'ch2_description'=>$ch2_description,
            'ch3_description'=>$ch3_description,
            'ch4_description'=>$ch4_description,
            'send_to'=>$request->send_to,
            'restaurant_id'=>$request->restaurant_id,
            'dish_category_id'=>$request->dish_category_id
            ]);
        return response()->json([
            "data"=>$dish
        ]);
    }
    protected function storeImage($image){
        $image_name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/dish/';
        // $image->move($destinationPath, $image_name);
        $img = Image::make($image->getRealPath());
        $img->resize(500, 500, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$image_name);
        
        $path='/images/dish/'.$image_name;

        return $path;
    }


    public function update(Request $request){

        
        $request->validate([
            'name'=>"required",
            'description'=>"required",
            'price'=>"required",
            'send_to'=>"required",
        ]);


        $dish=Dish::findOrFail($request->id);
        $choices=['','','',''];
        $ch=explode(',',$request->choices);
        
        foreach($ch as $index=>$choice){
            $choices[$index]=$choice;
        }

        $ch1_description;
        $ch1_description=explode(',',$request->ch0_description);
        $ch1_description=serialize($ch1_description);

        $ch2_description;
        $ch2_description=explode(',',$request->ch1_description);
        $ch2_description=serialize($ch2_description);

        $ch3_description;
        $ch3_description=explode(',',$request->ch2_description);
        $ch3_description=serialize($ch3_description);


        $ch4_description;
        $ch4_description=explode(',',$request->ch3_description);
        $ch4_description=serialize($ch4_description);

        $extras=[];
        $extrasPrice=explode(',',$request->extrasPrice);
        $extrasName=explode(',',$request->extrasName);

        foreach($extrasName as $index=>$e){
            $value=[
                "name"=>$e,
                "price"=>$extrasPrice[$index]
            ];
            array_push($extras,$value);
        }

        $extras=serialize($extras); 

        $come_withs;
        $come_withs=explode(',',$request->come_with);
        $come_withs=serialize($come_withs);


        $image1="";$image2="";$image3="";$image4="";
        if($request->image1){
            $request->validate([
                "image1"=>'reqired|image|mimes:jpg,png,jpeg|max:2000',
            ]);
            $path=public_path($dish->image1);                    
            @unlink($path);
            $image1=$this->storeImage($request->image1);
            $dish->update([
                'image1'=>$image1,
            ]);
        }
        if($request->image2){
            $path=public_path($dish->image2);                    
            if($request->image2!='delete'){
                $request->validate([
                    "image2"=>'image|mimes:jpg,png,jpeg|max:2000',
                ]);
                
                $image2=$this->storeImage($request->image2);
                $dish->update([
                    'image2'=>$image2,
                ]);
            }else{
                $dish->update([
                    'image2'=>null,
                ]);
            }
            @unlink($path);
        }
        
        if($request->image3){
            $path=public_path($dish->image3);                    
            if($request->image3!='delete'){

                $request->validate([
                    "image3"=>'image|mimes:jpg,png,jpeg|max:2000',
                ]);
                $image3=$this->storeImage($request->image3);
                $dish->update([
                       'image3'=>$image3,
                ]);
            }else{
                $dish->update([
                    'image3'=>null,
                ]);
            }
            @unlink($path);
        }
        if($request->image4){
            $path=public_path($dish->image4);                    
            if($request->image4!='delete'){
                $request->validate([
                    "image4"=>'image|mimes:jpg,png,jpeg|max:2000',
                ]);
                $image4=$this->storeImage($request->image4);
                $dish->update([
                    'image4'=>$image4,
                ]);
            }else{
                $dish->update([
                    'image4'=>null,
                ]);
            }
            
            @unlink($path);
            
        }

        $dish->update([
            'name'=>$request->name,
            'come_with'=>$come_withs,
            'description'=>$request->description,
            'price'=>$request->price,
            'choice1'=>$choices[0],
            'choice2'=>$choices[1],
            'choice3'=>$choices[2],
            'choice4'=>$choices[3],
            'extras'=>$extras,
            'ch1_description'=>$ch1_description,
            'ch2_description'=>$ch2_description,
            'ch3_description'=>$ch3_description,
            'ch4_description'=>$ch4_description,
            'send_to'=>$request->send_to,
        ]);
        
        return response()->json([
            "data"=>$dish
        ]); 
    }
    public function delete($id){
        $dish=Dish::findOrFail($id);
        if($this->securityChecks($dish->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}
        if($dish->image1){
            @unlink($dish->image1);
        }
        if($dish->image2){
            @unlink($dish->image2);
        }
        if($dish->image3){
            @unlink($dish->image3);
        }
        if($dish->image4){
            @unlink($dish->image4);
        }
        $dish->delete();
        return response()->json([
            "data"=>$id
        ]);
    }
    public function show($id){
        $dish=new DishResource(Dish::findOrFail($id));

        return response()->json([
            "data"=>$dish
        ]);
    }
    public function searchDish($restaurant_id,$value){
        $dNames=Dish::select('name')->where('restaurant_id',$restaurant_id)
        ->where('name','LIKE',"%{$value}%")
        ->get();

        return response()->json([
            "data"=>$dNames
        ]);
    }
    public function getSearchDish($restaurant_id,$name){
        $dish=new DishResource(Dish::where('restaurant_id',$restaurant_id)->where('name',$name)->first());

        return response()->json([
            "data"=>$dish
        ]);
    }

    
    public function updateStatus(Request $request){
        $dish=Dish::findOrFail($request->id);

        if($this->securityChecks($dish->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $dish->update([
            "status"=>$request->status
        ]);

        return response()->json([
            "data"=>$dish
        ]);
        
    }

    private function securityChecks($restaurant_id){
        
        if(auth()->guard('api')->user()->role=='owner'){
            if(auth()->guard('api')->user()->restaurant->id!=$restaurant_id){
                return true;
            }else{
                return false;
            }
        }else{
            if(auth()->guard('api')->user()->restaurant_id != $restaurant_id){
                return true;
                return response()->json(["errors"=>"Not authorized"],403);
            }else{
                return false;
            }
        }
        return true;
    }
}