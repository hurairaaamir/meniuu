<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProductEnteriesResource;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
    public function index($restaurant_id){
        $this->securityChecks($restaurant_id);
        $productCategories=ProductCategory::where('restaurant_id',$restaurant_id)->get();

        return response()->json([
            "data"=>$productCategories
        ]);
    }

    public function show($id){
        $productCategory=ProductCategory::findOrFail($id);

        return response()->json([
            "data"=>$productCategory
        ]);
    }
    public function store(Request $request){
        $request->validate([
            'category_name'=>"required",
            'restaurant_id'=>"required"
        ]);
        
        $productCategory=ProductCategory::create([
            'category_name'=>$request->category_name,
            'restaurant_id'=>$request->restaurant_id,
        ]);

        return response()->json([
            "data"=>$productCategory
        ]);

    }
    public function update(Request $request,$id){
        $productCategory=ProductCategory::findOrFail($id);
        $request->validate([
            'category_name'=>"required",
            'restaurant_id'=>"required"
        ]);
        $productCategory->update([
            'category_name'=>$request->category_name,
            'restaurant_id'=>$request->restaurant_id,
        ]);

        return response()->json([
            "data"=>"updated Successfully"
        ]);

    }
    public function delete($id){
        $productCategory=ProductCategory::findOrFail($id);

        $productCategory->delete();

        return response()->json([
            "data"=>$id
        ]);
    }

    private function securityChecks($restaurant_id){
        if(auth()->user()->role=='owner'){
            if(auth()->user()->restaurant->id!=$restaurant_id){
                return response()->json(["error"=>"Not authorized"],403);
            }else{
                return true;
            }
        }else{
            if(auth()->user()->restaurant_id != $restaurant_id){
                return response()->json(["error"=>"Not authorized"],403);
            }else{
                return true;
            }
        }
        return response()->json(["error"=>"Not authorized"],403);
    }
}
