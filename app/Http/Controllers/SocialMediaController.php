<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialMedia;

class SocialMediaController extends Controller
{
    public function index($restaurant_id){
        $social_medias=SocialMedia::where('restaurant_id',$restaurant_id)->get();

        return response()->json([
            "data"=>$social_medias
        ]);
    }

    public function show($id){
        $social_media=SocialMedia::findOrFail($id);

        return response()->json([
            "data"=>$social_media
        ]);
    }
    public function store(Request $request){
        $request->validate([
            'link'=>"required",
            'name'=>"required",
            'restaurant_id'=>"required"
        ]);
        $social_media=SocialMedia::create($request->all());

        return response()->json([
            "data"=>$social_media
        ]);

    }
    public function update(Request $request,$id){
        
        $request->validate([
            'link'=>"required",
        ]);
        $social_media=SocialMedia::findOrFail($id);

        $social_media->update([
            'link'=>$request->link,
        ]);

        return response()->json([
            "data"=>"updated Successfully"
        ]);

    }
    public function delete($id){
        $social_media=SocialMedia::findOrFail($id);

        $social_media->delete();

        return response()->json([
            "data"=>$social_media
        ]);
    }
}
