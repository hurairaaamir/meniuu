<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;

class TableController extends Controller
{
    public function index($restaurant_id){
        $tables=Table::where('restaurant_id',$restaurant_id)->get();

        return response()->json([
            "data"=>$tables
        ]);
    }

    public function show($id){
        $table=Table::findOrFail($id);

        return response()->json([
            "data"=>$table
        ]);
    }
    public function store(Request $request){
        $request->validate([
            'name'=>"required",
            'link'=>"required",
            'restaurant_id'=>"required"
        ]);
        $table=Table::create($request->all());

        $table->update([
            "link"=>$request->link.$table->id
        ]);
        

        return response()->json([
            "data"=>$table
        ]);

    }
    public function update(Request $request,$id){
        $table=Table::findOrFail($id);
        
        $request->validate([
            'name'=>"required",
            'link'=>"required",
        ]);

        $table->update([
            'name'=>$request->name,
            "link"=>$request->link.$id
        ]);

        return response()->json([
            "data"=>"updated Successfully"
        ]);

    }
    public function delete($id){
        $table=Table::findOrFail($id);

        $table->delete();

        return response()->json([
            "data"=>$id
        ]);
    }
}
