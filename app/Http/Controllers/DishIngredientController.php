<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DishIngredient;
use App\Dish;

class DishIngredientController extends Controller
{
    public function index($restaurant_id,$dish_id){

        if($this->securityChecks($restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $dish=DishIngredient::where('restaurant_id',$restaurant_id)->where('dish_id',$dish_id)->get();

        return response()->json([
            "data"=>$dish
        ]);
    }

    public function show($id){

        $dish=DishIngredient::findOrFail($id);

        if($this->securityChecks($dish->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        return response()->json([
            "data"=>$dish
        ]);
    }
    public function store(Request $request){
        $request->validate([
            'product'=>"required",
            'category'=>"required",
            'measuring_unit'=>"required",
            'quantity'=>"required",
            'id'=>"required"
        ]);

        if($this->securityChecks($request->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $product=DishIngredient::create([
            'product'=>$request->product,
            'category'=>$request->category,
            'measuring_unit'=>$request->measuring_unit,
            'quantity'=>$request->quantity,
            'dish_id'=>$request->id,
            'restaurant_id'=>$request->restaurant_id
        ]);

        return response()->json([
            "data"=>$product
        ]);

    }
    public function update(Request $request,$id){
        $request->validate([
            'product'=>"required",
            'category'=>"required",
            'measuring_unit'=>"required",
            'quantity'=>"required",
            'id'=>"required"
        ]);


        $dish_ingredient=DishIngredient::findOrFail($id);

        if($this->securityChecks($dish_ingredient->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}    

        $dish_ingredient->update([
            'product'=>$request->product,
            'category'=>$request->category,
            'measuring_unit'=>$request->measuring_unit,
            'quantity'=>$request->quantity,
        ]);

        return response()->json([
            "data"=>$dish_ingredient
        ]);

    }
    public function delete($id){
        $dish_ingredient=DishIngredient::findOrFail($id);
        
        if($this->securityChecks($dish_ingredient->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}    

        $dish_ingredient->delete();

        return response()->json([
            "data"=>$id
        ]);
    } 

    private function securityChecks($restaurant_id){
        
        if(auth()->guard('api')->user()->role=='owner'){
            if(auth()->guard('api')->user()->restaurant->id!=$restaurant_id){
                return true;
            }else{
                return false;
            }
        }else{
            if(auth()->guard('api')->user()->restaurant_id != $restaurant_id){
                return true;
                return response()->json(["errors"=>"Not authorized"],403);
            }else{
                return false;
            }
        }
        return true;
    }
}
