<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderItemResource;
use App\Events\StockEnteryEvent;
use Illuminate\Support\Facades\Hash;
use App\Restaurant;
use App\SocialMedia;
use App\Table;
use App\Order;
use App\OrderSecret;
use App\OrderItem;
use Stripe\Stripe;
// use Cartalyst\Stripe\Stripe;

class OrderController extends Controller
{
    /** 
     * fetch order by type
     * 
     * @return orders by type
    */
    public function index($restaurant_id,$type)
    {
        if($this->securityChecks($restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        if($type=='all'){
            if(auth()->user()->role=='delivery_man'){
                $orders=OrderResource::collection(Order::where('restaurant_id',$restaurant_id)->where('active','yes')
                                            ->where('source','link')
                                            ->where('status','<>','Delivered')
                                            ->where('serviceMode','Delivery')
                                            ->get());                
            }else{
                $orders=OrderResource::collection(Order::where('restaurant_id',$restaurant_id)->where('active','yes')
                                                ->where('status','<>','Delivered')
                                                ->where('status','<>','served')
                                                ->get());
            }
        }else if($type=='pending'){
            $orders=OrderResource::collection(Order::where('restaurant_id',$restaurant_id)->where('active','yes')
                                                    // ->where('status','<>','served')
                                                    ->where('orderType','pending')->get());
        }else if($type=='payed'){
            $orders=OrderResource::collection(Order::where('restaurant_id',$restaurant_id)->where('active','yes')
                                                    ->where('status','<>','Delivered')
                                                    ->where('status','<>','served')
                                                    ->where('orderType','payed')->get());
        }else if($type=='served'){
            $orders=OrderResource::collection(Order::where('restaurant_id',$restaurant_id)->where('active','yes')
                                                    ->where('status','served')
                                                    ->orWhere('status','Delivered')
                                                    ->get());
        }
        return response()->json([
            "data"=>$orders
        ]);
    }



    /** 
     * filtering order
     * 
     * @return orders filter
    */
    public function getOrdersBySource(Request $request)
    {
        $restaurant_id=$request->restaurant_id;
        $sources=$request->source;

        if($this->securityChecks($restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $orders=OrderResource::collection(Order::where('restaurant_id',$restaurant_id)->where('active','yes')
                        ->whereIn('source',$sources)
                        ->where('status','<>','served')
                        ->where('status','<>','Delivered')
                        ->get());

        return response()->json([
            "data"=>$orders
        ]);
        
    }


    /** 
     * fetching order 
     * 
     * @return orders 
    */
    
    public function getOrder($restaurant_id,$id)
    {
        $order= new OrderResource(Order::findOrFail($id));

        if($this->securityChecks($order->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        return response()->json([
            "data"=>$order
        ]);
    }


    /** 
     * 
     * 
     * @return Random number
    */
    private function random($size)
    {
        $permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, $size);
    }


    /** 
     * creating 
     * 
     * @return success order 
    */

    public function store(Request $request)
    {
        $rest=Restaurant::findOrFail($request->restaurant_id);
        if($rest->status=='close'){
            return response()->json([
                'errors' => ['restaurantClose'=>["Sorry Restaurant is close"]],
            ], 422);
        }

        $request->validate([
            "subtotal"=>"required",
            "restaurant_id"=>"required",
        ]);

        
        if($request->source=='link'){
            $request->validate([
                "serviceMode"=>"required"
            ]);
            if($request->serviceMode=='Delivery'){
                $request->validate([
                    // "name"=>"required", 
                    "customer_name"=>"required",      
                    // "email"=>"required|email",
                    "address"=>"required",
                    "phone"=>"required"
                ]);
            }
            if($request->serviceMode=='PickUp'){
                $request->validate([
                    "customer_name"=>"required",
                    // "email"=>"required|email",
                    "phone"=>"required"
                ]);
            }
        }
        
        if($request->paymentMethod=='card'&&(!$request->receipt)){
            $request->validate([
                "email"=>"required|email",
            ]);            

        }else if($request->paymentMethod=='cash'){
            if(auth()->guard('api')->user()->role=='customer'){
                return response()->json(["error"=>"invalid request"],422);
            }
            $request->validate([
                "pay"=>"required",
            ]);
        }else if($request->paymentMethod=='card'){
            $secret=OrderSecret::where('secret_id',$request->id)->first();
            if($secret){
                $secret->delete();
            }else{ 
                return response()->json(["error"=>"invalid request"],422);
            }
        }

        $orderStatus=$this->status($request->items);
        
        

        $items=$request->items;
        ($request->orderType=='table')?$orderType="payed":$orderType=$request->orderType;

        

        $order=Order::create([
            "email"=>$request->email,
            "phone"=>$request->phone,
            "optional_note"=>$request->optional_note,
            "pay"=>$request->pay,
            "change"=>$request->change,
            "table"=>$request->table,
            "subtotal"=>$request->subtotal,
            "total"=>$request->total,
            "timestamp"=>$request->timestamp,
            "restaurant_id"=>$request->restaurant_id,
            "status"=>$orderStatus,
            "orderType"=>$orderType,
            "paymentMethod"=>$request->paymentMethod,
            "cardNo"=>$request->cardNo,
            "mmyycc"=>$request->mmyycc,
            "tip"=>$request->tip,
            "tipAmount"=>$request->tipAmount,
            "serviceMode"=>$request->serviceMode,
            "address"=>$request->address,
            "source"=>$request->source,
            "active"=>"yes",
            "stripe_id"=>$request->id,
            "customer_name"=>$request->customer_name,
            "order_code"=>""
        ]);
        $order_code=$this->random(5).'#'.$order->id;

            
        $order->update([
            "order_code"=>$order_code
        ]);
        foreach($items as $item){
            
            $ch_description=serialize($item["ch_description"]);

            $extras=[];
            ($item["send_to"]=='waiters')?$status="cooked":$status="cooking";
            foreach($item["extras"] as $extra){
                $arr=[
                    "name"=>$extra["name"],
                    "price"=>$extra["price"]
                ];
                array_push($extras,$arr);
            }
            $extras=serialize($extras);
 
            if($request->orderType=='table'){
                    $orderItem=OrderItem::create([
                        "dish_id"=>$item["id"],
                        "ch_description"=>$ch_description,
                        "extras"=>$extras,
                        "price"=>$item["price"],
                        "name"=>$item["name"],
                        "quantity"=>$item["quantity"],
                        "note"=>$item["note"],
                        "total"=>$item["total"],
                        "status"=>$status,
                        "category_name"=>$item["category_name"],
                        "restaurant_id"=>$request->restaurant_id,
                        "order_id"=>$order->id
                    ]);
            }else{
                $orderItem=OrderItem::create([
                    "dish_id"=>$item["id"],
                    "ch_description"=>$ch_description,
                    "extras"=>$extras,
                    "price"=>$item["price"],
                    "name"=>$item["name"],
                    "quantity"=>$item["quantity"],
                    "note"=>$item["note"],
                    "total"=>$item["total"],
                    "status"=>$status,
                    "category_name"=>$item["category_name"],
                    "restaurant_id"=>$request->restaurant_id,
                    "order_id"=>$order->id
                ]);
            }
            if($item["send_to"]=='waiters'){
                event(new StockEnteryEvent($orderItem->name,$orderItem->restaurant_id,$orderItem->order_id));
            }
        }

        $order= new OrderResource($order);

        event(new \App\Events\OrderEvent($order,$order->restaurant_id,null,null));

        if($order->serviceMode=="PickUp"&&$order->status=="cooked"){
            $this->sendPickUpEmail($order);
        }

        return response()->json([
            "data"=>$order->order_code
        ]);
    }



    /** 
     * checking order items status for the update of compete order status
     * 
     * @return event
    */
    private function status($items)
    {
        $length=count($items);
        $i=0;
        foreach($items as $item){
            // ($item["send_to"]=='waiters')? : print_r("nothing");
            
            if($item["send_to"]=='waiters'){
                $i++;
                
            }
        }
        ($length==$i)?$status="cooked":$status="cooking";
           
        return $status;
    }



    /** 
     * making order inactive
     * 
     * @return inactivate order
    */
    
    public function delete($id)
    {
        $order=Order::findOrFail($id);
        if($this->securityChecks($order->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $order->update([
            "active"=>"no"
        ]);
        
        event(new \App\Events\OrderEvent(null,$order->restaurant_id,null,$id));

        return response()->json([
            "data"=>$id
        ]);
    }

    

    /** 
     * Password confimation
     * 
     * @return successOrError
    */
    public function confirmPassword(Request $request)
    {
        $request->validate([
            "password"=>"required"
        ]);
        if(Hash::check($request->password, auth()->user()->password)){
          return response()->json([
              "data"=>"success"
          ]);  
        }else{
            return response()->json([
                "data"=>"error"
            ]);
        }
    }



    /** 
     * Password confimation
     * 
     * @return successOrError
    */
    public function item_status(Request $request)
    {
        $item=OrderItem::findOrFail($request->id);

        $item->update([
            "status"=>$request->status
        ]);

        if($request->status=='cooked'){
            event(new StockEnteryEvent($item->name,$item->restaurant_id,$item->order_id));
        }   

        $sendItem=new OrderItemResource($item);
        event(new \App\Events\OrderEvent(null,$request->restaurant_id,$sendItem,null));

        
        
        return response()->json([
            "data"=>$request->status
        ]);
    }



    /** 
     * Password confimation
     * 
     * @return OrderStatus
    */
    public function order_status(Request $request)
    {
        $order=Order::findOrFail($request->id);

        $order->update([
            "status"=>$request->status
        ]);

        $order=new OrderResource($order);

        event(new \App\Events\OrderEvent($order,$order->restaurant_id,null,null));

        if($order->serviceMode=="PickUp"&&$order->status=="cooked"){
            $this->sendPickUpEmail($order);
        }

        return response()->json([
            "data"=>$order
        ]);

    }

    /** 
     * sending email Food cooked
     * 
     * @return SendingEmail
    */

    private function sendPickUpEmail($order)
    {
        $restaurant=Restaurant::find($order->restaurant_id);

        $data=[
            "logo"=>$restaurant->logo,
            "customer"=>$order->customer_name,
            "address"=>$restaurant->address,
            "restaurant"=>$restaurant->name
        ];

        event(new \App\Events\FoodReadyEvent($order->email,$data));
    }


    /** 
     * Making order payed
     * 
     * @return OrderTypeUpdateEvent
    */

    public function updateOrderType(Request $request)
    {
        $request->validate([
            "paymentMethod"=>"required"
        ]);
        $order=Order::findOrFail($request->order_id);
   
        if($request->paymentMethod=="card"){
            $order->update([
                "paymentMethod"=>$request->paymentMethod,
                "orderType"=>"payed"
            ]);
        }else if($request->paymentMethod=="cash"){
            $request->validate([
                "pay"=>"required"
            ]);
            $order->update([
                "paymentMethod"=>$request->paymentMethod,
                "pay"=>$request->pay,
                "change"=>$request->change,
                "orderType"=>"payed"
            ]);
        }
        $order=new OrderResource($order);
                    
        event(new \App\Events\OrderEvent($order,$order->restaurant_id,null,null));
        

        return response()->json([
            "data"=>$order->id
        ]);
    }

    /** 
     * Counting order number
     * 
     * @return SendingOrderNumbers
    */
    public function getOrderNo($restaurant_id)
    {
        $role=auth()->user()->role;
        if($role=='delivery_man'||$role=='cook'){
            $liveNo=Order::where('restaurant_id',$restaurant_id)                                            
            ->where('active','yes')
            ->where('source','link')
            ->where('serviceMode','Delivery')
            ->count();

            $pendingNo=null;
        }else{
            $liveNo=Order::where('restaurant_id',$restaurant_id)->where('status','<>','served')->where('status','<>','Delivered')->where('active','yes')->count();
            $pendingNo=Order::where('restaurant_id',$restaurant_id)->where('orderType','pending')->where('active','yes')->count();
        }

        return response()->json([
            "data"=>[
                "live_no"=>$liveNo,
                "pending_no"=>$pendingNo
            ]
        ]);
        
    }


    /** 
     * Password confimation
     * 
     * @return successOrError
    */
    public function checkout(Request $request)
    {
        try{
            $stripe= Stripe::make('sk_test_51HOjzsC1zYIo0nPVtGcQectayrFTWlHpuLrEq2a9k556QtlIYMOJksjiPXZDr5lDFFbezTywBwWobnXakLRvcw5a00wkFxG1U5');
            $charge = $stripe->charges()->create([
                'amount'               => 30.00,
                'currency'             => 'USD',
                'source'=>$request->stripeToken,
                "description"=>"this is test description",
                // 'receipt_email'=>$request->email,
                'receipt_email'=>"0909cosmos@gmail.com",
                'metadata'=>[
                    'data1'=>'metadata 1',
                    'data2'=>'metadata 2',
                    'data3'=>'metadata 3',
                    'data4'=>'metadata 4',
                ]
                
            ]);
        }catch(Exception $e){

        }
 

        return $charge;
    }

    /** 
     * get By tabs
     * 
     * @return successOrError
    */

    public function getTabs($restaurant_id){
        $linkTabs=SocialMedia::where('restaurant_id',$restaurant_id)->where('active','yes')->get();
        $tableTabs=Table::where('restaurant_id',$restaurant_id)->where('active','yes')->get();

        return response()->json([
            "data"=>[
                "link"=>$linkTabs,
                "table"=>$tableTabs
            ]
        ]);
    }

    /** 
     * Security Checks middleware
     * 
     * @return successOrError
    */

    private function securityChecks($restaurant_id){
        
        if(auth()->guard('api')->user()->role=='owner'){
            if(auth()->guard('api')->user()->restaurant->id!=$restaurant_id){
                return true;
            }else{
                return false;
            }
        }else{
            if(auth()->guard('api')->user()->restaurant_id != $restaurant_id){
                return true;
                return response()->json(["errors"=>"Not authorized"],403);
            }else{
                return false;
            }
        }
        return true;
    }
    
}

