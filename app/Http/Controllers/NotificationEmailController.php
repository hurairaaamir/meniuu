<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotificationEmails;

class NotificationEmailController extends Controller
{
    public function show($restaurant_id){
        $emails=NotificationEmails::where('restaurant_id',$restaurant_id)->first();

        return response()->json([
            "data"=>$emails
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'min_email'=>"required|email",
            'max_email'=>"required|email",
            'opt_email'=>"required|email",
            'restaurant_id'=>"required"
        ]);
        $notificationEmail=NotificationEmails::where('restaurant_id',$request->restaurant_id)->first();

        if(!$notificationEmail){
            $notificationEmail=NotificationEmails::create([
                'min_email'=>$request->min_email,
                'max_email'=>$request->max_email,
                'opt_email'=>$request->opt_email,
                'restaurant_id'=>$request->restaurant_id
            ]);

        }else{
            $notificationEmail->update([
                'min_email'=>$request->min_email,
                'max_email'=>$request->max_email,
                'opt_email'=>$request->opt_email,
            ]);
        }

        return response()->json([ "data"=>$notificationEmail]);
    }
}
