<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DishCategory;
use Intervention\Image\Facades\Image;

class DishCategoryController extends Controller
{
    public function index($restaurant_id){


        $dishes=DishCategory::where('restaurant_id',$restaurant_id)->get();
        return response()->json([
            "data"=>$dishes
        ]);
    }
    public function indexActive($restaurant_id){
        

        $dishes=DishCategory::where('restaurant_id',$restaurant_id)->where("status","active")->get();
        return response()->json([
            "data"=>$dishes
        ]);
    }
    public function store(Request $request){
        if($this->securityChecks($request->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $request->validate([
            "dish_name"=>"required",
            "image"=>'required|image|mimes:jpg,png,jpeg|max:2000',
            "restaurant_id"=>"required"
        ]);
        $image=$this->storeImage($request->image);

        $dish=DishCategory::create([
            "dish_name"=>$request->dish_name,
            "image"=>$image,
            'status'=>'active',
            "restaurant_id"=>$request->restaurant_id
        ]);

        return response()->json([
            "data"=>$dish
        ]);
    }

    protected function storeImage($image){
        $image_name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/dishCategory/';
        // $image->move($destinationPath, $image_name);
        $img = Image::make($image->getRealPath());
        $img->resize(500, 500, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$image_name);
        
        $path='/images/dishCategory/'.$image_name;

        return $path;
    }

    public function update(Request $request){

        $request->validate([
            "dish_name"=>"required",
        ]);

        $dishCategory=DishCategory::findOrFail($request->id);

        if($this->securityChecks($dishCategory->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        if($request->image){
            $request->validate([
                "image"=>'required|image|mimes:jpg,png,jpeg|max:2000',
            ]);
            $path=public_path($dishCategory->image);                    
            @unlink($path);

            $image=$this->storeImage($request->image);

            $dishCategory->update([
                "dish_name"=>$request->dish_name,
                "image"=>$image
            ]);

        }else{

            $dishCategory->update([
                "dish_name"=>$request->dish_name,
            ]);
        }

        return response()->json([
            "data"=>$dishCategory
        ]); 
    }
    public function delete($id){
        $dish=DishCategory::findOrFail($id);
        if($this->securityChecks($dish->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}
        if($dish->image){
            @unlink($dish->image);
        }

        $dish->delete();

        return response()->json([
            "data"=>$id
        ]);
    }
    public function show($id){
        $dish=DishCategory::findOrFail($id);
        
        return response()->json([
            "data"=>$dish
        ]);
    }
    public function updateStatus(Request $request){
        // dd($request->all());
        $dishCategory=DishCategory::findOrFail($request->id);

        if($this->securityChecks($dishCategory->restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $dishCategory->update([
            "status"=>$request->status
        ]);

        return response()->json([
            "data"=>$dishCategory
        ]);
        
    }

    private function securityChecks($restaurant_id){
        
        if(auth()->guard('api')->user()->role=='owner'){
            if(auth()->guard('api')->user()->restaurant->id!=$restaurant_id){
                return true;
            }else{
                return false;
            }
        }else{
            if(auth()->guard('api')->user()->restaurant_id != $restaurant_id){
                return true;
                return response()->json(["errors"=>"Not authorized"],403);
            }else{
                return false;
            }
        }
        return true;
    }
}
