<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Extra;

class ExtraController extends Controller
{
    public function index($restaurant_id){
        $extras=Extra::where('restaurant_id',$restaurant_id)->get();

        return response()->json([
            "data"=>$extras
        ]);
    }

    public function store(Request $request){
        $request->validate([
            "name"=>"required",
            "price"=>"required",
            "restaurant_id"=>"required"
        ]);

        $extra=Extra::create([
            "name"=>$request->name,
            "price"=>$request->price,
            "restaurant_id"=>$request->restaurant_id
        ]);

        return response()->json([
            "data"=>$extra
        ]);
    }
    public function update(Request $request,$id){
        $request->validate([
            "name"=>"required",
            "price"=>"required"
        ]);
        $extra=Extra::findOrFail($id);

        $extra->update([
            "name"=>$request->name,
            "price"=>$request->price
        ]);

        return response()->json([
            "data"=>$extra
        ]); 
    }
    public function delete($id){
        $extra=Extra::findOrFail($id);
        
        $extra->delete();

        return response()->json([
            "data"=>$id
        ]);
    }
    public function show($id){
        $extra=Extra::findOrFail($id);
        
        return response()->json([
            "data"=>$extra
        ]);
    }
}
