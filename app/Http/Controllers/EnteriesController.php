<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProductEnteriesResource;
use App\Http\Resources\ProductEnteriesStupid;
use App\Http\Resources\HistoryResource;
use App\ProductCategory;
use App\StockEnteries;
use App\Stock;
use App\Product;

class EnteriesController extends Controller
{
    public function productEnteries($restaurant_id){

        if($this->securityChecks($restaurant_id)){return response()->json(["errors"=>"Not authorized"],403);}

        $products= ProductEnteriesResource::collection(ProductCategory::where('restaurant_id',$restaurant_id)->get());

        return response()->json([
            "data"=>$products
        ]);
    }

    public function store(Request $request){

        $request->validate([
            "restaurant_id"=>"required",
            "message"=>"required",
            "enteries"=>"required"
        ]);

        $type=$request->quantity_type;
            
            
        $stock=Stock::create([
            'restaurant_id'=>$request->restaurant_id,
            'message'=>$request->message,
            "type"=>$type,
        ]);

        foreach($request->enteries as $entry){

            $product=Product::where('product_name',$entry["product"])->first();

            if($type=="add"){
                $quantity=$product->quantity + $entry["quantity"];
            }else if($type=="output"){
                $quantity=$product->quantity - $entry["quantity"];
            }

            StockEnteries::create([
                'product'=>$entry["product"],
                'category'=>$entry["category"],
                'measuring_unit'=>$entry["measuring_unit"],
                'quantity'=>$entry["quantity"],
                'stock_id'=>$stock->id
            ]);


            $product->update([
                "quantity"=>$quantity
            ]);
        }

        return response()->json([
            "data"=>"data stored successfully"
        ]);


    }
    public function history($restaurant_id,$type){

        $histories=HistoryResource::collection(Stock::where('restaurant_id',$restaurant_id)->where('type',$type)->get());
        // if($type=='add'){
        // }else if($type=='output'){
        //     $histories=ProductEnteriesStupid::collection(Stock::where('restaurant_id',$restaurant_id)->get());
        // }

        return response()->json([
            "data"=>$histories
        ]);
    }
    private function securityChecks($restaurant_id){
        
        if(auth()->guard('api')->user()->role=='owner'){
            if(auth()->guard('api')->user()->restaurant->id!=$restaurant_id){
                return true;
            }else{
                return false;
            }
        }else{
            if(auth()->guard('api')->user()->restaurant_id != $restaurant_id){
                return true;
                return response()->json(["errors"=>"Not authorized"],403);
            }else{
                return false;
            }
        }
        return true;
    }
}
