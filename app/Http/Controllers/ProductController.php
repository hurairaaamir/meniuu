<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\Extra;


class ProductController extends Controller
{
    public function index($restaurant_id,$product_category_id){
        $products=Product::where('restaurant_id',$restaurant_id)->where('product_category_id',$product_category_id)->get();

        return response()->json([
            "data"=>$products
        ]);
    }

    public function show($id){
        $product=Product::findOrFail($id);

        return response()->json([
            "data"=>$product
        ]);
    }
    public function store(Request $request){
        $request->validate([
            'product_name'=>"required",
            'measuring_unit'=>"required",
            'min_stock'=>"required",
            'max_stock'=>"required",
            'opt_stock'=>"required",
            'product_category_id'=>"required",
            'restaurant_id'=>"required"
        ]);
        
        $product=Product::create([
            'product_name'=>$request->product_name,
            'measuring_unit'=>$request->measuring_unit,
            'min_stock'=>$request->min_stock,
            'max_stock'=>$request->max_stock,
            'opt_stock'=>$request->opt_stock,
            'product_category_id'=>$request->product_category_id,
            'restaurant_id'=>$request->restaurant_id
        ]);

        return response()->json([
            "data"=>$product
        ]);

    }
    public function update(Request $request,$id){
        $request->validate([
            'product_name'=>"required",
            'measuring_unit'=>"required",
            'min_stock'=>"required",
            'max_stock'=>"required",
            'opt_stock'=>"required",
            'product_category_id'=>"required",
            'restaurant_id'=>"required"
        ]);
        $product=Product::findOrFail($id);
            
        $product->update([
            'product_name'=>$request->product_name,
            'measuring_unit'=>$request->measuring_unit,
            'min_stock'=>$request->min_stock,
            'max_stock'=>$request->max_stock,
            'opt_stock'=>$request->opt_stock,
            'product_category_id'=>$request->product_category_id,
            'restaurant_id'=>$request->restaurant_id
        ]);

        return response()->json([
            "data"=>$id
        ]);

    }
    public function delete($id){
        $product=Product::findOrFail($id);

        $product->delete();

        return response()->json([
            "data"=>$id
        ]);
    } 

    public function getCount($restaurant_id){
        $productCategoryNo=ProductCategory::where('restaurant_id',$restaurant_id)->count();
        
        
        return response()->json([
            "data"=>[
                "product_no"=>$productCategoryNo,
            ]
        ]);
    }
      
}
