<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Dish;
use App\User;
use App\DishCategory;
use App\Order;
use App\OrderItem;
use App\StockEnteries;
use App\Product;

class AnalyticsController extends Controller
{
    public function countDish($restaurant_id){

        $data=DB::table('order_items')
                ->select('name',DB::raw('count(name) as count'))
                ->where('restaurant_id',$restaurant_id)
                ->groupBy('name')
                ->get();

        return  response()->json([
            "data"=>$data
        ]);               
    }
    public function getMostSold(Request $request){
        $dish=Dish::where('restaurant_id',$request->restaurant_id)
            ->where('name',$request->dish["name"])->first();
        $category=DishCategory::where('restaurant_id',$request->restaurant_id)
            ->where('dish_name',$request->category["name"])->first();

        return response()->json([
            "data"=>[
                "dish"=>$dish,
                "category"=>$category
            ]
        ]);
    }
    public function countCategoryDish($restaurant_id){

        $data=DB::table('order_items')
                ->select(DB::raw('category_name as name'),DB::raw('count(quantity) as count'))
                ->where('restaurant_id',$restaurant_id)
                ->groupBy('category_name')
                ->get();

        return  response()->json([
            "data"=>$data
        ]);               
    }

    public function orderDetails(Request $request){
        
        $restaurant_id=$request->restaurant_id;
        
        if($request->type=='year'){
            $year=substr($request->date,0,4);
            $orders=Order::where('restaurant_id',$restaurant_id)
                    ->where('orderType','payed')
                    ->whereYear('created_at',$year)
                    ->get(); 

        }
        else if($request->type=='day'){
            $orders=Order::where('restaurant_id',$restaurant_id)
                    ->where('orderType','payed')
                    ->where('created_at',$request->date)
                    ->get(); 
        }
        else if($request->type=='month'){
            $month=substr($request->date,5,2);
            $year=substr($request->date,0,4);
            $orders=Order::where('restaurant_id',$restaurant_id)
                ->where('orderType','payed')
                ->whereYear('created_at',$year)
                ->whereMonth('created_at',$month)
                ->get(); 
        }
        else{
            $orders=Order::where('restaurant_id',$restaurant_id)
                ->where('orderType','payed')
                ->whereDay('created_at',now()->day)
                ->get();
        }

        return  response()->json([
            "data"=>$orders
        ]);               
    }
    public function countCategoryDishPost(Request $request){
        $restaurant_id=$request->restaurant_id;
        if($request->type=='year'){
            $year=substr($request->date,0,4);
            $data=DB::table('order_items')
                    ->select(DB::raw('category_name as name'),DB::raw('count(quantity) as count'))
                    ->where('restaurant_id',$restaurant_id)
                    ->whereYear('created_at',$year)
                    ->groupBy('category_name')
                    ->get();
        }else if($request->type=='day'){
            $data=DB::table('order_items')
                    ->select(DB::raw('category_name as name'),DB::raw('count(quantity) as count'))
                    ->where('restaurant_id',$restaurant_id)
                    ->where('created_at',$request->date)
                    ->groupBy('category_name')
                    ->get();
        }else if($request->type=='month'){
            $month=substr($request->date,5,2);
            $year=substr($request->date,0,4);
            $data=DB::table('order_items')
                    ->select(DB::raw('category_name as name'),DB::raw('count(quantity) as count'))
                    ->where('restaurant_id',$restaurant_id)
                    ->whereMonth('created_at',$month)
                    ->whereYear('created_at',$year)
                    ->groupBy('category_name')
                    ->get();
        }else{
            $data=DB::table('order_items')
                    ->select(DB::raw('category_name as name'),DB::raw('count(quantity) as count'))
                    ->where('restaurant_id',$restaurant_id)
                    ->whereDay('created_at',now()->day)
                    ->groupBy('category_name')
                    ->get();
        }

        return  response()->json([
            "data"=>$data
        ]);   
    }






    public function getMoney(Request $request){
        if($request->data["type"]=='year')
        {
            $year=substr($request->data["date"],0,4);
            if($request->type=='card'||$request->type=='cash'){
                $money=Order::select(DB::raw("sum(total) as money"))
                    ->where('restaurant_id',$request->data["restaurant_id"])
                    ->whereYear('created_at',$year)
                    ->where("paymentMethod",$request->type)->get();
            }else{
                $money=Order::select(DB::raw("sum(total) as money"))
                    ->whereYear('created_at',$year)
                    ->where('restaurant_id',$request->data["restaurant_id"])->get();
            }
        }
        else if($request->data["type"]=='day')
        {
            if($request->type=='card'||$request->type=='cash'){
                $money=Order::select(DB::raw("sum(total) as money"))
                    ->where('restaurant_id',$request->data["restaurant_id"])
                    ->where('created_at',$request->data["date"])
                    ->where("paymentMethod",$request->type)->get();
            }else{
                $money=Order::select(DB::raw("sum(total) as money"))
                ->where('created_at',$request->data["date"])
                ->where('restaurant_id',$request->data["restaurant_id"])->get();
            }
        }
        else if($request->data["type"]=='month')
        {
            $month=substr($request->data["date"],5,2);
            $year=substr($request->data["date"],0,4);
            if($request->type=='card'||$request->type=='cash'){
                $money=Order::select(DB::raw("sum(total) as money"))
                    ->where('restaurant_id',$request->data["restaurant_id"])
                    ->whereYear('created_at',$year)
                    ->whereMonth('created_at',$month)
                    ->where("paymentMethod",$request->type)->get();
            }else{
                $money=Order::select(DB::raw("sum(total) as money"))
                ->where('restaurant_id',$request->data["restaurant_id"])
                ->whereYear('created_at',$year)
                ->whereMonth('created_at',$month)->get();
            }

        }
        else
        {
            if($request->type=='card'||$request->type=='cash'){
                $money=Order::select(DB::raw("sum(total) as money"))
                    ->where('restaurant_id',$request->data["restaurant_id"])
                    ->whereDay('created_at',now()->day)
                    ->where("paymentMethod",$request->type)->get();
            }else{
                $money=Order::select(DB::raw("sum(total) as money"))
                    ->where('restaurant_id',$request->data["restaurant_id"])
                    ->whereDay('created_at',now()->day)->get();
            }
            
        }

        return response()->json([
            "data"=>$money
        ]);
    }













    public function getPlates(Request $request)
    {
        if($request->data["type"]=='year')
        {
            $year=substr($request->data["date"],0,4);
            if($request->type=='card'||$request->type=='cash'){
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                    ->where('order_items.restaurant_id',$request->data["restaurant_id"])
                    ->join('orders','order_items.order_id','=','orders.id')
                    ->whereYear('order_items.created_at',$year)
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('name')->get();


            }else{
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                    ->where('restaurant_id',$request->data["restaurant_id"])
                    ->whereYear('created_at',$year)
                    ->groupBy('name')->get();
            }
        }
        else if($request->data["type"]=='day')
        {
            if($request->type=='card'||$request->type=='cash'){
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                    ->where('order_items.restaurant_id',$request->data["restaurant_id"])
                    ->join('orders','order_items.order_id','=','orders.id')
                    ->where('orders.created_at',$request->data["date"])
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('name')->get();
            }else{
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                ->where('created_at',$request->data["date"])
                ->where('restaurant_id',$request->data["restaurant_id"])
                ->groupBy('name')->get();
            }
        }
        else if($request->data["type"]=='month')
        {
            $month=substr($request->data["date"],5,2);
            $year=substr($request->data["date"],0,4);
            if($request->type=='card'||$request->type=='cash'){
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                    ->join('orders','order_items.order_id','=','orders.id')
                    ->where('order_items.restaurant_id',$request->data["restaurant_id"])
                    ->whereYear('orders.created_at',$year)
                    ->whereMonth('orders.created_at',$month)
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('name')->get();
            }else{
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                ->where('restaurant_id',$request->data["restaurant_id"])
                ->whereYear('created_at',$year)
                ->whereMonth('created_at',$month)
                ->groupBy('name')->get();
            }

        }
        else
        {
            if($request->type=='card'||$request->type=='cash'){
                    $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                    ->join('orders','order_items.order_id','=','orders.id')
                    ->where('order_items.restaurant_id',$request->data["restaurant_id"])
                    ->whereDay('orders.created_at',now()->day)
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('name')->get();
            }else{
                $plates=DB::table('order_items')->select(DB::raw("sum(quantity) as quantity,name"))
                ->where('restaurant_id',$request->data["restaurant_id"])
                ->whereDay('created_at',now()->day)
                ->groupBy('name')->get();
            }

        }

        return response()->json([
            "data"=>$plates
        ]);
    }










    public function getInventory(Request $request)
    {
        if($request->data["type"]=='year')
        {
            $year=substr($request->data["date"],0,4);
            if($request->type=='card'||$request->type=='cash'){
                
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                    ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                    ->join('orders','stock_enteries.order_id','=','orders.id')
                    ->whereYear('orders.created_at',$year)
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('stock_enteries.product')->get();


            }else{
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                    ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                    ->whereYear('stock_enteries.created_at',$year)
                    ->groupBy('stock_enteries.product')->get();
            }
        }
        else if($request->data["type"]=='day')
        {
            if($request->type=='card'||$request->type=='cash'){
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                    ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                    ->join('orders','stock_enteries.order_id','=','orders.id')
                    ->where('orders.created_at',$request->data["date"])
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('stock_enteries.product')->get();
            }else{
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                ->where('created_at',$request->data["date"])
                ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                ->groupBy('stock_enteries.product')->get();
            }
        }
        else if($request->data["type"]=='month')
        {
            $month=substr($request->data["date"],5,2);
            $year=substr($request->data["date"],0,4);
            if($request->type=='card'||$request->type=='cash'){
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                    ->join('orders','stock_enteries.order_id','=','orders.id')
                    ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                    ->whereYear('orders.created_at',$year)
                    ->whereMonth('orders.created_at',$month)
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('stock_enteries.product')->get();
            }else{
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                ->whereYear('created_at',$year)
                ->whereMonth('created_at',$month)
                ->groupBy('stock_enteries.product')->get();
            }

        }
        else
        {
            if($request->type=='card'||$request->type=='cash'){
                    $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                    ->join('orders','stock_enteries.order_id','=','orders.id')
                    ->where('stock_enteries.restaurant_id',$request->data["restaurant_id"])
                    ->whereDay('stock_enteries.created_at',now()->day)
                    ->where("orders.paymentMethod",$request->type)
                    ->groupBy('stock_enteries.product')->get();
            }else{
                $plates=DB::table('stock_enteries')->select(DB::raw("sum(quantity) as quantity,product"))
                ->where('restaurant_id',$request->data["restaurant_id"])
                ->whereDay('created_at',now()->day)
                ->groupBy('stock_enteries.product')->get();
            }

        }
        $products=Product::where("restaurant_id",$request->data["restaurant_id"])->get();
        foreach($plates as $plate){
            foreach($products as $product){
                if($product->product_name==$plate->product){
                    $plate->measuring_unit=$product->measuring_unit;
                }
            }
        }
        
        return response()->json([
            "data"=>$plates
        ]);
    } 






    public function countRevenue($restaurant_id){
        $orders = Order::select(
            DB::raw('sum(pay) as Amount'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
        )
        ->where('restaurant_id',$restaurant_id)
        ->whereYear('created_at', date('Y'))
        ->groupBy('monthKey')
        ->get();

        $data = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($orders as $order){
            $data[$order->monthKey-1] = $order->Amount;
        }

        return response()->json([
            "data"=>$data
        ]);
    }

    public function getRecord(){
        $orders=Order::all()->count();
        $users=User::all()->count();

        return response()->json([
            "data"=>[
                "orders"=>$orders,
                "users"=>$users
            ]
        ]);
    }


    
}
