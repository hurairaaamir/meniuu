<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\StockEnteries;
class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "message"=>$this->message,
            "Entries"=>$this->stock_entries,
            "created_at"=>$this->created_at
        ];
    }
}
