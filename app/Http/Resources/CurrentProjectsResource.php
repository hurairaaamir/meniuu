<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DeveloperResource;

class CurrentProjectsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $parentData=parent::toArray($request);

        // $additionalData=[
        //     'developers'=>$this->developers
        // ];

        // return array_merge($parentData,$additionalData);
        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "cost"=>$this->cost,
            "cost_currency"=>$this->cost_currency,
            "deadline"=>$this->deadline,
            "status"=>$this->status,
            "category"=>$this->category->name,
            'created_at'=>$this->created_at,
            "developers"=>DeveloperResource::collection($this->developers),
        ];
    }
}
