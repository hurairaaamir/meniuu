<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductEnteriesResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return[
            "retaurant_id"=>$this->restaurant_id,
            "category_name"=>$this->category_name,
            "products"=>$this->products
        ];
    }
}
