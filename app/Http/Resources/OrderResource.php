<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderItemResource;


class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "email"=>$this->email,
            "customer_name"=>$this->customer_name,
            "phone"=>$this->phone,
            "address"=>$this->address,
            "paymentMethod"=>$this->paymentMethod,
            "serviceMode"=>$this->serviceMode,
            "source"=>$this->source,
            "optional_note"=>$this->optional_note,
            "pay"=>$this->pay,
            "change"=>$this->change,
            "table"=>$this->table,
            "subtotal"=>$this->subtotal,
            "total"=>$this->total,
            "restaurant_id"=>$this->restaurant_id,
            "created_at"=>$this->created_at,
            "timestamp"=>$this->timestamp,
            "status"=>$this->status,
            "orderType"=>$this->orderType,
            "order_code"=>$this->order_code,
            "items"=>OrderItemResource::collection($this->items)

        ];
    }
}
