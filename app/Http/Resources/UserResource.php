<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $restaurant=$this->restaurant;
        if($restaurant){
            $cook=$restaurant->employee->where('role','cook')->count();
            $manager=$restaurant->employee->where('role','manager')->count();
            $cashier=$restaurant->employee->where('role','cashier')->count();
            $delivery_man=$restaurant->employee->where('role','delivery_man')->count();
            $waiter=$restaurant->employee->where('role','waiter')->count();
            return [
                "id"=>$this->id,
                "image"=>$this->image,
                'name'=>$this->name,
                "dehash_password"=>$this->dehash_password,
                "email"=>$this->email,
                'restaurant'=>$restaurant,
                'cook'=>$cook,
                "manager"=>$manager,
                "cashier"=>$cashier,
                "delivery_man"=>$delivery_man,
                "waiter"=>$waiter
            ];
        }else{
            return [
                "id"=>$this->id,
                "image"=>$this->image,
                'name'=>$this->name,
                "dehash_password"=>$this->dehash_password,
                "email"=>$this->email,
                'restaurant'=>[],
                'cook'=>'',
                "manager"=>'',
                "cashier"=>'',
                "delivery_man"=>'',
                "waiter"=>''
            ];
        }

        
    }
}
