<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\StockEnteries;
class ProductEnteriesStupid extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "message"=>$this->message,
            "Entries"=>StockEnteries::where('stock_id',$this->id)->where('type','output')->orderby('created_at','DESC')->get(),
            "created_at"=>$this->created_at
        ];
    }
}
