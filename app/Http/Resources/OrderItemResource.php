<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "ch_description"=>unserialize($this->ch_description),
            "extras"=>unserialize($this->extras),
            "price"=>$this->price,
            "name"=>$this->name,
            "quantity"=>$this->quantity,
            "status"=>$this->status,
            "note"=>$this->note,
            "total"=>$this->total,
            "order_id"=>$this->order_id
        ];
    }
}
