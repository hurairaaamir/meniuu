<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'come_with'=>unserialize($this->come_with),
            'description'=>$this->description,
            'price'=>$this->price,
            'image1'=>$this->image1,
            'image2'=>$this->image2,
            'image3'=>$this->image3,
            'image4'=>$this->image4,
            'choice1'=>$this->choice1,
            'choice2'=>$this->choice2,
            'choice3'=>$this->choice3,
            'choice4'=>$this->choice4,
            'status'=>$this->status,
            'ch1_description'=>unserialize($this->ch1_description),
            'ch2_description'=>unserialize($this->ch2_description),
            'ch3_description'=>unserialize($this->ch3_description),
            'ch4_description'=>unserialize($this->ch4_description),
            'extras'=>unserialize($this->extras),
            'send_to'=>$this->send_to,
            "dish_category_id"=>$this->dish_category_id
        ];
    }
}
