<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AllProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            "name"=>$this->name,
            "deadline"=>$this->deadline,
            "cost"=>$this->cost,
            "cost_currency"=>$this->cost_currency,
            "completion_time"=>$this->completion_time,
            "status"=>$this->status,
            "image"=>$this->image,
            "category"=>$this->category->name,
            "client_name"=>$this->client
        ];
    }
}
