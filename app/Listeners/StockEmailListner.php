<?php

namespace App\Listeners;

use App\Events\StockEmailEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Mail;
// implements ShouldQueue
class StockEmailListner 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StockEmailEvent  $event
     * @return void
     */
    public function handle(StockEmailEvent $event)
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;                      // Enable verbose debug output
            // $mail->SMTPDebug = 2;                      // Enable verbose debug output

            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.rigrex.com';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'restaurante@rigrex.com';                     // SMTP username
            $mail->Password   = 'RRClient_007';                               // SMTP password
            $mail->SMTPAutoTLS = false;
            $mail->SMTPSecure = false;
            $mail->Port       =  26;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('restaurante@rigrex.com', 'Meaal');
            $mail->addAddress($event->email);     // Add a recipient
            // $mail->addAddress('your-recipient@gmail.com');               // Name is optional
            
            

            $mail->isHTML(true);

            $mail->Subject = 'Contact';

            $mail->Body    = view('emails.stockEmail',[
                "product_name"=>$event->product["product_name"],
                "quantity"=>$event->product["quantity"],
                'type'=>$event->type,
                'measuring_unit'=>$event->product["measuring_unit"]
            ])->render();

            $mail->send();

        } catch (Exception $e) {
            print_r($e);
        }
    }
}
