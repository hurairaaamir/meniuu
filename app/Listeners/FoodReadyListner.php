<?php

namespace App\Listeners;

use App\Events\FoodReadyEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Mail;

// 
class FoodReadyListner implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FoodReadyEvent  $event
     * @return void
     */
    public function handle(FoodReadyEvent $event)
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;                      // Enable verbose debug output
            // $mail->SMTPDebug = 2;                      // Enable verbose debug output

            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.rigrex.com';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'restaurante@rigrex.com';                     // SMTP username
            $mail->Password   = 'RRClient_007';                               // SMTP password
            $mail->SMTPAutoTLS = false;
            $mail->SMTPSecure = false;
            $mail->Port       =  26;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('restaurante@rigrex.com', 'Meaal');
            $mail->addAddress($event->email);    
            
            

            $mail->isHTML(true);

            $mail->Subject = 'Contact';

            $mail->Body = view('emails.dishCooked',$event->data)->render();

            $mail->send();

        } catch (Exception $e) {
            print_r($e);
        }
    }
}
