<?php

namespace App\Listeners;

use App\Events\StockEnteryEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Dish;
use App\Product;
use App\StockEnteries;
use App\NotificationEmails;
use Exception;
class StockEnteryListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  StockEnteryEvent  $event
     * @return void
     */
    public function handle(StockEnteryEvent $event)
    {
        try{
            $order_id=$event->order_id;
            $dish=Dish::where("name",$event->name)->where('restaurant_id',$event->restaurant_id)->first();
            foreach($dish->DishIngredients as $ingredient){
                $product=Product::where('product_name',$ingredient->product)->first();
                $product->quantity=$product->quantity-$ingredient->quantity;
                $product->save();
                
                $this->checkStock($product);


                StockEnteries::create([
                    'product'=>$ingredient->product,
                    'category'=>$ingredient->category,
                    'measuring_unit'=>$ingredient->measuring_unit,
                    'quantity'=>$ingredient->quantity,
                    "order_id"=>$order_id,
                    "restaurant_id"=>$event->restaurant_id,
                    'stock_id'=>null
                ]);
            }
        }catch(Exception $e){

        }
    }

    private function checkStock($product){
        $min_diff= (float)$product->quantity - (float)$product->min_stock;
        $max_diff=(float)$product->quantity - (float)$product->max_stock;
        $opt_diff=(float)$product->quantity - (float)$product->opt_stock;
        $emails=NotificationEmails::where('restaurant_id',$product->restaurant_id)->first();

        if(-5<$min_diff && 5>$min_diff&&$emails->min_email){
            event(new \App\Events\StockEmailEvent($product,'min',$emails->min_email));
        }
        if(-5<$max_diff && 5>$max_diff&&$emails->max_email){
            event(new \App\Events\StockEmailEvent($product,'max',$emails->max_email));
        }
        if(-5<$opt_diff && 5>$opt_diff&&$emails->opt_email){
            event(new \App\Events\StockEmailEvent($product,'opt',$emails->opt_email));
        }
    }
}