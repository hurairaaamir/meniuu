<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['product_name','measuring_unit','min_stock','max_stock','opt_stock','product_category_id','restaurant_id','quantity'];

    public function ProductCategory(){
        return $this->belongsTo(ProductCategory::class);
    }
}


