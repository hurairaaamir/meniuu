(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _frontend_components_sidebar_Sidebar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../frontend-components/sidebar/Sidebar */ "./resources/js/src/layouts/frontend-components/sidebar/Sidebar.vue");
/* harmony import */ var _frontend_components_topbar_Topbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../frontend-components/topbar/Topbar */ "./resources/js/src/layouts/frontend-components/topbar/Topbar.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // import siteNav from '../frontend-components/nav.vue'

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      sidebar: true
    };
  },
  components: {
    Topbar: _frontend_components_topbar_Topbar__WEBPACK_IMPORTED_MODULE_1__["default"],
    Sidebar: _frontend_components_sidebar_Sidebar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
    var pageWidth = window.innerWidth > screen.width ? screen.width : window.innerWidth;

    if (pageWidth < 800) {
      this.sidebar = false;
    } else {
      this.sidebar = true;
    }

    next();
  },
  computed: {
    loginPage: function loginPage() {
      console.log(window.location.href);

      if (window.location.href.includes("login")) {
        return false;
      }

      return true;
    }
  },
  methods: {
    manageSidebar: function manageSidebar() {
      var pageWidth = window.innerWidth > screen.width ? screen.width : window.innerWidth;

      if (pageWidth < 800) {
        this.sidebar = false;
      } else {
        this.sidebar = true;
      }
    }
  },
  watch: {
    sidebar: function sidebar(newval, oldval) {
      console.log(newval);
    }
  },
  created: function created() {
    var _this = this;

    this.manageSidebar();
    window.addEventListener("resize", function () {
      _this.manageSidebar();
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css);", ""]);
exports.push([module.i, "@import url(https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;500;600;700;800&family=Open+Sans:wght@300;400;600;700;800&display=swap);", ""]);

// module
exports.push([module.i, ".extra-features,\n.categories-list {\n  overflow-x: scroll;\n}\n.extra-features table,\n.categories-list table {\n  min-width: 400px;\n}\n.layout--full-page {\n  min-height: 100vh;\n}\n.overlay {\n  display: none;\n}\n@media (max-width: 800px) {\n.overlay {\n    display: block;\n    position: fixed;\n    top: 0;\n    height: 100%;\n    width: 100%;\n    z-index: 1;\n}\n[dir] .overlay {\n    background: rgba(0, 0, 0, 0.521);\n}\n[dir=ltr] .overlay {\n    left: 0;\n}\n[dir=rtl] .overlay {\n    right: 0;\n}\n}\n.nav-animation {\n  transition: all 0.5s ease ease-in-out;\n}\n.sidebar-holder {\n  width: 0;\n  transition: all 0.3s ease-in-out;\n}\n.sidebar-holder.active {\n  width: 309px;\n}\n@-webkit-keyframes navanim-ltr {\n0% {\n    transform: translateX(-100%);\n}\n100% {\n    transform: translateX(0%);\n}\n}\n@keyframes navanim-ltr {\n0% {\n    transform: translateX(-100%);\n}\n100% {\n    transform: translateX(0%);\n}\n}\n@-webkit-keyframes navanim-rtl {\n0% {\n    transform: translateX(100%);\n}\n100% {\n    transform: translateX(0%);\n}\n}\n@keyframes navanim-rtl {\n0% {\n    transform: translateX(100%);\n}\n100% {\n    transform: translateX(0%);\n}\n}\n.p-relative {\n  position: relative;\n}\n*:focus {\n  outline: none;\n}\n.main {\n  overflow: hidden;\n  width: 100%;\n}\n.template-layout {\n  display: flex;\n  transition: all 0.3s ease-in-out;\n}\n[dir] .template-layout {\n  background-color: var(--body-color);\n}\n.template-layout .sidebar_ {\n  position: fixed;\n  width: 260px;\n  top: 60px;\n  height: 100%;\n  z-index: 9;\n  transition: all 0.3s ease-in-out;\n}\n[dir=ltr] .template-layout .sidebar_ {\n  box-shadow: 2px 4px 4px 0px #555;\n  transform: translateX(-100%);\n}\n[dir=rtl] .template-layout .sidebar_ {\n  box-shadow: -2px 4px 4px 0px #555;\n  transform: translateX(100%);\n}\n.template-layout .main {\n  transition: all 0.3s ease-in-out;\n}\n.template-layout.col-template .main {\n  width: calc(100% - 262px);\n}\n[dir] .template-layout.col-template .sidebar_ {\n  transform: translateX(0);\n}\n.btn-round-blue {\n  width: 180px;\n  height: 46px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  color: #fff;\n  text-decoration: none;\n  font-family: \"Open Sans\", sans-serif;\n  font-style: normal;\n  font-weight: bold;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n}\n[dir] .btn-round-blue {\n  background: var(--btn-color);\n  border-radius: 37.5px;\n  border: 1px solid var(--main-color);\n}\n.btn-round-blue:hover {\n  text-decoration: none;\n  color: #fff;\n}\n[dir] .btn-round-blue:hover {\n  box-shadow: inset 0px 0px 4px 0px #eee;\n}\n.btn-white {\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 14px;\n  font-weight: bold;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n  color: var(--main-color);\n}\n[dir] .btn-white {\n  background-color: #fff;\n  box-shadow: 0px 0px 2px 0px #777;\n}\n.btn-round {\n  height: 50px;\n  width: 50px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  color: #fff;\n  font-family: \"Montserrat\", sans-serif;\n}\n[dir] .btn-round {\n  border-radius: 50%;\n  background-color: var(--main-color);\n  border: 1px solid var(--main-color);\n}\n.btn.bg-white:hover {\n  color: #fff !important;\n}\n[dir] .btn.bg-white:hover {\n  background: var(--main-color) !important;\n}\n.btn.bg-white:hover svg,\n.btn.bg-white:hover svg path {\n  fill: #fff;\n}\n[dir] .row-list {\n  margin: 2px 0;\n}\n[dir] .row-list:hover:nth-child(even) {\n  background-color: var(--main-color) !important;\n}\n[dir] .row-list:hover:nth-child(odd) {\n  background-color: var(--hover-color) !important;\n}\n.vue-tags-input {\n  max-width: unset !important;\n}\n[dir] .vue-tags-input {\n  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.15);\n}\n[dir] .vue-tags-input .ti-input {\n  border: none;\n}\n.back-btn {\n  position: absolute;\n  top: 10px;\n  height: 40px;\n  width: 40px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  color: var(--main-color);\n  fill: var(--main-color);\n}\n[dir] .back-btn {\n  background-color: #eee;\n  border-radius: 50%;\n}\n[dir=ltr] .back-btn {\n  left: 20px;\n}\n[dir=rtl] .back-btn {\n  right: 20px;\n}\n[dir] .back-btn:hover {\n  box-shadow: 0px 0px 4px 0px #777;\n}\n[dir] .pointer {\n  cursor: pointer;\n}\n@media (max-width: 800px) {\n.template-layout {\n    display: grid;\n    grid-template-columns: 1fr;\n}\n.template-layout.col-template {\n    grid-template-columns: 1fr;\n}\n[dir] .template-layout.col-template .main {\n    transform: translateX(0);\n}\n.template-layout.col-template .main {\n    width: calc(100%);\n}\n}\n.fadeHeight-enter-active,\n.fadeHeight-leave-active {\n  transition: all 0.4s;\n  max-height: 236;\n}\n.fadeHeight-enter,\n.fadeHeight-leave-to {\n  opacity: 0;\n  max-height: 0px;\n}\n.router-link-exact-active ~ a {\n  display: grid;\n  grid-template-columns: 1fr;\n}\n.add-extra {\n  z-index: 9 !important;\n}\n[dir=ltr] .multiselect__tags {\n  border-top-right-radius: 0;\n  border-bottom-right-radius: 0;\n}\n[dir=rtl] .multiselect__tags {\n  border-top-left-radius: 0;\n  border-bottom-left-radius: 0;\n}\n@media (max-width: 500px) {\n[dir] .inventory {\n    padding: 20px 0 !important;\n}\n[dir] .inventory .card {\n    padding: 20px 0 !important;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SidebarPage.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=template&id=68cf994b&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=template&id=68cf994b& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "layout--full-page" },
    [
      _vm.sidebar
        ? _c("div", {
            staticClass: "overlay",
            on: {
              click: function($event) {
                _vm.sidebar = false
              }
            }
          })
        : _vm._e(),
      _vm._v(" "),
      _vm.loginPage
        ? _c("Topbar", {
            on: {
              toogleSidebar: function($event) {
                _vm.sidebar = !_vm.sidebar
              }
            }
          })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "template-layout",
          class: { "col-template": _vm.sidebar && _vm.loginPage }
        },
        [
          _c(
            "div",
            { staticClass: "sidebar_" },
            [
              _vm.loginPage
                ? _c("Sidebar", { staticClass: "nav-animation" })
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("div", {
            staticClass: "sidebar-holder",
            class: { active: _vm.sidebar }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "main" },
            [_c("router-view"), _vm._v(" "), _c("vue-progress-bar")],
            1
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue":
/*!***************************************************************!*\
  !*** ./resources/js/src/layouts/sidebar-page/SidebarPage.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SidebarPage_vue_vue_type_template_id_68cf994b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SidebarPage.vue?vue&type=template&id=68cf994b& */ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=template&id=68cf994b&");
/* harmony import */ var _SidebarPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SidebarPage.vue?vue&type=script&lang=js& */ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SidebarPage.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _SidebarPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SidebarPage_vue_vue_type_template_id_68cf994b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SidebarPage_vue_vue_type_template_id_68cf994b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/layouts/sidebar-page/SidebarPage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SidebarPage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SidebarPage.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=template&id=68cf994b&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=template&id=68cf994b& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_template_id_68cf994b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SidebarPage.vue?vue&type=template&id=68cf994b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/sidebar-page/SidebarPage.vue?vue&type=template&id=68cf994b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_template_id_68cf994b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SidebarPage_vue_vue_type_template_id_68cf994b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);