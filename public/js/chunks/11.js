(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import { EventBus } from './event-bus.js';
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      errors: new Errors(),
      sidebar: true,
      loginbtn: true,
      show: {
        profile: false,
        authPopup: false,
        auth: false
      },
      user: {
        email: "",
        password: "",
        password_confirmation: "",
        name: ""
      }
    };
  },
  created: function created() {
    var _this = this;

    this.$eventHub.$on("openSidebarTable", this.toogleSidebar);
    this.manageSidebar();
    window.addEventListener("resize", function () {
      _this.manageSidebar();
    });
    document.addEventListener("touchmove", process_touchmove, false);
    var touch = 60;
    var touchend = 0;

    function process_touchmove(ev) {
      if (touch > ev.touches[0].pageX) {
        touch = ev.touches[0].pageX;
      }

      touchend = ev.touches[0].pageX; // console.log(ev.touches[0].pageX);
    }

    document.addEventListener('touchend', function () {
      if (touchend >= 60 && touch < 20) {
        _this.sidebar = true;
        console.log(touch);
      }

      touch = 60;
      touchend = 0;
    }, false);
  },
  computed: {
    restaurant: function restaurant() {
      return this.$store.state.restaurant;
    },
    loggedIn: function loggedIn() {
      return this.$store.getters.loggedIn;
    }
  },
  methods: {
    closeModel: function closeModel() {
      this.show = {
        profile: false,
        authPopup: false,
        auth: false
      };
    },
    manageSidebar: function manageSidebar() {
      var pageWidth = window.innerWidth > screen.width ? screen.width : window.innerWidth;

      if (pageWidth < 800) {
        this.sidebar = false;
      } else {
        this.sidebar = true;
      }
    },
    shiftBtn: function shiftBtn() {
      this.loginbtn = !this.loginbtn;
      this.errors.clear("name");
    },
    toogleSidebar: function toogleSidebar() {
      this.sidebar = !this.sidebar;
    },
    showProfile: function showProfile() {
      this.show.profile = !this.show.profile;
    },
    loginCustomer: function loginCustomer() {
      var _this2 = this;

      this.$Progress.start();
      this.$store.dispatch("retrieveTokenCustomer", this.user).then(function (response) {
        _this2.$Progress.start();

        _this2.$store.dispatch("getCustomer").then(function (respones) {
          _this2.$Progress.finish();

          _this2.show.profile = false;
          _this2.show.authPopup = false;
          _this2.show.auth = false;
        });
      }).catch(function (errors) {
        if (errors == "Error: Request failed with status code 400") {
          _this2.$refs.login_error.innerHTML = "Invalid email or password";
        } else {
          _this2.errors.record(errors.response.data);
        }
      });
    },
    createCustomer: function createCustomer() {
      var _this3 = this;

      this.$Progress.start();
      this.$store.dispatch("createCustomer", this.user).then(function (response) {
        _this3.loginCustomer();
      }).catch(function (errors) {
        _this3.errors.record(errors.response.data);
      });
    },
    logOut: function logOut() {
      var _this4 = this;

      this.$store.dispatch("logout").then(function (response) {
        _this4.$store.dispatch('resetUser');
      });
    },
    changeLng: function changeLng(type) {
      if (this.$i18n.locale != type) {
        this.$i18n.locale = type;
      } else {
        this.$i18n.locale = 'mx';
      }
    }
  },
  mounted: function mounted() {
    var _this5 = this;

    window.addEventListener("click", function (e) {
      if (e.target == document.querySelector(".popup")) {
        _this5.show.profile = false;
        _this5.show.authPopup = false;
        _this5.show.auth = false;
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".overlay-sidebar[data-v-1f5dcc50] {\n  display: none;\n}\n@media (max-width: 800px) {\n.overlay-sidebar[data-v-1f5dcc50] {\n    display: block;\n    position: fixed;\n    top: 0;\n    height: 100%;\n    width: 100%;\n    z-index: 1;\n}\n[dir] .overlay-sidebar[data-v-1f5dcc50] {\n    background: rgba(0, 0, 0, 0.521);\n}\n[dir=ltr] .overlay-sidebar[data-v-1f5dcc50] {\n    left: 0;\n}\n[dir=rtl] .overlay-sidebar[data-v-1f5dcc50] {\n    right: 0;\n}\n}\nlabel[data-v-1f5dcc50],\nbutton[data-v-1f5dcc50],\na[data-v-1f5dcc50] {\n  font-family: \"Montserrat\", sans-serif;\n}\n.btn-round-primary[data-v-1f5dcc50] {\n  width: 100%;\n  max-width: 300px;\n  width: 100%;\n  color: #000;\n  text-decoration: none;\n}\n[dir] .btn-round-primary[data-v-1f5dcc50] {\n  background-color: var(--main-color);\n  margin: 40px auto;\n  border-radius: 19px;\n  padding: 10px;\n  border: 1px solid var(--smain-color);\n  text-align: center;\n}\n[dir=ltr] .btn-round-primary[data-v-1f5dcc50] {\n  box-shadow: 1px 2px 3px 0px rgba(0, 0, 0, 0);\n}\n[dir=rtl] .btn-round-primary[data-v-1f5dcc50] {\n  box-shadow: -1px 2px 3px 0px rgba(0, 0, 0, 0);\n}\n.btn-round-primary[data-v-1f5dcc50]:hover {\n  color: #000;\n}\n.btn-round-primary.br-0[data-v-1f5dcc50] {\n  max-width: unset !important;\n}\n[dir] .btn-round-primary.br-0[data-v-1f5dcc50] {\n  border-radius: 0 !important;\n  margin-bottom: 10px !important;\n}\n.popup[data-v-1f5dcc50] {\n  position: fixed;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  display: flex;\n  justify-content: center;\n  z-index: 9;\n  overflow-y: scroll;\n}\n[dir] .popup[data-v-1f5dcc50] {\n  background: rgba(0, 0, 0, 0.253);\n}\n[dir=ltr] .popup[data-v-1f5dcc50] {\n  left: 0;\n}\n[dir=rtl] .popup[data-v-1f5dcc50] {\n  right: 0;\n}\n.popup .profile-popup[data-v-1f5dcc50] {\n  max-width: 600px;\n  width: 100%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n}\n[dir] .popup .profile-popup[data-v-1f5dcc50] {\n  margin-top: 30px;\n  padding: 35px;\n  background-color: #fff;\n  border-radius: 12px;\n}\n.popup .profile-popup .copy-cat[data-v-1f5dcc50] {\n  width: 100%;\n}\n[dir] .popup .profile-popup .copy-cat[data-v-1f5dcc50] {\n  text-align: center;\n}\n.popup .profile-popup .img-wraper[data-v-1f5dcc50] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.popup .profile-popup .img-wraper img[data-v-1f5dcc50] {\n  width: 200px;\n}\n[dir] .popup .profile-popup .restaurant[data-v-1f5dcc50] {\n  margin-top: 30px;\n}\n.popup .profile-popup .restaurant h3[data-v-1f5dcc50] {\n  font-family: \"Montserrat\", sans-serif;\n  font-size: 16px;\n  font-weight: 500;\n}\n[dir] .popup .profile-popup .restaurant h3[data-v-1f5dcc50] {\n  text-align: center;\n}\n.popup .profile-popup .btn-round-blue[data-v-1f5dcc50] {\n  width: 100%;\n  color: #fff;\n  font-size: 17px;\n}\n[dir] .popup .profile-popup .btn-round-blue[data-v-1f5dcc50] {\n  background: var(--btn-color);\n  border-radius: 37.5px;\n  border: 1px solid var(--main-color);\n  padding: 10px;\n}\n.popup .profile-popup h6[data-v-1f5dcc50] {\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 18px;\n}\n[dir] .popup .profile-popup h6[data-v-1f5dcc50] {\n  text-align: center;\n  margin-bottom: 40px;\n}\n.popup .profile-popup .form-group label[data-v-1f5dcc50] {\n  width: 100%;\n}\n[dir] .popup .profile-popup .form-group textarea[data-v-1f5dcc50], [dir] .popup .profile-popup .form-group select[data-v-1f5dcc50], [dir] .popup .profile-popup .form-group input[data-v-1f5dcc50] {\n  border: none;\n}\n[dir=ltr] .popup .profile-popup .form-group textarea[data-v-1f5dcc50], [dir=ltr] .popup .profile-popup .form-group select[data-v-1f5dcc50], [dir=ltr] .popup .profile-popup .form-group input[data-v-1f5dcc50] {\n  box-shadow: 1px 1px 8px 1px rgba(0, 0, 0, 0.16);\n}\n[dir=rtl] .popup .profile-popup .form-group textarea[data-v-1f5dcc50], [dir=rtl] .popup .profile-popup .form-group select[data-v-1f5dcc50], [dir=rtl] .popup .profile-popup .form-group input[data-v-1f5dcc50] {\n  box-shadow: -1px 1px 8px 1px rgba(0, 0, 0, 0.16);\n}\n[dir=ltr] .popup .profile-popup .form-group textarea[data-v-1f5dcc50]:focus, [dir=ltr] .popup .profile-popup .form-group select[data-v-1f5dcc50]:focus, [dir=ltr] .popup .profile-popup .form-group input[data-v-1f5dcc50]:focus {\n  box-shadow: 1px 1px 8px 1px rgba(0, 0, 0, 0.16);\n}\n[dir=rtl] .popup .profile-popup .form-group textarea[data-v-1f5dcc50]:focus, [dir=rtl] .popup .profile-popup .form-group select[data-v-1f5dcc50]:focus, [dir=rtl] .popup .profile-popup .form-group input[data-v-1f5dcc50]:focus {\n  box-shadow: -1px 1px 8px 1px rgba(0, 0, 0, 0.16);\n}\n.sidebar[data-v-1f5dcc50] {\n  position: fixed;\n  top: 0;\n  width: 260px;\n  height: 100%;\n  z-index: 1;\n}\n[dir] .sidebar[data-v-1f5dcc50] {\n  background-color: #fff;\n  box-shadow: 0px 0px 4px 0 #ccc;\n}\n[dir=ltr] .sidebar[data-v-1f5dcc50] {\n  left: 0;\n}\n[dir=rtl] .sidebar[data-v-1f5dcc50] {\n  right: 0;\n}\n.sidebar-wraper .profile[data-v-1f5dcc50] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\n[dir] .sidebar-wraper .profile[data-v-1f5dcc50] {\n  padding: 20px 10px;\n  text-align: center !important;\n  background-color: white;\n}\n.sidebar-wraper .profile h4[data-v-1f5dcc50] {\n  font-family: \"Montserrat\", sans-serif;\n  color: var(--font-color);\n  font-size: 14px;\n  line-height: 16px;\n  font-weight: 600;\n}\n.sidebar-wraper .list[data-v-1f5dcc50] {\n  display: flex;\n  align-items: center;\n  height: 45px;\n  font-size: 16px;\n  color: var(--font-color);\n  transition: all 0.4s ease;\n  text-decoration: none;\n}\n[dir] .sidebar-wraper .list[data-v-1f5dcc50] {\n  margin: 2px 0;\n  cursor: pointer;\n}\n[dir=ltr] .sidebar-wraper .list[data-v-1f5dcc50] {\n  padding-right: 20px;\n  padding-left: 20px;\n}\n[dir=rtl] .sidebar-wraper .list[data-v-1f5dcc50] {\n  padding-left: 20px;\n  padding-right: 20px;\n}\n.sidebar-wraper .list .icon[data-v-1f5dcc50] {\n  display: flex;\n  align-items: center;\n}\n.sidebar-wraper .list img[data-v-1f5dcc50] {\n  height: 29px;\n}\n[dir=ltr] .sidebar-wraper .list img[data-v-1f5dcc50] {\n  padding: 4px 10px 4px 20px;\n}\n[dir=rtl] .sidebar-wraper .list img[data-v-1f5dcc50] {\n  padding: 4px 20px 4px 10px;\n}\n[dir] .sidebar-wraper .list[data-v-1f5dcc50]:hover, [dir] .sidebar-wraper .list.active[data-v-1f5dcc50] {\n  background-color: var(--main-color);\n}\n[dir=ltr] .sidebar-wraper .list[data-v-1f5dcc50]:hover, [dir=ltr] .sidebar-wraper .list.active[data-v-1f5dcc50] {\n  border-left: 4px solid var(--font-color);\n}\n[dir=rtl] .sidebar-wraper .list[data-v-1f5dcc50]:hover, [dir=rtl] .sidebar-wraper .list.active[data-v-1f5dcc50] {\n  border-right: 4px solid var(--font-color);\n}\n.sidebar[data-v-1f5dcc50] {\n  transition: all 0.3s ease-in-out;\n}\n[dir=ltr] .sidebar[data-v-1f5dcc50] {\n  transform: translateX(-100%);\n}\n[dir=rtl] .sidebar[data-v-1f5dcc50] {\n  transform: translateX(100%);\n}\n.router-view[data-v-1f5dcc50] {\n  transition: all 0.3s ease-in-out;\n  position: relative;\n  min-height: 100vh;\n  width: 100%;\n}\n[dir] .router-view[data-v-1f5dcc50] {\n  padding: 0 !important;\n}\n.router-view .overlay[data-v-1f5dcc50] {\n  display: none;\n  position: fixed;\n  top: 0;\n  height: 100%;\n  width: 100%;\n}\n[dir] .router-view .overlay[data-v-1f5dcc50] {\n  background: transparent;\n}\n[dir=ltr] .router-view .overlay[data-v-1f5dcc50] {\n  left: 0;\n}\n[dir=rtl] .router-view .overlay[data-v-1f5dcc50] {\n  right: 0;\n}\n.wraper[data-v-1f5dcc50] {\n  display: flex;\n}\n.wraper .sidebar-holder[data-v-1f5dcc50] {\n  transition: all 0.3s ease-in-out;\n}\n.wraper.active[data-v-1f5dcc50] {\n  display: grid;\n  grid-template-columns: 260px 1fr;\n}\n[dir] .wraper.active .sidebar[data-v-1f5dcc50] {\n  transform: translateX(0);\n}\n.wraper.active .router-view[data-v-1f5dcc50] {\n  width: 100%;\n}\n.wraper.active .router-view .overlay[data-v-1f5dcc50] {\n  display: block !important;\n}\n@media (max-width: 800px) {\n.wraper[data-v-1f5dcc50] {\n    display: grid;\n    grid-template-columns: 0px 1fr;\n}\n.wraper .sidebar-holder[data-v-1f5dcc50] {\n    transition: all 0.3s ease-in-out;\n}\n.wraper.active[data-v-1f5dcc50] {\n    grid-template-columns: 0px 1fr;\n}\n[dir] .wraper.active .sidebar[data-v-1f5dcc50] {\n    transform: translateX(0);\n}\n.wraper.active .router-view[data-v-1f5dcc50] {\n    width: 100%;\n}\n[dir] .wraper.active .router-view[data-v-1f5dcc50] {\n    transform: translateX(0);\n}\n.wraper.active .router-view .overlay[data-v-1f5dcc50] {\n    display: block !important;\n}\n}\n@media (max-width: 576px) {\n[dir] .router-view[data-v-1f5dcc50] {\n    padding: 0 !important;\n}\n}\n.slide-fade-enter-active[data-v-1f5dcc50] {\n  transition: all 0.3s ease;\n}\n.slide-fade-leave-active[data-v-1f5dcc50] {\n  transition: all 0.3s cubic-bezier(1, 0.5, 0.8, 1);\n}\n.slide-fade-enter[data-v-1f5dcc50] {\n  opacity: 0;\n}\n[dir] .slide-fade-enter[data-v-1f5dcc50] {\n  transform: translateY(-10px);\n}\n.slide-fade-leave-to[data-v-1f5dcc50] {\n  opacity: 0;\n}\n[dir] .slide-fade-leave-to[data-v-1f5dcc50] {\n  transform: translateY(10px);\n}\n.status-outer[data-v-1f5dcc50] {\n  width: 100px;\n  height: 100px;\n  display: inline-block;\n}\n[dir] .status-outer[data-v-1f5dcc50] {\n  border: 2px solid #989898;\n  border-radius: 50%;\n}\n[dir] .img-border img[data-v-1f5dcc50] {\n  border-radius: 100%;\n  padding: 3px;\n  border: 5px solid var(--main-color);\n  margin: 3px;\n}\n.name2[data-v-1f5dcc50] {\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: normal;\n  font-size: 20px;\n  line-height: 27px;\n  color: var(--font-color);\n}\n.status[data-v-1f5dcc50] {\n  font-family: \"Open Sans\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 14px;\n  line-height: 19px;\n  color: var(--font-color);\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.language[data-v-1f5dcc50] {\n  list-style: none;\n  position: relative;\n}\n[dir] .language[data-v-1f5dcc50] {\n  text-align: center;\n  cursor: pointer;\n}\n.language .dropdown[data-v-1f5dcc50] {\n  min-width: 100%;\n  display: none;\n  position: absolute;\n  z-index: 999;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  top: 20px;\n}\n[dir] .language .dropdown[data-v-1f5dcc50] {\n  border: solid 1px #e3dcdc;\n}\n[dir=ltr] .language .dropdown[data-v-1f5dcc50] {\n  left: 0;\n}\n[dir=rtl] .language .dropdown[data-v-1f5dcc50] {\n  right: 0;\n}\n.language .dropdown .active[data-v-1f5dcc50] {\n  color: white;\n}\n[dir] .language .dropdown .active[data-v-1f5dcc50] {\n  background-color: #E5D362 !important;\n}\n.language:hover .dropdown[data-v-1f5dcc50] {\n  display: flex;\n}\n[dir] .language .dropdown li[data-v-1f5dcc50] {\n  padding: 3px 35px;\n  background-color: white;\n}\n.dropdown:hover .dropdown[data-v-1f5dcc50] {\n  display: flex;\n}\n[dir] .name[data-v-1f5dcc50] {\n  margin: 0px 10px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "wraper ", class: { active: _vm.sidebar } },
    [
      _c("div", { staticClass: "sidebar" }, [
        _c("div", { staticClass: "sidebar-wraper" }, [
          _c("div", { staticClass: "profile" }, [
            _c("span", { staticClass: "status-outer" }, [
              _c("span", { staticClass: "img-border" }, [
                _c("img", {
                  staticStyle: {
                    "border-radius": "100%",
                    width: "90px",
                    height: "90px"
                  },
                  attrs: { src: _vm.restaurant.logo, alt: "" }
                })
              ])
            ]),
            _vm._v(" "),
            _c("h3", { staticClass: "name2 mt-2" }, [
              _vm._v(_vm._s(_vm.restaurant.name))
            ]),
            _vm._v(" "),
            _c("h4", [_vm._v(_vm._s(_vm.$store.state.user.name))]),
            _vm._v(" "),
            _vm.restaurant.status == "open"
              ? _c("span", { staticClass: "status" }, [
                  _c(
                    "svg",
                    {
                      staticClass: "Online",
                      attrs: {
                        width: "16",
                        height: "16",
                        viewBox: "0 0 16 16",
                        xmlns: "http://www.w3.org/2000/svg"
                      }
                    },
                    [
                      _c("circle", {
                        attrs: {
                          "data-v-471d35ec": "",
                          cx: "8",
                          cy: "8",
                          r: "8",
                          fill: "green"
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c("span", { staticClass: "ml-2" }, [_vm._v("Open")]),
                  _vm._v(" "),
                  _c("span", { staticClass: "language name" }, [
                    _c("span", [
                      _vm._v(
                        "\n                        (" +
                          _vm._s(_vm.$i18n.locale == "en" ? "EN" : "ES") +
                          ")\n                    "
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "ul",
                      {
                        staticClass: "dropdown",
                        attrs: { "aria-labelledby": "navbarDropdown" }
                      },
                      [
                        _c(
                          "li",
                          {
                            class: { active: _vm.$i18n.locale == "en" },
                            on: {
                              click: function($event) {
                                return _vm.changeLng("en")
                              }
                            }
                          },
                          [_vm._v("EN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class: { active: _vm.$i18n.locale == "mx" },
                            on: {
                              click: function($event) {
                                return _vm.changeLng("mx")
                              }
                            }
                          },
                          [_vm._v("ES")]
                        )
                      ]
                    )
                  ])
                ])
              : _c("span", { staticClass: "status" }, [
                  _c(
                    "svg",
                    {
                      staticClass: "Offline",
                      attrs: {
                        width: "16",
                        height: "16",
                        viewBox: "0 0 16 16",
                        xmlns: "http://www.w3.org/2000/svg"
                      }
                    },
                    [
                      _c("circle", {
                        attrs: {
                          "data-v-471d35ec": "",
                          cx: "8",
                          cy: "8",
                          r: "8",
                          fill: "red"
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "ml-2" }, [_vm._v("Close")])
                ])
          ]),
          _vm._v(" "),
          _vm.loggedIn
            ? _c(
                "div",
                { staticClass: "list", on: { click: _vm.showProfile } },
                [
                  _c("span", { staticClass: "icon mr-2" }, [
                    _c(
                      "svg",
                      {
                        attrs: {
                          width: "28",
                          height: "36",
                          viewBox: "0 0 36 36",
                          fill: "none",
                          xmlns: "http://www.w3.org/2000/svg"
                        }
                      },
                      [
                        _c("path", {
                          attrs: {
                            "fill-rule": "evenodd",
                            "clip-rule": "evenodd",
                            d:
                              "M25 10C25 13.866 21.866 17 18 17C14.134 17 11 13.866 11 10C11 6.13401 14.134 3 18 3C21.866 3 25 6.13401 25 10ZM18 18.9984C22.7897 18.9984 27.3615 21.0003 30.61 24.52C30.8536 24.7892 30.992 25.1371 31 25.5V31.5C31 32.3284 30.3284 33 29.5 33H6.5C5.67932 33.0001 5.01094 32.3406 5 31.52V25.52C5.00314 25.1502 5.14192 24.7943 5.39 24.52C8.63854 21.0003 13.2103 18.9984 18 18.9984Z",
                            fill: "#737373"
                          }
                        })
                      ]
                    )
                  ]),
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.$t("My Profile")) +
                      "\n            "
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.loggedIn
            ? _c("div", { staticClass: "list", on: { click: _vm.logOut } }, [
                _c("span", { staticClass: "icon mr-2" }, [
                  _c(
                    "svg",
                    {
                      attrs: {
                        width: "28",
                        height: "36",
                        viewBox: "0 0 36 36",
                        fill: "none",
                        xmlns: "http://www.w3.org/2000/svg"
                      }
                    },
                    [
                      _c("path", {
                        attrs: {
                          "fill-rule": "evenodd",
                          "clip-rule": "evenodd",
                          d:
                            "M22 6H6V30H24C24 31.1046 23.1046 32 22 32H6C4.89543 32 4 31.1046 4 30V6C4 4.89543 4.89543 4 6 4H22C23.1046 4 24 4.89543 24 6V15.8H22V6ZM25.8024 17.3324C26.1718 16.9631 26.7632 16.9402 27.16 17.28L33 23.07L27.16 28.87C26.9156 29.1554 26.5319 29.2797 26.1666 29.1918C25.8013 29.1039 25.5161 28.8187 25.4282 28.4534C25.3403 28.0881 25.4646 27.7044 25.75 27.46L29.13 24H14.63C14.0777 24 13.63 23.5523 13.63 23C13.63 22.4477 14.0777 22 14.63 22H29.13L25.75 18.69C25.4102 18.2932 25.4331 17.7018 25.8024 17.3324Z",
                          fill: "#737373"
                        }
                      })
                    ]
                  )
                ]),
                _vm._v(
                  "\n                " +
                    _vm._s(_vm.$t("Logout")) +
                    "\n            "
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          !_vm.loggedIn
            ? _c(
                "div",
                {
                  staticClass: "list",
                  on: {
                    click: function($event) {
                      _vm.show.auth = true
                    }
                  }
                },
                [
                  _vm._m(0),
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.$t("Login")) +
                      "\n            "
                  )
                ]
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", {
        staticClass: "sidebar-holder",
        class: { active: _vm.sidebar }
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "router-view" },
        [
          _c("router-view"),
          _vm._v(" "),
          _c("vue-progress-bar"),
          _vm._v(" "),
          _vm.sidebar
            ? _c("div", {
                staticClass: "overlay-sidebar",
                on: {
                  click: function($event) {
                    _vm.sidebar = false
                  }
                }
              })
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "slide-fade" } }, [
        _vm.show.profile
          ? _c("div", { staticClass: "popup" }, [
              _c("div", { staticClass: "profile-popup" }, [
                _c("h6", [_vm._v("Help us to give you an exclusive service")]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.$t("Name")) +
                        "\n                        "
                    ),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: { type: "text", disabled: "" },
                      domProps: { value: _vm.$store.state.user.name }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.$t("Birthday")) +
                        "\n                        "
                    ),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: { type: "date" }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.$t("Gender")) +
                        "\n                        "
                    ),
                    _c(
                      "select",
                      {
                        staticClass: "form-control",
                        attrs: { name: "", id: "" }
                      },
                      [
                        _c("option", { attrs: { value: "" } }, [
                          _vm._v(_vm._s(_vm.$t("Male")))
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "" } }, [
                          _vm._v(_vm._s(_vm.$t("Female")))
                        ])
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.$t("Gastronomic Preferences")) +
                        "\n                        "
                    ),
                    _c("textarea", {
                      staticClass: "form-control",
                      attrs: { id: "", cols: "30", rows: "4" }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn-round-blue",
                    on: { click: _vm.closeModel }
                  },
                  [_vm._v(_vm._s(_vm.$t("Save")))]
                )
              ])
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "slide-fade" } }, [
        _vm.show.auth
          ? _c("div", { staticClass: "popup" }, [
              !_vm.show.authPopup
                ? _c("div", { staticClass: "profile-popup" }, [
                    _c("div", { staticClass: "img-wraper" }, [
                      _c("img", {
                        attrs: { src: "/images/logo/2.png", alt: "" }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "restaurant" }, [
                      _c("h3", [_vm._v(_vm._s(_vm.$t("Meaal for Customers")))]),
                      _vm._v(" "),
                      _c("h3", [
                        _vm._v(
                          "\n                        " +
                            _vm._s(
                              _vm.$t(
                                "Earn points on every purchase you make at each Meaal-affiliated Restaurant, while helping to strengthen local economies, buying directly to local restaurants based on each location you visit."
                              )
                            ) +
                            "\n                    "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "d-flex justify-content-center" },
                        [
                          _c(
                            "button",
                            {
                              staticClass: "btn-round-primary",
                              on: {
                                click: function($event) {
                                  _vm.show.authPopup = true
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n                            " +
                                  _vm._s(_vm.$t("Login as a Customer")) +
                                  "\n                        "
                              )
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("h3", [
                        _vm._v(_vm._s(_vm.$t("Meaal for Restaurants")))
                      ]),
                      _vm._v(" "),
                      _c("h3", [
                        _vm._v(
                          "\n                        " +
                            _vm._s(
                              _vm.$t(
                                "Meaal all the systems that your Restaurant Business needs. POS Systems, Kitchen Displays, Home Delivery Systems, Card Processing, Digital Menu, Management tools. and more."
                              )
                            ) +
                            "\n                    "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "d-flex justify-content-center" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "btn-round-primary",
                              attrs: { to: "/login" }
                            },
                            [
                              _vm._v(
                                "\n                            " +
                                  _vm._s(_vm.$t("Login as a Restaurant")) +
                                  "\n                        "
                              )
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "copy-cat" }, [
                        _c("h3", [
                          _vm._v(
                            "© " +
                              _vm._s(new Date().getFullYear()) +
                              " Meaal, lnc."
                          )
                        ])
                      ])
                    ])
                  ])
                : _c("div", { staticClass: "profile-popup" }, [
                    _c("div", { staticClass: "img-wraper" }, [
                      _c("img", {
                        attrs: { src: "/images/logo/2.png", alt: "" }
                      })
                    ]),
                    _vm._v(" "),
                    _vm.loginbtn
                      ? _c("h3", { staticClass: "name2" }, [_vm._v("Login")])
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.loginbtn
                      ? _c("h3", { staticClass: "name2" }, [_vm._v("Register")])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "restaurant px-5" }, [
                      !_vm.loginbtn
                        ? _c("div", { staticClass: "form-group" }, [
                            _c("label", { attrs: { for: "email-auth" } }, [
                              _vm._v(_vm._s(_vm.$t("Name")))
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.name,
                                  expression: "user.name"
                                }
                              ],
                              staticClass: "form-control",
                              class: { "is-invalid": _vm.errors.has("name") },
                              attrs: { type: "email", id: "email-auth" },
                              domProps: { value: _vm.user.name },
                              on: {
                                keypress: function($event) {
                                  return _vm.errors.clear("name")
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.user,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.errors.has("name")
                        ? _c("span", { staticStyle: { color: "red" } }, [
                            _vm._v(
                              "\n                        " +
                                _vm._s(_vm.errors.get("name")) +
                                "\n                    "
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "email-auth" } }, [
                          _vm._v(_vm._s(_vm.$t("Email")))
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.user.email,
                              expression: "user.email"
                            }
                          ],
                          staticClass: "form-control",
                          class: { "is-invalid": _vm.errors.has("email") },
                          attrs: { type: "email", id: "email-auth" },
                          domProps: { value: _vm.user.email },
                          on: {
                            keypress: function($event) {
                              return _vm.errors.clear("email")
                            },
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.user, "email", $event.target.value)
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.errors.has("email")
                        ? _c("span", { staticStyle: { color: "red" } }, [
                            _vm._v(
                              "\n                        " +
                                _vm._s(_vm.errors.get("email")) +
                                "\n                    "
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "password-auth" } }, [
                          _vm._v(_vm._s(_vm.$t("Password")))
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.user.password,
                              expression: "user.password"
                            }
                          ],
                          staticClass: "form-control",
                          class: {
                            "is-invalid": _vm.errors.has("password")
                          },
                          attrs: { type: "password", id: "password-auth" },
                          domProps: { value: _vm.user.password },
                          on: {
                            keypress: function($event) {
                              return _vm.errors.clear("password")
                            },
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.user,
                                "password",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.errors.has("password")
                        ? _c("span", { staticStyle: { color: "red" } }, [
                            _vm._v(
                              "\n                        " +
                                _vm._s(_vm.errors.get("password")) +
                                "\n                    "
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.loginbtn
                        ? _c("div", { staticClass: "form-group" }, [
                            _c("label", { attrs: { for: "password-auth" } }, [
                              _vm._v(_vm._s(_vm.$t("Conifrm Password")))
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.password_confirmation,
                                  expression: "user.password_confirmation"
                                }
                              ],
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.errors.has("password")
                              },
                              attrs: { type: "password", id: "password-auth" },
                              domProps: {
                                value: _vm.user.password_confirmation
                              },
                              on: {
                                keypress: function($event) {
                                  return _vm.errors.clear("password")
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.user,
                                    "password_confirmation",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("span", {
                        ref: "login_error",
                        staticClass: "mt-2",
                        staticStyle: { color: "red" }
                      }),
                      _vm._v(" "),
                      _vm.loginbtn
                        ? _c(
                            "button",
                            {
                              staticClass: "btn-round-primary br-0 mb-5",
                              on: { click: _vm.loginCustomer }
                            },
                            [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(_vm.$t("LOGIN")) +
                                  "\n                    "
                              )
                            ]
                          )
                        : _c(
                            "button",
                            {
                              staticClass: "btn-round-primary br-0 mb-5",
                              on: { click: _vm.createCustomer }
                            },
                            [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(_vm.$t("CREATE ACCOUNT")) +
                                  "\n                    "
                              )
                            ]
                          ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "mt-5",
                          staticStyle: { cursor: "pointer" },
                          attrs: { role: "button" },
                          on: { click: _vm.shiftBtn }
                        },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(
                                _vm.loginbtn ? "Create Account" : "Login"
                              ) +
                              "\n                    "
                          )
                        ]
                      )
                    ])
                  ])
            ])
          : _vm._e()
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "icon mr-2" }, [
      _c("img", {
        attrs: {
          src: "https://meniuu.com/img\\mobile\\logout_image.png",
          alt: ""
        }
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/layouts/take-order-table/take-order-table.vue":
/*!************************************************************************!*\
  !*** ./resources/js/src/layouts/take-order-table/take-order-table.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _take_order_table_vue_vue_type_template_id_1f5dcc50_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true& */ "./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true&");
/* harmony import */ var _take_order_table_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./take-order-table.vue?vue&type=script&lang=js& */ "./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true& */ "./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _take_order_table_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _take_order_table_vue_vue_type_template_id_1f5dcc50_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _take_order_table_vue_vue_type_template_id_1f5dcc50_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1f5dcc50",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/layouts/take-order-table/take-order-table.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./take-order-table.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true&":
/*!**********************************************************************************************************************************!*\
  !*** ./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=style&index=0&id=1f5dcc50&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_style_index_0_id_1f5dcc50_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true& ***!
  \*******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_template_id_1f5dcc50_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/take-order-table/take-order-table.vue?vue&type=template&id=1f5dcc50&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_template_id_1f5dcc50_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_take_order_table_vue_vue_type_template_id_1f5dcc50_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);