(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_feather_icons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-feather-icons */ "./node_modules/vue-feather-icons/dist/vue-feather-icons.es.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'forgot-password',
  components: {
    KeyIcon: vue_feather_icons__WEBPACK_IMPORTED_MODULE_0__["KeyIcon"]
  },
  created: function created() {
    this.$Progress.stop();
  },
  data: function data() {
    return {
      errors: new Errors(),
      email: '',
      success: '',
      hideForm: false,
      loading: false
    };
  },
  methods: {
    forgetPassword: function forgetPassword() {
      var _this = this;

      this.$Progress.start();
      this.$http.post('/api/forget-password', {
        email: this.email
      }).then(function (response) {
        _this.$Progress.finish();

        _this.success = "Email Sent Successfully";
        _this.hideForm = true;
      }).catch(function (errors) {
        console.log(errors.response.data.errors.onEmail);
        _this.$refs.forget_error.innerHTML = errors.response.data.errors.onEmail;

        _this.$Progress.finish();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "[dir] .is-invalid[data-v-1308a3fd] {\n  background-color: #ff5555;\n  box-shadow: 0px 0px 8px #fa5e5e !important;\n}\n.center_out[data-v-1308a3fd] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\n.custom-class[data-v-1308a3fd] {\n  color: #E5D362;\n  width: 116px;\n  height: 112px;\n}\n[dir] .custom-class[data-v-1308a3fd] {\n  padding: 10px;\n}\n.login[data-v-1308a3fd] {\n  min-height: 100vh;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n[dir] .login[data-v-1308a3fd] {\n  background-image: linear-gradient(to top, #00c6fb1c 0%, #005bea00 100%), url(/images/pages/login/bg-login.jpg) !important;\n  background-size: cover;\n  background-position: center center;\n}\n.login .panel[data-v-1308a3fd] {\n  min-width: 440px;\n}\n[dir] .login .panel[data-v-1308a3fd] {\n  padding: 10px 40px;\n  background-color: #fff;\n  border-radius: 10px;\n}\n.login .panel .logo[data-v-1308a3fd] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.login .panel h1[data-v-1308a3fd] {\n  font-family: \"Montserrat\", sans-serif;\n  color: var(--main-color);\n  font-weight: 600;\n  letter-spacing: 1px;\n  font-size: 25px;\n}\n[dir] .login .panel h1[data-v-1308a3fd] {\n  text-align: center;\n  text-align: center;\n  margin: 0;\n  padding-top: 10px;\n}\n.login .panel p[data-v-1308a3fd] {\n  font-family: \"Montserrat\", sans-serif;\n  color: #000;\n  font-weight: 400;\n  letter-spacing: 1px;\n  font-size: 16px;\n}\n[dir] .login .panel p[data-v-1308a3fd] {\n  text-align: center;\n  text-align: center;\n  margin: 0;\n}\n.login .panel .form-group[data-v-1308a3fd] {\n  position: relative;\n}\n.login .panel .form-group input[data-v-1308a3fd] {\n  font-family: \"Montserrat\", sans-serif;\n  color: #111;\n  font-size: 14px !important;\n  letter-spacing: 1px;\n  font-weight: 400 !important;\n}\n[dir] .login .panel .form-group input[data-v-1308a3fd] {\n  background: #f2f2f2;\n  border-color: unset;\n  border-radius: 15px !important;\n  padding: 23px 40px;\n  border: unset;\n}\n.login .panel .form-group input[data-v-1308a3fd]:focus {\n  outline: none !important;\n}\n.login .panel .form-group img[data-v-1308a3fd] {\n  position: absolute;\n  top: 17px;\n  width: 17px;\n}\n[dir=ltr] .login .panel .form-group img[data-v-1308a3fd] {\n  left: 15px;\n}\n[dir=rtl] .login .panel .form-group img[data-v-1308a3fd] {\n  right: 15px;\n}\n@media (max-width: 440px) {\n.login .panel[data-v-1308a3fd] {\n    min-width: auto;\n}\n[dir] .login .panel[data-v-1308a3fd] {\n    padding: 10px 32px;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "login" }, [
    !_vm.hideForm
      ? _c("div", { staticClass: "panel" }, [
          _c(
            "div",
            { staticClass: "logo" },
            [
              _c("key-icon", {
                staticClass: "custom-class",
                attrs: { size: "1.5x" }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("h1", [_vm._v("Forgot Password")]),
          _vm._v(" "),
          _c(
            "p",
            { staticClass: "m-3 mb-5 mt-5", staticStyle: { width: "320px" } },
            [
              _vm._v(
                "Enter your Email and we'll mail you a link to reset your password"
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "form",
            {
              attrs: { method: "post" },
              on: {
                keydown: function($event) {
                  return _vm.errors.clear($event.target.name)
                },
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.forgetPassword()
                }
              }
            },
            [
              _c("input", {
                attrs: {
                  type: "hidden",
                  name: "_token",
                  value: "dsvNkUNqKsvOOBxtv5j6HDpiYcTPdiMCATr3tw5k"
                }
              }),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "form-group has-feedback",
                  staticStyle: { "margin-bottom": "0px" }
                },
                [
                  _c("img", {
                    staticStyle: {
                      position: "absolute",
                      top: "17px",
                      left: "15px",
                      width: "17px"
                    },
                    attrs: { src: "https://meniuu.com/img/email_icon.png" }
                  }),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.email,
                        expression: "email"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.errors.has("email") },
                    attrs: {
                      type: "email",
                      value: "",
                      placeholder: "Email",
                      name: "email",
                      autocomplete: "off"
                    },
                    domProps: { value: _vm.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.email = $event.target.value
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c("div", {
                ref: "forget_error",
                staticClass: "mt-2 mb-2",
                staticStyle: { color: "red" }
              }),
              _vm._v(" "),
              _vm.success
                ? _c("div", { staticStyle: { color: "green" } }, [
                    _vm._v("\n        " + _vm._s(_vm.success) + "\n      ")
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _vm._m(1)
            ]
          )
        ])
      : _c(
          "div",
          {
            staticClass: "panel center_out",
            staticStyle: { "min-height": "50vh" }
          },
          [
            _c(
              "svg",
              {
                attrs: {
                  width: "99",
                  height: "99",
                  viewBox: "0 0 99 99",
                  fill: "none",
                  xmlns: "http://www.w3.org/2000/svg"
                }
              },
              [
                _c("path", {
                  attrs: {
                    d:
                      "M49.5 99C76.8381 99 99 76.8381 99 49.5C99 22.1619 76.8381 0 49.5 0C22.1619 0 0 22.1619 0 49.5C0 76.8381 22.1619 99 49.5 99Z",
                    fill: "#25AE88"
                  }
                }),
                _vm._v(" "),
                _c("path", {
                  attrs: {
                    d: "M75.2398 29.7002L43.5598 65.3402L23.7598 49.5002",
                    stroke: "white",
                    "stroke-width": "2",
                    e: "",
                    "stroke-miterlimit": "10",
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round"
                  }
                })
              ]
            ),
            _vm._v(" "),
            _c(
              "p",
              { staticClass: "m-3 mt-5", staticStyle: { width: "320px" } },
              [
                _vm._v(
                  "Email sent Successfully Check your inbox for password reset"
                )
              ]
            )
          ]
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row justify-content-center mt-3" }, [
      _c("br")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xs-12 center_out" }, [
      _c(
        "button",
        { staticClass: "btn-round-blue", attrs: { type: "submit" } },
        [_vm._v("\n          Send\n        ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/pages/ForgetPassword.vue":
/*!*********************************************************!*\
  !*** ./resources/js/src/views/pages/ForgetPassword.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ForgetPassword_vue_vue_type_template_id_1308a3fd_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true& */ "./resources/js/src/views/pages/ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true&");
/* harmony import */ var _ForgetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ForgetPassword.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/ForgetPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true& */ "./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ForgetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ForgetPassword_vue_vue_type_template_id_1308a3fd_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ForgetPassword_vue_vue_type_template_id_1308a3fd_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1308a3fd",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/ForgetPassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/ForgetPassword.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/src/views/pages/ForgetPassword.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ForgetPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true& ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=style&index=0&id=1308a3fd&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_style_index_0_id_1308a3fd_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_template_id_1308a3fd_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/ForgetPassword.vue?vue&type=template&id=1308a3fd&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_template_id_1308a3fd_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgetPassword_vue_vue_type_template_id_1308a3fd_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);