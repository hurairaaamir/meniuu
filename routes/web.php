<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('application');
});

// Auth::routes();

Route::get('/user/{id}', 'UserController@show');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/url',function(){
    event(new \App\Events\RestaurantOpenEvent("Opened"));
    return event(new App\Events\RestaurantOpenEvent("Opened"));
});
Route::get('/test',function(){
    $data=[
        "product_name"=>"onion",
        "quantity"=>10,
        'measuring_unit'=>'kilo',
        'type'=>'min'
    ];

    return view('emails.stockEmail',$data);
    // event(new \App\Events\StockEmailEvent($data,'min',"0909cosmos@gmail.com"));

    // return "here";
    
    return view('emails.dishCooked',[
        "dish"=>"yellow rice",
        "logo"=>"https://restaurante.meaal.mx/images/restaurant/restaurant1.png",
        "customer"=>"Hurraira",
        "address"=>"street 3 road 2",
        "restaurant"=>"rigrex"
    ]);
});

Route::get('/mail','AuthController@mail');
Route::get('/order/stripe','StripeController@checkout');

Route::get('/changePassword/{token}','AuthController@changePassword')->name('changePassword');


