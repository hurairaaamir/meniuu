<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Broadcast::routes(['middleware' => 'auth:api']);

Route::middleware(['auth:api','customer'])->group(function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::resource('user-management/users', 'UserController');

    Route::post('/user-management/store-users','UserController@store');

    Route::post('/user-management/edit-users','UserController@update');

    Route::post('/user-management/edit-owner','UserController@editOwner');

    Route::post('/user-management/edit-restaurant','RestaurantController@editRestaurant');
    
    Route::post('/user-management/send-message','RestaurantController@sendMessage');
    
    Route::post('team-store','UserController@team_store');

    

    Route::post('create-restaurant','RestaurantController@store');
    
    Route::get('get-restaurant','RestaurantController@show');

    Route::get('team-get/{type}/{restaurant_id}','UserController@fetchTeam');

    Route::delete('team/delete/{id}','UserController@delete');


    Route::post('/product/store','ProductController@store');
    Route::get('/product/index/{restaurant_id}/{category_id}','ProductController@index');
    Route::get('/product/show/{id}','ProductController@show');
    Route::put('/product/update/{id}','ProductController@update');
    Route::delete('/product/delete/{id}','ProductController@delete');
    Route::get('/product/get-count/{restaurant_id}','ProductController@getCount');

    Route::post('/category/store','ProductCategoryController@store');
    Route::get('/category/index/{restaurant_id}','ProductCategoryController@index');
    Route::get('/category/show/{id}','ProductCategoryController@show');
    Route::put('/category/update/{id}','ProductCategoryController@update');
    Route::delete('/category/delete/{id}','ProductCategoryController@delete');
    

    
    Route::get('/enteries/get/{id}','EnteriesController@productEnteries');
    Route::post('/enteries/store','EnteriesController@store');
    Route::get('enteries/history/{restaurant_id}/{type}','EnteriesController@history');


    Route::get('/extras/index/{restaurant_id}','ExtraController@index');
    Route::get('/extras/show/{id}','ExtraController@show');
    Route::post('/extras/store','ExtraController@store');
    Route::put('/extras/update/{id}','ExtraController@update');
    Route::delete('/extras/delete/{id}','ExtraController@delete');


    Route::get('/dishes/index/{restaurant_id}/{category_id}','DishController@index');
    Route::get('/dishes/show/{id}','DishController@show');
    Route::post('/dishes/store','DishController@store');
    Route::post('/dishes/update','DishController@update');
    Route::delete('/dishes/delete/{id}','DishController@delete');
    Route::post('/dish/status','DishController@updateStatus');

    Route::get('/dish-category/index/{restaurant_id}','DishCategoryController@index');    
    Route::get('/dish-category/show/{id}','DishCategoryController@show');
    Route::post('/dish-category/store','DishCategoryController@store');
    Route::post('/dish-category/update','DishCategoryController@update');
    Route::delete('/dish-category/delete/{id}','DishCategoryController@delete');
    Route::post('/dish-category/status','DishCategoryController@updateStatus');
    
    
    Route::get('/dish-ingredient/index/{restaurant_id}/{dish_id}','DishIngredientController@index');
    Route::get('/dish-ingredient/show/{id}','DishIngredientController@show');
    Route::post('/dish-ingredient/store','DishIngredientController@store');
    Route::post('/dish-ingredient/update','DishIngredientController@update');
    Route::delete('/dish-ingredient/delete/{id}','DishIngredientController@delete');
    

    Route::get('/table/index/{restaurant_id}','TableController@index');
    Route::get('/table/show/{id}','TableController@show');
    Route::post('/table/store','TableController@store');
    Route::put('/table/update/{id}','TableController@update');
    Route::delete('/table/delete/{id}','TableController@delete');

    Route::get('/social-media/index/{restaurant_id}','SocialMediaController@index');
    Route::get('/social-media/show/{id}','SocialMediaController@show');
    Route::post('/social-media/store','SocialMediaController@store');
    Route::put('/social-media/update/{id}','SocialMediaController@update');
    Route::delete('/social-media/delete/{id}','SocialMediaController@delete');

    Route::get('/order/index/{restaurant_id}/{type}','OrderController@index');

    Route::get('/order/get/{restaurant_id}/{id}','OrderController@getOrder');

    Route::post('order/get-order-by-source','OrderController@getOrdersBySource');
    
    Route::put('/order/update/{id}','OrderController@update');
    Route::delete('/order/delete/{id}','OrderController@delete');
    Route::post('/order/confirmPassword','OrderController@confirmPassword');
    Route::post('/order/item-status','OrderController@item_status');
    Route::post('/order/order-status','OrderController@order_status');
    Route::post('/order/update-order-type','OrderController@updateOrderType');
    Route::get('/order/get-order-no/{restaurant_id}','OrderController@getOrderNo');
    Route::get('/order/tabs/{restaurant_id}','OrderController@getTabs');

    Route::get('/restaurant/get-count/{restaurant_id}','RestaurantController@getCount');
    
    Route::get('/analytics/count-dish/{restaurant_id}','AnalyticsController@countDish');
    Route::get('/analytics/count-dish-category/{restaurant_id}','AnalyticsController@countCategoryDish');
    Route::post('/analytics/count-dish-category','AnalyticsController@countCategoryDishPost');
    Route::get('/analytics/count-revenue/{restaurant_id}','AnalyticsController@countRevenue');
    Route::post('/analytics/order-details','AnalyticsController@orderDetails');
    Route::post('/analytics/get-most-sold','AnalyticsController@getMostSold');
    Route::post('/analytics/get-money','AnalyticsController@getMoney');
    Route::post('/analytics/get-plate','AnalyticsController@getPlates');
    Route::post('/analytics/get-inventory','AnalyticsController@getInventory');
    Route::post('/restaurant/change-status','RestaurantController@changeStatus');
    Route::get('/restaurant/get-account/{restaurant_id}','RestaurantController@getAccount');
    Route::post('/change-permission','RestaurantController@changePermission');
    Route::post('/get-record',"AnalyticsController@getRecord");


    Route::post('/notification-email/store',"NotificationEmailController@store");
    Route::get('/notification-email/show/{restaurant_id}',"NotificationEmailController@show");
});

Route::middleware(['auth:api'])->group(function(){
    Route::post('/logout', 'AuthController@logout');
    Route::post('/auth/user','UserController@auth_user');
    
    
});

Route::get('/order/search-dish/{restaurant_id}/{value}','DishController@searchDish');
Route::get('/dishes/get-search-dish/{restaurant_id}/{value}','DishController@getSearchDish');


Route::get('/dish-category/active/{restaurant_id}','DishCategoryController@indexActive');
Route::get('/dishes/active/{restaurant_id}/{category_id}','DishController@indexActive');
Route::get('/show-restaurant/{id}','RestaurantController@get');
Route::get('/order/show/{id}','OrderController@show');
Route::post('/order/store','OrderController@store');







Route::post('/login', 'AuthController@login')->name('login');
Route::post('/create-customer', 'AuthController@createCustomer');


Route::post('/register', 'AuthController@register');
Route::post('/order/stripe','StripeController@checkout');
Route::post('/webhook','StripeController@webhook');
Route::get('/intent','StripeController@getIntent');


Route::post('/pay/intent','StripeController@payIntent');
Route::post('/verify/stripe/email','StripeController@verifyStripeEmail');
Route::get('/bank/success/{restaurant_id}','StripeController@ConnectSuccess');
Route::post('/forget-password','AuthController@forgetPassword');
Route::post('/reset-password','AuthController@resetPassword');

