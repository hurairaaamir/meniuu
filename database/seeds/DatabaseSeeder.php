<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Restaurant;
use App\Account;
use App\Product;
use App\ProductCategory;
use App\Dish;
use App\DishCategory;
use App\Order;
use App\OrderItem;
use App\DishIngredient;
use App\Extra;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){


        DB::table('oauth_clients')->insert([
            "id"=>1,
            "user_id"=>null,
            'name'=>"Laravel Personal Access Client",
            "secret"=>"15ZB7Uz2aSROflGgPp7XPhAfsFwtZGVLcrc3RZ9i",
            // "provider"=>null,
            "redirect"=>"http://localhost",
            "personal_access_client"=>1,
            "password_client"=>0,
            "revoked"=>0
        ]);

        DB::table('oauth_clients')->insert([
            "id"=>2,
            "user_id"=>null,
            'name'=>"Laravel Password Grant Client",
            "secret"=>"fmTKwSaSFycytDgB5F6ntiS0wcWxIRpuSrLX3I5A",
            // "provider"=>"users",
            "redirect"=>"http://localhost",
            "personal_access_client"=>0,
            "password_client"=>1,
            "revoked"=>0
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            "id"=>1,
            "client_id"=>1
        ]);

        User::create([
            'name' => "Admin",
            'email' => "admin@admin.com",
            'email_verified_at' => now(),
            'role'=>'admin',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'image'=>'/images/restaurant/admin.jpeg'
        ]);

    

        factory(User::class,15)->create();
        
        $users=User::where('id','>',2)->get();
        
        foreach($users as $user){
            $id=rand(1,4);
            switch($id){
                case 1:
                    $image="/images/restaurant/restaurant1.png";
                break;
                case 2:
                    $image="/images/restaurant/restaurant2.png";
                break;
                case 3:
                    $image="/images/restaurant/restaurant3.png";
                break;
                default:
                    $image="/images/restaurant/restaurant4.png";
                break;
            }
            $restaurant=Restaurant::create([
                'name'=>"Restaurant",
                "home_delivery"=>"yes we have home delivery",
                "logo"=>'/images/restaurant/restaurant1.png',
                'address'=>'street 3 house 5 PAC Kamra',
                'delivery'=>'yes',
                'phone'=>'0300000000000',
                'currency'=>'USD',
                'description'=>'Sed hendrerit. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Quisque id mi.Pellentesque auctor neque nec urna. Phasellus ullamcorper ipsum rutrum nunc. Pellentesque dapibus hendrerit tortor.
                                Curabitur suscipit suscipit tellus. Fusce pharetra convallis urna. In dui magna, posuere eget, vestibulum et, tempor auctor, justo.',
                'user_id'=>$user->id
            ]);
             
            for($i=0;$i<2;$i++){
                User::create([
                    'name' => $restaurant->name.$i." Cook",
                    'email' => "cook".$user->id.$i."@gmail.com",
                    'email_verified_at' => now(),
                    'role'=>'cook',
                    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                    'remember_token' => Str::random(10),
                    'restaurant_id'=>$restaurant->id,
                    'image'=>'/images/users/cook.jpeg'
                ]);     
            }

//          waiter
            for($i=0;$i<4;$i++){
                User::create([
                    'name' => $restaurant->name." Waiter",
                    'email' => "waiter".$user->id.$i."@gmail.com",
                    'email_verified_at' => now(),
                    'role'=>'waiter',
                    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                    'remember_token' => Str::random(10),
                    'restaurant_id'=>$restaurant->id,
                    'image'=>'/images/users/waiter.jpeg'
                ]);     
            }
            
            User::create([
                'name' => $restaurant->name." Manager",
                'email' => "manager".$user->id."@gmail.com",
                'email_verified_at' => now(),
                'role'=>'manager',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
                'restaurant_id'=>$restaurant->id,
                'image'=>'/images/users/manager.png'
            ]);
            
        
            for($i=0;$i<3;$i++){
                User::create([
                    'name' => $restaurant->name." Delivery Man",
                    'email' => "delivery_man".$user->id.$i."@gmail.com",
                    'email_verified_at' => now(),
                    'role'=>'delivery_man',
                    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                    'remember_token' => Str::random(10),
                    'restaurant_id'=>$restaurant->id,
                    'image'=>'/images/users/delivery_man.jpeg'
                ]);
            }

        }
        $owner=User::create([
            'name' => "Owner",
            'email' => "owner@owner.com",
            'email_verified_at' => now(),
            'role'=>'owner',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'image'=>'/images/restaurant/admin.jpeg'
        ]);
        User::create([
            'name' => "huraira",
            'email' => "muhammadhurairaaamir@gmail.com",
            'email_verified_at' => now(),
            'role'=>'owner',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'image'=>'/images/restaurant/admin.jpeg'
        ]);
        $restaurant=Restaurant::create([
            'name'=>"Restaurant",
            "home_delivery"=>"yes we have home delivery",
            "logo"=>'/images/restaurant/restaurant1.png',
            'address'=>'street 3 house 5 PAC Kamra',
            'delivery'=>'yes',
            'phone'=>'0300000000000',
            'currency'=>'USD',
            'description'=>'Sed hendrerit. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Quisque id mi.Pellentesque auctor neque nec urna. Phasellus ullamcorper ipsum rutrum nunc. Pellentesque dapibus hendrerit tortor.
                            Curabitur suscipit suscipit tellus. Fusce pharetra convallis urna. In dui magna, posuere eget, vestibulum et, tempor auctor, justo.',
            'user_id'=>$owner->id
        ]);

        // cook
        User::create([
            'name' => "Cook",
            'email' => "cook@cook.com",
            'email_verified_at' => now(),
            'role'=>'cook',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'restaurant_id'=>$restaurant->id,
            'image'=>'/images/users/cook.jpeg'
        ]);
        
        User::create([
            'name' => "Manager",
            'email' => "manager@manager.com",
            'email_verified_at' => now(),
            'role'=>'manager',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'restaurant_id'=>$restaurant->id,
            'image'=>'/images/users/manager.png'
        ]);

        User::create([
            'name' => "Cashier",
            'email' => "cashier@cashier.com",
            'email_verified_at' => now(),
            'role'=>'cashier',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'restaurant_id'=>$restaurant->id,
            'image'=>'/images/users/cashier.jpeg'
        ]);

        User::create([
            'name' => "Delivery Man",
            'email' => "delivery@delivery.com",
            'email_verified_at' => now(),
            'role'=>'delivery_man',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'restaurant_id'=>$restaurant->id,
            'image'=>'/images/users/delivery_man.jpeg'
        ]);

        User::create([
            'name' => "Waiter",
            'email' => "waiter@waiter.com",
            'email_verified_at' => now(),
            'role'=>'waiter',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'restaurant_id'=>$restaurant->id,
            'image'=>'/images/users/waiter.jpeg'
        ]);
        $categories=[
            [
                "id"=>'1',
                'category_name'=>"vegetables",
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'2',
                'category_name'=>"Fruits",
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'3',
                'category_name'=>"Spices",
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]

        ];
        
        ProductCategory::insert($categories);
        
        $products=[
            [
                'id'=>'1',
                'product_name'=>'Potatos',
                'measuring_unit'=>'Kilogram',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'1',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'21',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'2',
                'product_name'=>'Tomatos',
                'measuring_unit'=>'Grams',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'1',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'43',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'3',
                'product_name'=>'Onions',
                'measuring_unit'=>'Kilogram',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'1',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'33',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'4',
                'product_name'=>'Mangoes',
                'measuring_unit'=>'Grams',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'2',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'23',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'5',
                'product_name'=>'Bananas',
                'measuring_unit'=>'Pound',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'2',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'42',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'6',
                'product_name'=>'Apples',
                'measuring_unit'=>'Pound',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'2',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'32',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'7',
                'product_name'=>'Red chilli',
                'measuring_unit'=>'Ounce',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'3',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'23',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'8',
                'product_name'=>'Black pepper',
                'measuring_unit'=>'Ounce',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'3',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'43',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>'9',
                'product_name'=>'Salt',
                'measuring_unit'=>'Piece',
                'min_stock'=>'20',
                'max_stock'=>'40',
                'opt_stock'=>'30',
                'product_category_id'=>'3',
                'restaurant_id'=>$restaurant->id,
                'quantity'=>'32',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]
        ];
        Product::insert($products);

















        Account::create([
            "account_id"=>"acct_1Hcr8SJbNjBKO64D",
            "email"=>"meaal.test@gmail.com",
            "status"=>'active',
            "restaurant_id"=>$restaurant->id,
        ]);

        $dishCategories=[
            [
                "id"=>1,
                "dish_name"=>"Rice",
                "image"=>"/images/dishCategory/rice.jpeg",
                "restaurant_id"=>$restaurant->id,
                "status"=>"active",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],
            [
                "id"=>2,
                "dish_name"=>"Sandwich",
                "image"=>"/images/dishCategory/sandwich.webp",
                "restaurant_id"=>$restaurant->id,
                "status"=>"active",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>3,
                "dish_name"=>"Shakes",
                "image"=>"/images/dishCategory/juice.jpg",
                "restaurant_id"=>$restaurant->id,
                "status"=>"active",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]
        ];

        DishCategory::insert($dishCategories);
       
        $dishes=[
            [
                "id"=>1,
                "name"=>"yellow rice",
                "come_with"=>'a:2:{i:0;s:5:"salad";i:1;s:7:"ketchup";}',
                "description"=>"Nam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit vel",
                "price"=>"12",
                "image1"=>"/images/dish/rice1.jpg",
                "image3"=>"/images/dish/rice2.jpg",
                "image2"=>"/images/dish/rice3.webp",
                "image4"=>"",
                "choice1"=>"Aenean leo ligula porttitor eu",
                "ch1_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice2"=>"Aenean leo ligula porttitor eu",
                "ch2_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice3"=>"Aenean leo ligula porttitor eu",
                "ch3_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice4"=>"Aenean leo ligula porttitor eu",
                "ch4_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "send_to"=>"waiters",
                "extras"=>'a:3:{i:0;a:2:{s:4:"name";s:5:"salad";s:5:"price";s:1:"1";}i:1;a:2:{s:4:"name";s:7:"ketchup";s:5:"price";s:1:"2";}i:2;a:2:{s:4:"name";s:6:"cheese";s:5:"price";s:1:"3";}}',
                "status"=>"active",
                "dish_category_id"=>1,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],
            [
                "id"=>2,
                "name"=>"Bread sandwich",
                "come_with"=>'a:2:{i:0;s:5:"salad";i:1;s:7:"ketchup";}',
                "description"=>"Nam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit vel",
                "price"=>"12",
                "image1"=>"/images/dish/sandwich3.jpg",
                "image2"=>"/images/dish/sandwich2.jpeg",
                "image3"=>"/images/dish/sandwich3.jpg",
                "image4"=>"",
                "choice1"=>"Aenean leo ligula porttitor eu",
                "ch1_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice2"=>"Aenean leo ligula porttitor eu",
                "ch2_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice3"=>"Aenean leo ligula porttitor eu",
                "ch3_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice4"=>"Aenean leo ligula porttitor eu",
                "ch4_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "status"=>"active",
                "send_to"=>"waiters",
                'restaurant_id'=>$restaurant->id,
                "extras"=>'a:3:{i:0;a:2:{s:4:"name";s:5:"salad";s:5:"price";s:1:"1";}i:1;a:2:{s:4:"name";s:7:"ketchup";s:5:"price";s:1:"2";}i:2;a:2:{s:4:"name";s:6:"cheese";s:5:"price";s:1:"3";}}',
                "dish_category_id"=>2,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],
            [
                "id"=>3,
                "name"=>"fruit shakes",
                "come_with"=>'a:2:{i:0;s:5:"salad";i:1;s:7:"ketchup";}',
                "description"=>"Nam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit vel",
                "price"=>"12",
                "image1"=>"/images/dish/juice1.jpg",
                "image2"=>"/images/dish/juice2.jpg",
                "image3"=>"/images/dish/juice3.png",
                "image4"=>"",
                "choice1"=>"Aenean leo ligula porttitor eu",
                "ch1_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice2"=>"Aenean leo ligula porttitor eu",
                "ch2_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice3"=>"Aenean leo ligula porttitor eu",
                "ch3_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "choice4"=>"Aenean leo ligula porttitor eu",
                "ch4_description"=>'a:2:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";}',
                "status"=>"active",
                "send_to"=>"waiters",
                'restaurant_id'=>$restaurant->id,
                "extras"=>'a:3:{i:0;a:2:{s:4:"name";s:5:"salad";s:5:"price";s:1:"1";}i:1;a:2:{s:4:"name";s:7:"ketchup";s:5:"price";s:1:"2";}i:2;a:2:{s:4:"name";s:6:"cheese";s:5:"price";s:1:"3";}}',
                "dish_category_id"=>3,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ]
            
        ];
       
        Dish::insert($dishes);
        $DishIngredients=[
            [
                "id"=>1,
                'product'=>"Onions",
                'category'=>"vegetables",
                'measuring_unit'=>"Kilogram",
                'quantity'=>"2",
                'dish_id'=>1,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
                
            ],
            [
                "id"=>2,
                'product'=>"Potatos",
                'category'=>"vegetables",
                'measuring_unit'=>"Kilogram",
                'quantity'=>"1",
                'dish_id'=>1,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
                
            ],
            [
                "id"=>3,
                'product'=>"Red chilli",
                'category'=>"Spices",
                'measuring_unit'=>"Ounce",
                'quantity'=>"1",
                'dish_id'=>1,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],  




            [
                "id"=>4,
                'product'=>"Tomatos",
                'category'=>"vegetables",
                'measuring_unit'=>"Grams",
                'quantity'=>"2",
                'dish_id'=>2,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
                
            ],
            [
                "id"=>5,
                'product'=>"Black pepper",
                'category'=>"Spices",
                'measuring_unit'=>"Ounce",
                'quantity'=>"1",
                'dish_id'=>2,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ], 



            [
                "id"=>6,
                'product'=>"Bananas",
                'category'=>"Fruits",
                'measuring_unit'=>"Pound",
                'quantity'=>"2",
                'dish_id'=>3,
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ]
        ];
        DishIngredient::insert($DishIngredients);

        
        factory(Order::class,65)->create();


        $orders=Order::all();

        foreach($orders as $order){
            $name="";
            $dish_id=rand(1,3);
            switch($dish_id){
                case 1:
                    $name="yellow rice";
                    $category_name="Rice";
                break;
                case 2:
                    $name="Bread sandwich";
                    $category_name="Sandwich";
                break;
                case 3:
                    $name="fruit shakes";
                    $category_name="Shakes";
                break;
            }
            OrderItem::create([
                "ch_description"=>"",
                "extras"=>"",
                "price"=>"12",
                "name"=>$name,
                "category_name"=>$category_name,
                "quantity"=>1,
                "note"=>"Phasellus a est. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Duis lobortis massa imperdiet quam. In auctor lobortis lacus.",
                "total"=>"20",
                "status"=>"served",
                'restaurant_id'=>$restaurant->id,
                "order_id"=>$order->id
            ]);

        }

        $order=Order::create([
            "email"=>"test@gmail.com",
            "phone"=>"03000000000",
            "optional_note"=>"Nam quam nunc blandit vel Nam quam nunc blandit velNam quam nunc blandit vel",
            "pay"=>"100",
            "change"=>"32",
            "table"=>"table 2",
            "subtotal"=>"24",
            "status"=>"cooking",
            "orderType"=>"pending",
            "timestamp"=>"Sat Aug 29 2020 12:36:20 GMT+0500 (Pakistan Standard Time)",
            "restaurant_id"=>$restaurant->id
        ]);

        OrderItem::create([
            "ch_description"=>'a:4:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";i:2;s:17:"Aenean leo ligula";i:3;s:30:"Aenean leo ligula porttitor eu";}',
            "extras"=>'a:3:{i:0;a:2:{s:4:"name";s:5:"salad";s:5:"price";s:1:"1";}i:1;a:2:{s:4:"name";s:7:"ketchup";s:5:"price";s:1:"2";}i:2;a:2:{s:4:"name";s:6:"cheese";s:5:"price";s:1:"3";}}',
            "price"=>"12",
            "name"=>"mango juice",
            "quantity"=>"1",
            "note"=>"Nam quam nunc blandit vel Nam quam nunc blandit vel Nam quam nunc blandit vel Nam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit vel",
            "total"=>"12",
            "status"=>"cooking",
            "order_id"=>$order->id
        ]);

        OrderItem::create([
            "ch_description"=>'a:4:{i:0;s:30:"Aenean leo ligula porttitor eu";i:1;s:17:"Aenean leo ligula";i:2;s:17:"Aenean leo ligula";i:3;s:30:"Aenean leo ligula porttitor eu";}',
            "extras"=>'a:3:{i:0;a:2:{s:4:"name";s:5:"salad";s:5:"price";s:1:"1";}i:1;a:2:{s:4:"name";s:7:"ketchup";s:5:"price";s:1:"2";}i:2;a:2:{s:4:"name";s:6:"cheese";s:5:"price";s:1:"3";}}',
            "price"=>"12",
            "name"=>"mango juice",
            "quantity"=>"1",
            "note"=>"Nam quam nunc blandit vel Nam quam nunc blandit vel Nam quam nunc blandit vel Nam quam nunc blandit velNam quam nunc blandit velNam quam nunc blandit vel",
            "total"=>"12",
            "status"=>"cooking",
            "order_id"=>$order->id
        ]);

        

        $extras=[
            [
                "id"=>1,
                "name"=>"salad",
                "price"=>"1",
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],
            [
                "id"=>2,
                "name"=>"ketchup",
                "price"=>"2",
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],
            [
                "id"=>3,
                "name"=>"cheese",
                "price"=>"3",
                'restaurant_id'=>$restaurant->id,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42'
            ],
        ];
        Extra::insert($extras);
    }


}
            