<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string("email")->nullable();
            $table->string("phone")->nullable();
            $table->text("optional_note")->nullable();
            $table->string("pay")->nullable();
            $table->string("change")->nullable();
            $table->string("table")->nullable();
            $table->string("subtotal")->nullable();
            $table->string("total")->nullable();
            $table->string("timestamp");
            $table->string("paymentMethod")->nullable();
            $table->string("cardNo")->nullable();
            $table->string("mmyycc")->nullable();
            $table->string("status")->nullable();
            $table->string("orderType")->nullable();  
            
            $table->string("tipAmount")->nullable();
            $table->string("tip")->nullable();
            $table->string("address")->nullable();
            $table->string("source")->nullable();
            
            $table->string("serviceMode")->nullable();
            $table->string("delivery")->nullable();
            $table->string("order_code")->nullable();

            $table->string('customer_name')->nullable();
            $table->string("stripe_id")->nullable();
            $table->enum('active',['yes','no'])->nullable();

            $table->unsignedBigInteger('restaurant_id')->nullable();
            $table->foreign('restaurant_id')
                ->references('id')->on('restaurants')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
