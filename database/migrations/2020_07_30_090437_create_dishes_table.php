<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishes', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('come_with')->nullable();
            $table->text('description')->nullable();
            $table->string('price');
            $table->string('image1');
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->string('choice1')->nullable();
            $table->text('ch1_description')->nullable();
            $table->string('choice2')->nullable();
            $table->text('ch2_description')->nullable();
            $table->string('choice3')->nullable();
            $table->text('ch3_description')->nullable();
            $table->string('choice4')->nullable();
            $table->text('ch4_description')->nullable();
            $table->enum('status',['active','inactive']);
            $table->string('send_to');
            $table->text('extras')->nullable();
            $table->unsignedBigInteger('restaurant_id')->nullable();
            $table->foreign('restaurant_id')
                ->references('id')->on('restaurants')
                ->onDelete('cascade');
            $table->unsignedBigInteger('dish_category_id')->nullable();
            $table->foreign('dish_category_id')
                ->references('id')->on('dish_categories')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dishes');
    }
}
