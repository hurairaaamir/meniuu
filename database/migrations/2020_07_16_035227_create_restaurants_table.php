<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('delivery')->nullable();
            $table->string('phone')->nullable();
            $table->string('currency')->nullable();
            $table->text('description')->nullable();
            $table->string('home_delivery')->nullable();
            $table->enum('status',['open','close','disabled'])->default('close');
            $table->string('logo')->nullable();
            $table->enum('analytics_permission',['yes','no'])->default('no');
            $table->enum('inventory_permission',['yes','no'])->default('no');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
