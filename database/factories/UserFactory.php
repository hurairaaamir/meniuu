<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Profile;
use App\Order;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'role'=> 'owner',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'image'=>$faker->randomElement(['/images/portrait/small/avatar-s-1.jpg','/images/portrait/small/avatar-s-3.jpg','/images/portrait/small/avatar-s-5.jpg','/images/portrait/small/avatar-s-7.jpg'])
    ];
});

$factory->define(DishCategory::class, function (Faker $faker) {
    $no=rand(1,3);
    $name="";
    $image="";
    switch($no){
        case 1:
            $name="rice";
            $image="rice.jpg";
        break;
        case 2:
            $name="sandwich";
            $image="sandwich.webp";
        break;
        case 3:
            $name="Shakes";
            $image="juice.jpg";
        break;
    }
    return [
        "dish_name"=>$name,
        "image"=>$image,
        'status'=>'active',
        "restaurant_id"=>"1"
    ];
});
$factory->define(Order::class, function (Faker $faker) {
    return [
        "email"=>$faker->randomElement(['0909comos@gmail.com','test@gmail.com','customer@customer.com']),
        "phone"=>$faker->randomElement(['09000000000000','03000000000','0800000000000']),
        "optional_note"=>'Sed a libero. Etiam vitae tortor. Maecenas vestibulum mollis diam. Donec venenatis vulputate lorem. Donec sodales sagittis magna.',
        "pay"=>$faker->randomElement(['40','45','50']),
        "change"=>'5',
        "table"=>'table 1',
        "subtotal"=>'30',
        "total"=>$faker->randomElement(['40','45','50']),
        "timestamp"=>"Sat Aug 29 2020 12:36:20 GMT+0500 (Pakistan Standard Time)",
        "created_at"=>$faker->randomElement(['2020-02-15 19:14:42','2019-01-01 19:14:42','2020-03-15 19:14:42',now()]),
        "restaurant_id"=>15,
        "status"=>'served',
        "orderType"=>'payed',
        "paymentMethod"=>$faker->randomElement(['cash','card']),
        "active"=>"no",
    ];
});


